<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');
require_once('Constants.class.php');

/****************************************************************************
 * Description
 *
 * This class is used for handling database actions of Contact History Actions 
 * Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.02
 * 
 *  * Changes
 * Date        Version Author                Reason
 * ??/??/2012  1.00    Nageswara Rao Kanteti Initoal version
 * 09/01/2013  1.01    Brian Etherington     Replaced SkylineBrandID with SKYLINE_BRAND_ID in Constants.class.php
 * 24/04/2013  1.02    Andrew J. Williams    Added New ID to return array in create method
 *****************************************************************************/

class ContactHistoryActions extends CustomModel {
    
    private $conn;
    private $dbColumns = array('t1.ContactHistoryActionID', 't1.ActionCode', 't1.Action',  't1.Type', 't1.Status', 't2.BrandName', 't1.BrandID');
    private $tables    = "contact_history_action AS t1 LEFT JOIN brand AS t2 ON t1.BrandID=t2.BrandID";
    private $table     = "contact_history_action";
    //private $SkylineBrandID = 1000;      
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
       
        /*
        
        if (( isset($args['StockCodeSearchStr']) && $args['StockCodeSearchStr'] != '')  || 
                (isset($args['ModelNoSearchStr']) && $args['ModelNoSearchStr'] != '' )) {
           
            $args['where'] = '(';
            
            if ( $args['StockCodeSearchStr'] != '') {
                
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['StockCodeSearchStr'].'%' ).' OR ';
                }
                
            }
            else
            {
                for ( $i=0 ; $i<count($columns) ; $i++ ) {
		$args['where'] .= $columns[$i].' LIKE '.$this->conn->quote( '%'.$args['ModelNoSearchStr'].'%' ).' OR ';
                }
            }
            
            
            $args['where'] = substr_replace( $args['where'], "", -3 );
            $args['where'] .= ')';
	}
        
        */
        
        //$output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        
        
        
        if($this->controller->user->SuperAdmin)
        {
            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
        }
        else if(is_array($this->controller->user->Brands))
        {    
            
           $brandsList  = implode(",", array_keys($this->controller->user->Brands));
           
           
           
           if($brandsList)
           {
                //$brandsList .= ",".$this->controller->SkylineBrandID;  
                $brandsList .= ",".constants::SKYLINE_BRAND_ID;
           }    
          
          
            $args['where'] = "t1.BrandID IN (".$brandsList.")";

            $output = $this->ServeDataTables($this->conn, $this->tables, $this->dbColumns, $args);
       
        }
        
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['ContactHistoryActionID']) || !$args['ContactHistoryActionID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
       /**
     * Description
     * 
     * This method finds the maximum action code for given brand in database table.
     * 
     * @param interger $BrandID This is primary key of brand table.
     * @global $this->table
     * @return integer It returns maximum action code if it finds in the database table otherwise it returns 0.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function getActionCode($BrandID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ActionCode FROM '.$this->table.' WHERE BrandID=:BrandID ORDER BY ContactHistoryActionID DESC LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        //$this->controller->log(var_export($BrandID, true));
        
        $fetchQuery->execute(array(':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        if(isset($result[0]))
        {
           return $result[0];
        }
        else
        {
             return 0;
        }
       
    }
    
    
    /**
     * Description
     * 
     * This method is used for to validate action code for given brand.
     *
     * @param interger $ActionCode  
     * @param interger $BrandID.
     * @param interger $ContactHistoryActionID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($ActionCode, $BrandID, $ContactHistoryActionID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ContactHistoryActionID FROM '.$this->table.' WHERE ActionCode=:ActionCode AND BrandID=:BrandID AND ContactHistoryActionID!=:ContactHistoryActionID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ActionCode' => $ActionCode, ':BrandID' => $BrandID, ':ContactHistoryActionID' => $ContactHistoryActionID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['ContactHistoryActionID'])
        {
                return false;
        }
        
        return true;
    
    }
    
     /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
//        $sql = 'INSERT INTO '.$this->table.' (Action, ActionCode, Type, Status, BrandID)
//            VALUES(:Action, :ActionCode, :Type, :Status, :BrandID)';
        
        $fields = explode(', ', 'Action, ActionCode, Type, Status, BrandID');
        $fields = array_combine($fields , $fields);
        
        $sql = TableFactory::ContactHistoryAction()->insertCommand($fields);
        #$this->controller->log( $sql );
        
        if(!isset($args['ActionCode']) || !$args['ActionCode'])
        {
            $args['ActionCode'] = $this->getActionCode($args['BrandID'])+1;//Preparing next action code.
        }
       // $this->controller->log(var_export('tessss', true));
        
        if($this->isValidAction($args['ActionCode'], $args['BrandID'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':Action' => $args['Action'], ':ActionCode' => $args['ActionCode'], ':Type' => $args['Type'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID']));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['data_inserted_msg'],
                        'ContactHistoryActionID' => $this->conn->lastInsertId()
                      );
        }
         else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ContactHistoryActionID, ActionCode, Action, Type, Status, BrandID FROM '.$this->table.' WHERE ContactHistoryActionID=:ContactHistoryActionID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ContactHistoryActionID' => $args['ContactHistoryActionID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['ActionCode'], $args['BrandID'], $args['ContactHistoryActionID']))
        {        
            /* Execute a prepared statement by passing an array of values */
//            $sql = 'UPDATE '.$this->table.' SET Action=:Action, ActionCode=:ActionCode, Type=:Type, Status=:Status, BrandID=:BrandID
//            WHERE ContactHistoryActionID=:ContactHistoryActionID';
            
            $fields = explode(', ', 'Action, ActionCode, Type, Status, BrandID');
            $fields = array_combine($fields , $fields);
            
            $where = 'ContactHistoryActionID=:ContactHistoryActionID';

            $sql = TableFactory::ContactHistoryAction()->updateCommand($fields, $where);            
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(array(':Action' => $args['Action'], ':ActionCode' => $args['ActionCode'], ':Type' => $args['Type'], ':Status' => $args['Status'], ':BrandID' => $args['BrandID'], ':ContactHistoryActionID' => $args['ContactHistoryActionID']));
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * fetchBrandContactActions
     * 
     * Get actions codes and name for ContacT Histaory Action for a specifc brand.
     * The query will return codes if set specifically for a brand or the default
     * if net set for the specified brand.
     * 
     * @param integer $bID  Brand ID
     * @return array    Results of query
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function fetchBrandContactActions($bId) {
        
        $params = array( 'BrandID' => $bId,
                         'SkylineBrandID' => constants::SKYLINE_BRAND_ID );
        
        /*$sql = "
                SELECT
                        `ContactHistoryActionID`,
                        `Action`
                FROM
                        `contact_history_action`
                WHERE
                        `BrandID` = $bId                                                -- Required Brand
                        OR (
                            `BrandID` = $this->SkylineBrandID                           -- Default Brand
                            AND `ActionCode` NOT IN (                                   -- Where the required brand has not been set
                                                    SELECT 
                                                            `ActionCode` 
                                                    FROM 
                                                            `contact_history_action` 
                                                    WHERE 
                                                            `BrandID` = $bId
                                                    )
                            )
                         AND `Status` = 'Active'
                ORDER BY
                        `ActionCode` 
               ";*/
        
         $sql = "
                SELECT
                        `ContactHistoryActionID`,
                        `Action`
                FROM
                        `contact_history_action`
                WHERE
                        `BrandID` = :BrandID                                            -- Required Brand
                        OR (
                            `BrandID` = :SkylineBrandID                                 -- Default Brand
                            AND `ActionCode` NOT IN (                                   -- Where the required brand has not been set
                                                    SELECT 
                                                            `ActionCode` 
                                                    FROM 
                                                            `contact_history_action` 
                                                    WHERE 
                                                            `BrandID` = :BrandID
                                                    )
                            )
                         AND `Status` = 'Active'
                ORDER BY
                        `ActionCode` 
               ";
        
        $result = $this->Query($this->conn, $sql, $params);
        
        return($result);
    }
    
}
?>