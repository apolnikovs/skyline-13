<?php

require_once('BaseSmartyController.class.php');

/**
 * Short Description of PerformanceController.
 * 
 * Long description of PerformanceController.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.09
 * 
 *  
 * Changes
 * Date        Version Author                Reason
 * 11/12/2012  1.0     Brian Etherington     Initial Version
 * 13/12/2012  1.01    Andrew J. Williams    Copied existing performance code form Dafault Contoller
 * 31/12/2012  1.02    Brian Etherington     Added Segmentation Charts
 * 09/01/2012  1.03    Andrew J. Williams    Changes to Open TAT screens - showing all three graphs & datatable
 * 28/01/2012  1.04    Andrew J. Williams    Fixed sorting by percent in Open TAT screen
 * 07/02/2013  1.05    Andrew J. Williams    Changes to Open TAT screens for Joe
 * 25/03/2013  1.06    Andrew J. Williams    Changes to fetchOpenTAT for definition of OpenJob
 * 02/04/2013  1.07    Andrew J. Williams    Layout Changes for Joe
 * 16/04/2013  1.08    Andrew J. Williams    Closes TAT Dials changeds to refelect recent changes on Closed Job Screen
 * 25/04/2013  1.09    Andrew J. Williams    Issue 314 - Undefined display on Performace - Open Jobs
 ******************************************************************************/

class PerformanceController extends BaseSmartyController {
    
    private $model;

    public function __construct() {   
        
        parent::__construct();
        
        $this->model = $this->loadModel('PerformanceBusinessModel');
        
        $this->smarty->assign('spid', 0); 
        $this->smarty->assign('nid', 0);
        $this->smarty->assign('cid', 0);
        $this->smarty->assign('bid', 0);
        $this->smarty->assign('graphtype', 0);
        $this->smarty->assign('graphdatasrc', 0);
        $this->smarty->assign('graph', 0);
        $this->smarty->assign('guage', 0);
        $this->smarty->assign('showguage', 0);
        
    }
    
   /**
    * JobsGauges
    * 
    * The is to display Jobs Gauges sections of the Performance Screen
    *
    * @param array $args
    * @return void
    * 
    * Initial Static Demo
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    * 
    * Activated fields with live data
    * @author Andrew J. Williams <Andrew.Williams@awcomputech.com>
    *  
    ********************************************/
    public function JobsGaugesAction( $args ) {
        $branches_model = $this->loadModel('Branches'); 
        $job_model = $this->loadModel('Job');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $Skyline = $this->loadModel('Skyline');
        $users_model = $this->loadModel('Users');
        
        $user = $users_model->GetUser( $this->session->UserID );
        $this->smarty->assign('loggedin_user', $this->user);
        $gaugePrefs = $users_model->getGaugePreferences_user( $this->session->UserID );
        $this->smarty->assign('gaugePrefs', $gaugePrefs);

        if ( ! is_null($user->NetworkID)) {
            $args['nid'] = $user->NetworkID;
        }
        
        if ( ! is_null($user->ClientID)) {
            $args['cid'] = $user->ClientID;
        }

        if ( ! is_null($user->ServiceProviderID)) {
            $args['spid'] = $user->ServiceProviderID;
        }
        
        if ( ! is_null($user->BranchID)) {
            $args['bid'] = $user->BranchID;
        }
        
        $filters = array (                                                      /* Array of filters to return correct jobs (open closed etc) passed to quries getting data */
                          'OpenJobs' => 'ISNULL(`ServiceProviderDespatchDate`)',             
                          'ClosedJobs' => 'NOT ISNULL(`ServiceProviderDespatchDate`)',
                          'OpenTAT' => 'ISNULL(`ServiceProviderDespatchDate`)',
                          'ClosedTAT' => 'NOT ISNULL(`ServiceProviderDespatchDate`)'
                         );
        
        $graphdataopts = array (
                                //'jbiw' => 'Booked in Last Week',
                               // 'jctry' => 'Country',
                                'jeng' => 'Engineer',
                                'jbm' => 'Manufacturer',
                                'jbs' => 'Status' //,
                                //'jcty' => 'Unitary Area'
                               );
        
        $this->smarty->assign('graphdataopts',$graphdataopts);
        
        $listdataopts = array (
                                'client' => 'Client',
                                'make' => 'Make',
                                'sp' => 'Service Provider'  //,
                                //'network' => 'Network'                        Network removed at Joe's request 02/04/2013
                              );//
        
        $this->smarty->assign('listdataopts',$listdataopts);
                 
        $page = $this->messages->getPage('performance', $this->lang);
        $this->smarty->assign('page', $page);  
        $tab = isset($args[0]) ? $args[0] : 'OpenJobs';                         /* If no paramter has been passed then default is OpenJobs */
        
        if(isset($args['openby'])) {                                            /* Het which perception og open by we need */
            $openby = $args['openby'];
            if ($openby == 'branch') {
                if ($tab == 'ClosedTAT') {
                    $filters[$tab] = "NOT ISNULL(`ClosedDate`)";
                    $datefield = '`ClosedDate`';
                } else {
                    $filters[$tab] = "ISNULL(`ClosedDate`)";
                    $datefield = '`DateBooked`';
                }
            } else {
                if ($tab == 'ClosedTAT') {
                    $filters[$tab] = "NOT ISNULL(`ServiceProviderDespatchDate`)";
                    $datefield = '`ServiceProviderDespatchDate`';
                } else {
                    $datefield = '`DateBooked`';
                }
            }
        } else {
            $openby = 'sp';
            if ($tab == 'ClosedTAT') {
                $datefield = '`ServiceProviderDespatchDate`';
            } else {
                $datefield = '`DateBooked`';
            }
        }
        $this->smarty->assign('openby',$openby);
        
        if ( isset($args['datefrom'])) {
            $datefrom = $args['datefrom'];
        } else {
            $year = date('Y');                                                  /* Current Year */
            if (date('m') == '01') {                                            /* If Januray */
                $year--;                                                        /* Last year */
            }
            $datefrom = "$year-01-01";
        } /* fi isset $args['display'] */
        $this->smarty->assign('datefrom',$datefrom);

        if ( isset($args['dateto'])) {
            $dateto = $args['dateto'];
        } else {
            $dateto = date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))); /*Last day of last month */
        } /* fi isset $args['display'] */
        $this->smarty->assign('dateto',$dateto);
        
        if (isset($args['displaylength'])) {                                    /* Check if display length has been passed */
            $displaylength = intval($args['displaylength']);                    /* Yes so use it */
        } else {
            $displaylength = 10;                                                /* No  so default to 10 */
        }
        $this->smarty->assign('displaylength',$displaylength);
        
        if ($tab == 'ClosedTAT' || $tab == 'ClosedJobs' ) {                     /* If dealing with closed jobs */
            if ( isset($args['display'])) {
                $display = $args['display'];
            } else {
                $display = '30';
            } /* fi isset $args['display'] */
            $this->smarty->assign('display',$display);                          /* Assign the display by oprtion */
            
            switch ($display) {
                case 'ytd':
                    $filters[$tab] .= " AND YEAR(j.$datefield) = YEAR(CURDATE())";
                    $this->smarty->assign('time_filter','Year to Date');
                    break;
                
                case '30':
                    $filters[$tab].= " AND j.$datefield > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                    $this->smarty->assign('time_filter','Last 30 Days');
                    break;

                case 'dr':
                    $filters[$tab] .= " AND j.$datefield >= '$datefrom' AND j.$datefield <= '$dateto'";
                    $this->smarty->assign('time_filter','Between '.date('d/m/Y', strtotime($datefrom)).' and '.date('d/m/Y', strtotime($dateto)));
                    break;

                default: /* mtd */
                    $filters[$tab] .= " AND YEAR(j.$datefield) = YEAR(CURDATE()) AND MONTH(j.$datefield) = MONTH(CURDATE())";
                    $this->smarty->assign('time_filter','Month to Date');
            }
        } else {
            $display = 'all';
            $this->smarty->assign('display',$display);
            $this->smarty->assign('time_filter','');
        } /* fi tab == 'ClosedTAT' */
        
        if ( isset($args['graph']) ) {                                          /* Check if graph parameter has been passed */
            if ( ($tab == 'OpenJobs') || ($tab == 'ClosedJobs') ) {             /* Check if OpenClosedJobs */
                if ( array_key_exists($args['graph'],$graphdataopts) ) {        /* And is in graph data */
                    $graphdata = $args['graph'];
                } else {
                    $graphdata = 'jbm';
                }
            } elseif ( ( $tab == 'OpenTAT' ) || ( $tab == 'ClosedTAT' ) ) {
                if ( array_key_exists($args['graph'],$graphdataopts) ) {        /* And is isn't in the open closed jobs */
                    $graphdata = $args['graph'];
                } else {
                    $graphdata = 'jbs';
                }
            }
        } else {
            if ( ($tab == 'OpenJobs') || ($tab == 'ClosedJobs') ) {             /* No - set graph to approriate default */
                $graphdata = 'jbm';                                             /* Open/Closed Jobs - default is jobs by manufacturer */
            } else {
                $graphdata = 'jbs';                                             /* OpenTAT - Deault is jobs over 14 days */
            }
        }
        $this->smarty->assign('graphdatasrc', $graphdata);
        
        if ( isset($args['listdatasrc']) ) {                                    /* Check if graph parameter has been passed */
            $listdatasrc = $args['listdatasrc'];
        } else {
            $listdatasrc = 'sp';                                                /* No - set deafult (Service provider*/
        }
        $this->smarty->assign('listdatasrc', $listdatasrc);
        
        if ( isset($args['guage']) ) {                                          /* Check if guage parameter has been passed */
            $guage = $args['guage'];
        } else {
            $guage = 'all';                                                     /* No - set deafult */
        }
        $this->smarty->assign('guage', $guage);

        if ( isset($args['graphtype']) ) {                                      /* Check if graph parameter has been passed */
            $graphtype = $args['graphtype'];
        } else {
            $graphtype = 'ColumnChart';                                         /* No - default is column chart */
        }
        $this->smarty->assign('graphtype', $graphtype);
        
        if ( isset($args['showguage']) ) {                                      /* Check if shows guage (make graph reflect selected guage [OpenTAT]) has been passed */
            $showguage = $args['showguage'];
        } else {
            $showguage = 0;                                                     /* No - default is not limit graph to selected guage */
        }
        $this->smarty->assign('showguage', $showguage);
        
        if ( isset($args['scorderby']) ) {                                      /* Check if data list sort parameter has been passed */
            $scorderby = $args['scorderby'];
        } else {
            $scorderby = 'Name';                                                /* No - name is deafault serach by option */
        }
        $this->smarty->assign('graphtype', $graphtype);
        
        if ( ($tab != "OpenJobs") && ($tab != "ClosedJobs")                     /* Check if not open jobs and closed job tabs */
              && isset($args['jobs']) ) {                                       /* and  if jobs parameter has been passed */
            $jobs = $args['jobs'];
            if ( $jobs == "closed" ) {
                $filters[$tab] .= $filters['ClosedJobs'];
            } else {
                $filters[$tab] .= $filters['OpenJobs'];
            }
        } elseif (                                                              /* NotOpenJobs or ClosedJobs and no jobs filter passed */
                  ($tab != "OpenJobs") 
                  && ($tab != "ClosedJobs") 
                  && ($tab != "OpenTAT")
                  && ($tab != "ClosedTAT")
                 ) {           
            $jobs = 'open';                                                     /* No - default is open */
            $filters[$tab] .= $filters['OpenJobs'];
        }
        if( isset($jobs) ) $this->smarty->assign('jobs', $jobs);
        
        $addWhere = "";                                                         /* Additional where clause based on passed parameters */
        $addWhereSp = "";                                                       /* As above but for service provider only queries (such as service provider capacity */
        if ( isset($args['spid']) ) {                                           /* Check parameters for specific service type */
            $addWhere = " AND sp.`ServiceProviderID` = {$args['spid']}"; 
            $addWhereSp = $addWhere;
            $spName = $service_providers_model->getServiceCentreCompanyName($args['spid']);
            $this->smarty->assign('spid', $args['spid']);
        } else {
            $spName = '';
            $this->smarty->assign('spid', 0);
        }
        $this->smarty->assign('sp_name', $spName);
        
        $ncbFilter = "";                                                        /* text for network / client / brand filter (default is blank) */
        
        if ( isset($args['nid']) ) {                                            /* Check parameters for specific network */
            $addWhere .= " AND j.`NetworkID` = {$args['nid']}"; 
            $nName = $Skyline->getNetworkName($args['nid']);
            $this->smarty->assign('nid', $args['nid']);
            $ncbFilter =  " in $nName";
        } else {
            $nName = '';
            $this->smarty->assign('nid', 0);
        }
        $this->smarty->assign('n_name', $nName);
        
        if ( isset($args['cid']) ) {                                            /* Check parameters for specific client */
            $addWhere .= " AND j.`ClientID` = {$args['cid']}"; 
            $cName = $Skyline->getClientName($args['cid']);
            $this->smarty->assign('cid', $args['cid']);
            $ncbFilter =  " in $cName";
        } else {
            $cName = '';
            $this->smarty->assign('cid', 0);
        }
        $this->smarty->assign('c_name', $cName);
        
        if ( isset($args['bid']) ) {                                            /* Check parameters for specific branch */
            $addWhere .= " AND j.`BranchID` = {$args['bid']}"; 
            $bName = $branches_model->name($args['bid']);
            $this->smarty->assign('bid', $args['bid']);
            $ncbFilter =  " in $bName";
        } else {
            $bName = '';
            $this->smarty->assign('bid', 0);
        }
        $this->smarty->assign('b_name', $bName);
        
        $this->smarty->assign('ncbFilter', $ncbFilter);

        $filters[$tab] = $filters[$tab].$addWhere;
        
        /* Filter by network / client / branch popup box */                
        $ClientID         = $this->user->ClientID;
        $BranchID         = $this->user->BranchID;
        $DefaultBranchID  = false;
        $branches         = array();

        if(!$this->user->NetworkID)
        {    
            $showNetworkDropDown = true; 
            $networks  = $Skyline->getNetworks();
            $this->smarty->assign('networks', $networks); 
            $clients   = array();
        }
        else
        {
            $showNetworkDropDown = false; 

            if($this->user->ClientID)
            {
                $clients   = array();

                $DefaultBranchID  = $Skyline->getClientDefaultBranch($this->user->ClientID);
            } 
            else
            {
                $clients  = $Skyline->getNetworkClients($this->user->NetworkID);
            }

            $this->smarty->assign('NetworkID', $this->user->NetworkID); 
        }

        if(!$BranchID && !$DefaultBranchID && $ClientID)
        {
            $branches  = $Skyline->getBranches(null, null, $ClientID, $this->user->NetworkID);
        }             

        $this->smarty->assign('ClientID', $ClientID); 
        $this->smarty->assign('BranchID', $BranchID);
        $this->smarty->assign('DefaultBranchID', $DefaultBranchID);


        $this->smarty->assign('clients', $clients); 
        $this->smarty->assign('branches', $branches); 

        $this->smarty->assign('showNetworkDropDown', $showNetworkDropDown); 
                
        /* Right Strip */
        
        $this->smarty->assign('service_centre_repair_capacities', $service_providers_model->getServiceProviderCapacity());
        
        $rc_colour_settings = array ('red' => 320, 'yellow' => 240);          /* Reapair capcaity red yellow settings <--- temporary will be a user default */
        $this->smarty->assign('rc_colour_settings', $rc_colour_settings);
        
        if ( $tab == 'OpenTAT') {
            /* Deal with the system status preferences for what is defined as an open job */
            $SystemStatusesModel = $this->loadModel('SystemStatuses');
            $UserStatusPreferences = $SystemStatusesModel->getUserStatusPreferences('openJobs', 'js');

            if(count($UserStatusPreferences)) {
                $sIDStr = '';
                $sep = '';

                foreach($UserStatusPreferences as $usp) {
                    $sIDStr = $sIDStr.$sep.$usp['StatusID'];
                    $sep = ',';
                }
                $filters[$tab] .= ' AND s.StatusID IN (' . $sIDStr . ')';
            }
        }

        
        if (!isset($this->session->UserID)) 
        {
            $this->redirect('index'); 
        }
        else
        {    
        switch ($tab) {
            case 'OpenJobs':
            case 'ClosedJobs':
                
                /*********
                * Gauges *
                *********/
                
                $totalMatchingJobs = $job_model->getTotalMatchingJobs($filters[$tab]);
                $this->smarty->assign('DialBoundaryUpper',$gaugePrefs['GaDialBoundaryUpper']);
                $this->smarty->assign('DialBoundaryLower',$gaugePrefs['GaDialBoundaryLower']);
                
                /* Gauge 0 - Service Centre Repair Capacity */
                $gauge0 = array(
                                'red' => 320, /* <--- temporary will be a user default */
                                'yellow' => 240, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 40,                                    /* inc * div - max */
                                'div' => 10
                                );
                $gauge0['value'] = intval($service_providers_model->getTotalServiceProviderCapacity($addWhereSp));
                $this->smarty->assign('gauge0', $gauge0);
                
                /* Gauge 1 - Jobs Awaiting Parts */
                $gauge1 = array(
                                'red' => 320, /* <--- temporary will be a user default */
                                'yellow' => 240, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 40,                                    /* inc * div - max */
                                'div' => 10
                                );
                $gauge1['value'] = intval($job_model->getJobsAwaitingParts($filters[$tab]));
                $this->smarty->assign('gauge1', $gauge1);

                /* Gauge 2 - % jobs over x days */
                $gauge2 = array(
                                'daysTarget' => 14, /* <--- temporary will be a user default */
                                'red' => 75, /* <--- temporary will be a user default */
                                'yellow' => 50, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 10,                               /* inc * div - max */
                                'div' => 10
                                );
                $jobsOver = intval($job_model->getNumberJobsBetweenDays($gauge2['daysTarget'] + 1, 99999, $filters[$tab]));
                $gauge2['value'] =  number_format($totalMatchingJobs == 0 ? 0 : $jobsOver / $totalMatchingJobs * 100 , 2);
                $this->smarty->assign('gauge2', $gauge2);
            
                /* Gauge 3 - Average Days from Booking Date*/
                $gauge3 = array(
                                'red' => 320, /* <--- temporary will be a user default */
                                'yellow' => 240, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 40,                                    /* inc * div - max */
                                'div' => 10
                                );
                $gauge3['value'] = (float) $job_model->getAverageDaysFromBookingDate($filters[$tab]);
                $this->smarty->assign('gauge3', $gauge3);
                
                /* Gauge 4 - Jobs awaiting an appointment */
                $gauge4 = array(
                                'red' => 320, /* <--- temporary will be a user default */
                                'yellow' => 240, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 40,                                    /* inc * div - max */
                                'div' => 10
                                );
                $gauge4['value'] = (float) $job_model->getJobsAverageEstimateOutsanding($filters[$tab]);
                $this->smarty->assign('gauge4', $gauge4);
                
                /* Gauge 5 - Average Days Estimates Outstanding */
                $gauge5 = array(
                                'red' => 320, /* <--- temporary will be a user default */
                                'yellow' => 240, /* <--- temporary will be a user default */
                                'min' => 0,
                                'inc' => 40,                                    /* inc * div - max */
                                'div' => 10
                                );
                $gauge5['value'] = intval($job_model->getJobsAwaitingAppointment($filters[$tab]));
                $this->smarty->assign('gauge5', $gauge5);
                


                
                break;
                
            case 'OpenTAT':
            case 'ClosedTAT':
                
                /*********
                * Gauges *
                *********/
                
                $totalMatchingJobs = $job_model->getTotalMatchingJobs($filters[$tab]);
                $this->smarty->assign('totalMatchingJobs', $totalMatchingJobs);
                /* Get guage dials dependent on tab. TODO: This is a quick and dirty fix code below should be rewritten */
                if ( $tab == 'OpenTAT') {
                    $jobsUnder7 = intval($job_model->getNumberJobsBetweenDays(0, $gaugePrefs['GaDialBoundaryLower'], $filters[$tab]));
                    $jobs714 = intval($job_model->getNumberJobsBetweenDays($gaugePrefs['GaDialBoundaryLower'], $gaugePrefs['GaDialBoundaryUpper']+1, $filters[$tab]));
                    $jobsOver14 = intval($job_model->getNumberJobsBetweenDays($gaugePrefs['GaDialBoundaryUpper']+1, 99999, $filters[$tab]));
                } else {
                    $jobsUnder7 = intval($job_model->getNumberCompletedJobsBetweenDays(0, $gaugePrefs['GaDialBoundaryLower'], $filters[$tab], $datefield));
                    $jobs714 = intval($job_model->getNumberCompletedJobsBetweenDays($gaugePrefs['GaDialBoundaryLower'], $gaugePrefs['GaDialBoundaryUpper'], $filters[$tab], $datefield));
                    $jobsOver14 = intval($job_model->getNumberCompletedJobsBetweenDays($gaugePrefs['GaDialBoundaryUpper'], 99999, $filters[$tab], $datefield));
                }
                $this->smarty->assign('DialBoundaryUpper',$gaugePrefs['GaDialBoundaryUpper']);
                $this->smarty->assign('DialBoundaryLower',$gaugePrefs['GaDialBoundaryLower']);

                if ($tab == 'OpenTAT') {
                    $jobStatusList = $Skyline->getSystemStatus();
                
                    if(count($UserStatusPreferences))
                    {
                        for($i=0;$i<count($jobStatusList);$i++)
                        {
                            $jobStatusList[$i]['Exists'] = false;

                            foreach($UserStatusPreferences as $usf)
                            {
                                if(in_array($jobStatusList[$i]['StatusID'], $usf))
                                {
                                    $jobStatusList[$i]['Exists'] = true;
                                    break;
                                }
                            }

                        }
                    } else {

                        for($i=0;$i<count($jobStatusList);$i++)
                        {
                            $jobStatusList[$i]['Exists'] = true;
                        }
                    }
                    
                    $this->smarty->assign('jobStatusList', $jobStatusList);
                } else {
                    $this->smarty->assign('jobStatusList', array());
                }
                
                /* Gauge 0 - % Jobs under 7 days */
                $gauge0 = array(
                                'red' => $gaugePrefs['GaDial0Red'],
                                'yellow' => $gaugePrefs['GaDial0Yellow'],
                                'min' => 0,
                                'inc' => 10,                                    /* inc * div = max */
                                'div' => 10
                                );
                $gauge0['value'] = ( $totalMatchingJobs == 0  ? 0 : number_format( ($jobsUnder7 / $totalMatchingJobs) * 100 , 2));
                $this->smarty->assign('gauge0', $gauge0);
                
                /* Gauge 1 - % Jobs 7 - 14 days */
                $gauge1 = array(
                                'red' => $gaugePrefs['GaDial1Red'],
                                'yellow' => $gaugePrefs['GaDial1Yellow'],
                                'min' => 0,
                                'inc' => 10,                                    /* inc * div = max */
                                'div' => 10
                                );
                $gauge1['value'] = ( $totalMatchingJobs == 0  ? 0 : number_format( $jobs714 / $totalMatchingJobs * 100 , 2));
                $this->smarty->assign('gauge1', $gauge1);

                /* Gauge 2 - % Jobs under 7 days */
                $gauge2 = array(
                                'red' => $gaugePrefs['GaDial2Red'],
                                'yellow' => $gaugePrefs['GaDial2Yellow'],
                                'min' => 0,
                                'inc' => 10,                                    /* inc * div = max */
                                'div' => 10
                                );
                $gauge2['value'] = ( $totalMatchingJobs == 0  ? 0 : number_format(  $jobsOver14 / $totalMatchingJobs * 100 , 2));
                $this->smarty->assign('gauge2', $gauge2);
            
                /* Gauge 3 - qty Jobs under 7 days */
                $guage3_inc = $gaugePrefs['GaDial3Max'] / 10;
                $gauge3 = array(
                                'red' => $gaugePrefs['GaDial3Red'],
                                'yellow' => $gaugePrefs['GaDial3Yellow'],
                                'min' => 0,
                                'inc' => $guage3_inc,                           /* inc * div = max */
                                'div' => $gaugePrefs['GaDial3Max'] / $guage3_inc
                                );
                $gauge3['value'] = intval($jobsUnder7);
                $this->smarty->assign('gauge3', $gauge3);
                
                /* Gauge 4 - qty Jobs 7 - 14 days */
                $guage4_inc = $gaugePrefs['GaDial4Max'] / 10;
                $gauge4 = array(
                                'red' => $gaugePrefs['GaDial4Red'],
                                'yellow' => $gaugePrefs['GaDial4Yellow'],
                                'min' => 0,
                                'inc' => $guage4_inc,                           /* inc * div = max */
                                'div' => $gaugePrefs['GaDial4Max'] / $guage4_inc
                                );
                $gauge4['value'] = intval($jobs714);
                $this->smarty->assign('gauge4', $gauge4);
                
                /* Gauge 5 - Qty jobs over 14 days */
                $guage5_inc = $gaugePrefs['GaDial5Max'] / 10;
                $gauge5 = array(
                                'red' => $gaugePrefs['GaDial5Red'],
                                'yellow' => $gaugePrefs['GaDial5Yellow'],
                                'min' => 0,
                                'inc' => $guage5_inc,                           /* inc * div = max */
                                'div' => $gaugePrefs['GaDial5Max'] / $guage5_inc
                                );
                $gauge5['value'] = intval($jobsOver14);
                $this->smarty->assign('gauge5', $gauge5);
                
                /***********************
                *  Service Centre List *
                ***********************/
                
                if ( ($guage == '14percent') || ($guage== '714percent')         /* Check if percent */
                      || ($guage == '7percent') ) {
                    /* Service Center red yellow settinngs */
                    if ($guage == '14percent') {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial0Red'], 'yellow' => $gaugePrefs['GaDial0Yellow']);
                    } elseif ($guage == '714percent') {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial1Red'], 'yellow' => $gaugePrefs['GaDial1Yellow']);
                    } else {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial2Red'], 'yellow' => $gaugePrefs['GaDial2Yellow']);
                    }
                     
                    $this->smarty->assign('sc_percent', 1);
                    $sc_count = "%";
                } else {
                    $sc_colour_settings = array('red' => 80, 'yellow' => 60);
                    /* Service Center red yellow settinngs */
                    if ($guage == '14') {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial3Red'], 'yellow' => $gaugePrefs['GaDial3Yellow']);
                    } elseif ($guage == '714') {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial4Red'], 'yellow' => $gaugePrefs['GaDial4Yellow']);
                    } elseif ($guage == '7') {
                        $sc_colour_settings = array('red' => $gaugePrefs['GaDial5Red'], 'yellow' => $gaugePrefs['GaDial5Yellow']);
                    } else {
                        $sc_colour_settings = array(
                                                    'red' => $gaugePrefs['GaDial5Red']+$gaugePrefs['GaDial4Red']+$gaugePrefs['GaDial3Red'], 
                                                    'yellow' => $gaugePrefs['GaDial5Yellow']+$gaugePrefs['GaDial4Yellow']+$gaugePrefs['GaDial3Yellow']
                                                   );
                    }
                    
                    $this->smarty->assign('sc_percent', 0);
                    $sc_count = "Qty";
                }
                $this->smarty->assign('sc_colour_settings', $sc_colour_settings);
                
                switch ($guage) {                                               /* Set up parameters for apporiate range required */
                    case '14percent':
                    case '14':
                        $sc_title = "Jobs Over {$gaugePrefs['GaDialBoundaryUpper']} Days";
                        $daysFrom = 99999;                                      /* 300 + years ago so "from the start" in skyline will need to be changed if used after 2300 AD !! */
                        $daysTo = $gaugePrefs['GaDialBoundaryUpper'] + 1;
                        break;
                    
                    case '714percent':
                    case '714':
                        $sc_title = "Jobs {$gaugePrefs['GaDialBoundaryLower']} to {$gaugePrefs['GaDialBoundaryUpper']} Days";
                        $daysFrom = $gaugePrefs['GaDialBoundaryUpper'] + 1;
                        $daysTo = $gaugePrefs['GaDialBoundaryLower'];
                        break;
                    
                    case '7percent':
                    case '7':
                        $sc_title = "Jobs Under {$gaugePrefs['GaDialBoundaryLower']} Days";
                        $daysFrom = $gaugePrefs['GaDialBoundaryLower'];      
                        $daysTo = 0;
                        break;
                    
                    case 'all':
                        $sc_title = "";
                        $daysFrom = 99999;      
                        $daysTo = 0;
                        break;
                }
                
                $guageonly = " AND ABS(DATEDIFF($datefield, `DateBooked`)) >= $daysTo  AND ABS(DATEDIFF($datefield, `DateBooked`)) < $daysFrom ";  /* Filter for guage only if required */
                
                $this->smarty->assign('sc_title', $sc_title);
                $this->smarty->assign('sc_count', $sc_count);
                
                if ($scorderby == 'Percent') {
                    $scpercent = true;
                    $scorderby = "";
                }
                
                switch($listdatasrc) {
                    case 'client':
                        $service_centre = $job_model->getJobsByDatesByClientCount($daysFrom, $daysTo, $filters[$tab], $scorderby);
                        break;
                    
                    case 'sp':
                        $service_centre = $job_model->getJobsByDatesByServiceCentreCount($daysFrom, $daysTo, $filters[$tab], $scorderby);
                        break;
                    
                    case 'make':
                        $service_centre = $job_model->getJobsByDatesByManufacturerCount($daysFrom, $daysTo, $filters[$tab], $scorderby);
                        break;
                    
                    case 'network':
                        $service_centre = $job_model->getJobsByDatesByNetworkCount($daysFrom, $daysTo, $filters[$tab], $scorderby);
                        break;
                    
                    default:
                        $service_centre = array();
                }

                for($n = 0; $n < count($service_centre); $n++) {
                    $service_centre[$n]['Percent'] = $service_centre[$n]['Count'] / $totalMatchingJobs * 100;
                }

                if ( isset($scpercent) && $scpercent) {
                    $service_centre = $this->sort_multi_array ($service_centre, 'Percent');
                }

                
                $this->smarty->assign('service_centre_list', $service_centre);
                
                break;

             }  /* esac $tab */
             
            /********
            * Graph *
            ********/
             
            if ( $showguage == 1 ) {
                $filters[$tab].=$guageonly;
                $this->smarty->assign('daysFrom', $daysFrom);                   /* Need to pass days from and to for data table */
                $this->smarty->assign('daysTo', $daysTo);
            }
            
            $graph_total_count = $job_model->countJobsGraphicalAnalysisFilter($filters[$tab]);
            switch ($graphdata) {
                case 'jbm':
                    $this->smarty->assign('graphtitle',' by Manufacturer' );
                    $graph = $job_model->getJobsByManufacturer($filters[$tab]);
                    break;

                case 'jbs':
                    $this->smarty->assign('graphtitle',' by Status' );
                    
                    if ($tab == 'ClosedJobs' || $tab == 'ClosedTAT') {          /* If closed show comletion status otherwise job status */
                        $graph = $job_model->getJobsByCompletionCount($filters[$tab]);
                    } else {
                        $graph = $job_model->getJobsByStatusCount($filters[$tab]);
                    
                        $graphcolour = array();
                        for ($n = 0; $n < count($graph); $n++) {                /* Loop through each graph item */
                            $graphcolour[$n] = $graph[$n]['Colour'];            /* Get the colour setting for the graph */
                            unset($graph[$n]['Colour']);                        /* Inset the graph colour so it does not appear in data */
                        }
                    }
                    break; 

                case 'jcty':
                    $this->smarty->assign('graphtitle',' by County / Unitary Authority' );
                    $graph = $job_model->getJobsByCountyCount($filters[$tab]);
                    break;

                case 'jctry':
                    $this->smarty->assign('graphtitle',' by Country' );
                    $graph = $job_model->getJobsByCountryCount($filters[$tab]);
                    break;

                case 'jeng':
                    $this->smarty->assign('graphtitle',' by Engineer' );
                    $graph = $job_model->getJobsByAppointmentEngineer($filters[$tab]);
                    $graph[] = array ( 'Name' => 'Unallocated', 'Colour' => 'CCCCCC', 'Count' => 0 );
                    $allocated = 0;                                             /* Want to keep a record of alloacted engineers so we can calculated unalloacted */
                    $graphcolour = array();
                    for ($n = 0; $n < count($graph); $n++) {                    /* Loop through each graph item */
                        $graphcolour[$n] = $graph[$n]['Colour'];                /* Get the colour setting for the graph */
                        unset($graph[$n]['Colour']);                            /* Inset the graph colour so it does not appear in data */
                        $allocated += $graph[$n]['Count'];                      /* Add ecah number of allocated engineer */
                        $this->log($allocated.'  '.$graph[$n]['Count']);
                    }
                    $graph[count($graph)-1]['Count'] = $graph_total_count - $allocated;
                    break;

                case 'jbiw':
                    $this->smarty->assign('graphtitle',' by Day Booked in Last Week' );
                    $graph = $job_model->getJobsBookedInWeekEnding(date('Y-m-d'),$filters[$tab]);
                    break;

            } /* switch $graphdata */

            if (isset($graphcolour)) $this->smarty->assign('graphcolour',$graphcolour);
            $this->smarty->assign('graph',$graph);
            $this->smarty->assign('graph_total_count',$graph_total_count);
            $this->smarty->assign('tab', $tab);
            $this->smarty->assign('filter', $filters[$tab]);

            switch($tab) {

                case 'OpenTAT':
                    $this->smarty->display('Performance/TatJobs.tpl');
                    break;

                case 'ClosedTAT':
                    $this->smarty->display('Performance/ClosedTatJobs.tpl');
                    break;
                
                default:
                    $this->smarty->display('Performance/OpenClosedJobs.tpl');
            }
        }  

    }
    
   /**
    * Geographic
    * 
    * The is to display geographic perfrormance screen
    *
    * @param array $args
    * @return void
    * @author Andrew J. Williams <Andrew.Williams@awcomputech.com>
    ********************************************/
    public function GeographicAction( $args ) {
        $branches_model = $this->loadModel('Branches'); 
        $job_model = $this->loadModel('Job');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $Skyline = $this->loadModel('Skyline');
        
        $page = $this->messages->getPage('performance', $this->lang);
        $this->smarty->assign('page', $page);
        
        $sqlOpenJobs = 'ISNULL(`ClosedDate`) 
                        AND j.`Status` != "29 JOB CANCELLED"
                        AND j.`Status` != "29 JOB CANCELLED"
                        AND j.`Status` != "26 INVOICE RECONCILED"
                        AND (ISNULL(j.CompletionStatus) OR j.CompletionStatus = "0")';
        $sqlClosedJobs = 'NOT ISNULL(`ClosedDate`)';
        
        if ( isset($args['jobs']) ) {                                           /* If jobs parameter has been passed */
            $jobs = $args['jobs'];
            if ( $jobs == "closed" ) {
                $filters = $sqlClosedJobs;
            } else {
                $filters = $sqlOpenJobs;
            }
        } else {
            $filters = $sqlOpenJobs;
            $jobs = 'open';
        }
        $this->smarty->assign('jobs', $jobs);
        
        $addWhere = "";                                                         /* Additional where clause based on passed parameters */
        $addWhereSp = "";                                                       /* As above but for service provider only queries (such as service provider capacity */
        if ( isset($args['spid']) ) {                                           /* Check parameters for specific service type */
            $addWhere = " AND sp.`ServiceProviderID` = {$args['spid']}"; 
            $addWhereSp = $addWhere;
            $spName = $service_providers_model->getServiceCentreCompanyName($args['spid']);
            $this->smarty->assign('spid', $args['spid']);
        } else {
            $spName = '';
            $this->smarty->assign('spid', 0);
        }
        $this->smarty->assign('sp_name', $spName);
        
        $ncbFilter = "";                                                        /* text for network / client / brand filter (default is blank) */
        
        if ( isset($args['nid']) ) {                                            /* Check parameters for specific network */
            $addWhere .= " AND j.`NetworkID` = {$args['nid']}"; 
            $nName = $Skyline->getNetworkName($args['nid']);
            $this->smarty->assign('nid', $args['nid']);
            $ncbFilter =  " in $nName";
        } else {
            $nName = '';
            $this->smarty->assign('nid', 0);
        }
        $this->smarty->assign('n_name', $nName);
        
        if ( isset($args['cid']) ) {                                            /* Check parameters for specific client */
            $addWhere .= " AND j.`ClientID` = {$args['cid']}"; 
            $cName = $Skyline->getClientName($args['cid']);
            $this->smarty->assign('cid', $args['cid']);
            $ncbFilter =  " in $cName";
        } else {
            $cName = '';
            $this->smarty->assign('cid', 0);
        }
        $this->smarty->assign('c_name', $cName);
        
        if ( isset($args['bid']) ) {                                            /* Check parameters for specific branch */
            $addWhere .= " AND j.`BranchID` = {$args['bid']}"; 
            $bName = $branches_model->name($args['bid']);
            $this->smarty->assign('bid', $args['bid']);
            $ncbFilter =  " in $bName";
        } else {
            $bName = '';
            $this->smarty->assign('bid', 0);
        }
        $this->smarty->assign('b_name', $bName);
        
        $this->smarty->assign('ncbFilter', $ncbFilter);

        $filters .= $addWhere;
        
        /* Filter by network / client / branch popup box */                
        $ClientID         = $this->user->ClientID;
        $BranchID         = $this->user->BranchID;
        $DefaultBranchID  = false;
        $branches         = array();

        if(!$this->user->NetworkID)
        {    
            $showNetworkDropDown = true; 
            $networks  = $Skyline->getNetworks();
            $this->smarty->assign('networks', $networks); 
            $clients   = array();
        }
        else
        {
            $showNetworkDropDown = false; 

            if($this->user->ClientID)
            {
                $clients   = array();

                $DefaultBranchID  = $Skyline->getClientDefaultBranch($this->user->ClientID);
            } 
            else
            {
                $clients  = $Skyline->getNetworkClients($this->user->NetworkID);
            }

            $this->smarty->assign('NetworkID', $this->user->NetworkID); 
        }

        if(!$BranchID && !$DefaultBranchID && $ClientID)
        {
            $branches  = $Skyline->getBranches(null, null, $ClientID, $this->user->NetworkID);
        }             

        $this->smarty->assign('ClientID', $ClientID); 
        $this->smarty->assign('BranchID', $BranchID);
        $this->smarty->assign('DefaultBranchID', $DefaultBranchID);


        $this->smarty->assign('clients', $clients); 
        $this->smarty->assign('branches', $branches); 

        $this->smarty->assign('showNetworkDropDown', $showNetworkDropDown); 
                        
        if (!isset($this->session->UserID))                                     /* */
        {
            $this->redirect('index'); 
        }
        
        $this->smarty->assign('service_centre_repair_capacities', $service_providers_model->getServiceProviderCapacity());
        
        $tab = 'Geographic';   
        $this->smarty->assign('graphtitle',' by County / Unitary Authority' );
        $countries = $job_model->getCountrysByJobs();
        $this->smarty->assign('countries',$countries);
        $graph_country = $job_model->getJobsByCountryCount($filters);

        $graph_county = $job_model->getJobsByCountyCount($filters);

        if ( isset($args['map']) ) {                                            /* Check if graph parameter has been passed */
            $map = $args['map'];
        } else {
            if (count($countries) == 1) {
                $map = $countries[0]['InternationalCode'];                                             
            } else {
                $map = "150";
            }
        }
        $this->smarty->assign('map', $map);

        $this->smarty->assign('graph_country',$graph_country);
        $this->smarty->assign('graph_county',$graph_county);

        $this->smarty->assign('tab', 'Geographic');

        $this->smarty->display('Performance/Geographic.tpl');

    }


    public function SegmentationAction ( /* $args */ ) {
        
        $page = $this->messages->getPage('performance', $this->lang);
        $this->smarty->assign('page', $page); 
 
        //$data = $this->model->getSegmentationSummary('2012-11-30');
        $data = $this->model->getSegmentationSummary();
        
        $this->smarty->assign('data', $data); 
        
        $this->smarty->assign('tab', 'Segmentation'); 
        $this->smarty->display('Performance/SegmentationSummary.tpl');       
    }
    
    public function SegmentationDetailAction ( $args ) {
        
        $page = $this->messages->getPage('performance', $this->lang);
        $this->smarty->assign('page', $page);
        
        $data = $this->model->getSegmentationSummary();
        
        $this->smarty->assign('data', $data); 
        
        $this->smarty->display('Performance/SegmentationDetail.tpl');
    }
    
   /**
    * jobsTableAction
    * 
    * Output matching jobs according to criteria for datatable
    *
    * @param array $args
    * @return void
    * @author Andrew J. Williams <a.williams@pccsuk.com>
    ***************************************************************************/
    
    public function jobsTableAction( $args ) { 
        $job_model = $this->loadModel('Job');

        $output = $job_model->fetchOpenTAT($args);
        echo json_encode( $output );
     }
     
   /**
    * jobsTableExport
    * 
    * Export matching jobs (from Graophical Analysis Page) to Excel 
    *
    * @param array $args    Arguments 
    * @return void
    * @author Andrew J. Williams <a.williams@pccsuk.com>
    **************************************************************************/
    
    public function jobsTableExportAction( $args ) { 
        $job_model = $this->loadModel('Job');
        
        if (isset($args[0]) ) {
            $fName = $args[0];
        } else {
            $fName = 'OpenTAT';
        }
        $fName .='_'.date('Y-m-d');
           
        if(isset($args['openby'])) {                                            /* Get which perception of closed by we need */                              
            if ($args['openby'] == 'branch') {                                  /* Branch selected */
                if ($args[0] == 'ClosedTAT') {                                  /* Closed Jobs */
                    $addWhere = "NOT ISNULL(`ClosedDate`)";
                    $datefield = '`ClosedDate`';                                /* Field in database we check for closed */
                } else {                                                        /* Open Jobs */
                    $filter = "ISNULL(`ClosedDate`)";
                }
                $fName .= '_branch';
            } else {                                                            /* Otherwise service provider */
                if ($args[0] == 'ClosedTAT') {                                  /* Closed Jobs */
                    $addWhere = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
                    $datefield = '`ServiceProviderDespatchDate`';               /* Field in database we check for closed */
                } else {                                                        /* Open Jobs */
                    $filter = "ISNULL(`ServiceProviderDespatchDate`)";
                }
                $fName .= '_serviceprovider';
            }
        } else {
            if ($args[0] == 'ClosedTAT') {                                      /* Closed Jobs */
                $addWhere = 'NOT ISNULL(`ServiceProviderDespatchDate`)';
                $datefield = '`ServiceProviderDespatchDate`';                   /* Field in database we check for closed */
            } else {                                                            /* Open Jobs */
                $addWhere = "ISNULL(`ServiceProviderDespatchDate`)";
            }
            $fName .= '_branch';
        }

        if ( isset($args['spid']) ) {                                           /* Check parameters for specific service type */
            $addWhere .= " AND sp.`ServiceProviderID` = {$args['spid']}"; 
            $fName .= '_sp='.$args['spid'];
        }        
        
        if ( isset($args['nid']) ) {                                            /* Check parameters for specific network */
            $addWhere .= " AND j.`NetworkID` = {$args['nid']}"; 
            $fName .= '_n='.$args['nid'];
        }
        
        if ( isset($args['cid']) ) {                                            /* Check parameters for specific client */
            $addWhere .= " AND j.`ClientID` = {$args['cid']}"; 
            $fName .= '_c='.$args['cid'];
        }
        
        if ( isset($args['bid']) ) {                                            /* Check parameters for specific branch */
            $addWhere .= " AND j.`BranchID` = {$args['bid']}"; 
            $fName .= '_b='.$args['bid'];
        }
                
        if ( isset($args['daysFrom']) && isset($args['daysTo']) )  {            /* Guage (days) Filter */
            $addWhere .= " AND j.`DateBooked` BETWEEN DATE_SUB(NOW(), INTERVAL {$args['daysFrom']} DAY) AND DATE_SUB(NOW(), INTERVAL {$args['daysTo']} DAY)";
        }

        switch ($args['display']) {
            case 'ytd':
                $addWhere .= " AND YEAR(j.$datefield`) = YEAR(CURDATE())";
                $fName .= '_ytd';
                break;

            case 'lm':
                $addWhere .= " AND YEAR(j.$datefield) = YEAR(CURDATE() - INTERVAL 1 MONTH) AND MONTH(j.$closedField) = MONTH(CURDATE() - INTERVAL 1 MONTH)";
                $fName .= '_lm';
                break;

            case '30':
                $addWhere .= " AND j.$datefield > DATE_ADD(NOW(), INTERVAL -30 DAY)";
                $fName .= '_30';
                break;
            
            case 'dr':
                $addWhere .= " AND j.$datefield >= '{$args['datefrom']}' AND j.$datefield <= '{$args['dateto']}'";
                $fName .= '_dr='.$args['datefrom'].'+'.$args['dateto'];
                break;
            
            case 'all':
                /* Nothing ! */
                break;

            default: /* mtd */
                $addWhere .= " AND YEAR(j.$datefield) = YEAR(CURDATE()) AND MONTH(j.$datefield) = MONTH(CURDATE())";
                $fName .= '_mtd';
        }
        
        if ($args[0] == 'ClosedTAT') {
            $output = $job_model->exportClosedJobsGraphicalAnalysis($addWhere, $datefield);
        } else {
            $output = $job_model->exportJobsGraphicalAnalysis($addWhere);
        }
        
        $csv = $this->array_to_csv($output);                                      /* Convert to CSV */

        header('Content-disposition: attachment; filename='.$fName.'.csv');
        header("Content-type: application/vnd.ms-excel");
        echo $csv;
        echo ",,,";
     } 
     
    /*
     * http://stackoverflow.com/questions/96759/how-do-i-sort-a-multidimensional-array-in-php
     **************************************************************************/
    private function sort_multi_array ($array, $key)
    {
        $keys = array();
        for ($i=1;$i<func_num_args();$i++) {
            $keys[$i-1] = func_get_arg($i);
        }

        // create a custom search function to pass to usort
        $func = function ($a, $b) use ($keys) {
            for ($i=0;$i<count($keys);$i++) {
                if ($a[$keys[$i]] != $b[$keys[$i]]) {
                    return ($a[$keys[$i]] < $b[$keys[$i]]) ? -1 : 1;
                }
            }
            return 0;
        };

        usort($array, $func);

        return $array;
    }
    
   /**
    * Generatting CSV formatted string from an array.
    * By Sergey Gurevich.
    ****************************************************************************/
    private function array_to_csv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"')
    {
        if (!is_array($array) or !is_array($array[0])) return false;
        $output = "";
        //Header row.
        if ($header_row)
        {
            foreach ($array[0] as $key => $val)
            {
                //Escaping quotes.
                $key = str_replace($qut, "$qut$qut", $key);
                $output .= "$col_sep$qut$key$qut";
            }
            $output = substr($output, 1)."\n";
        }
        //Data rows.
        foreach ($array as $key => $val)
        {
            $tmp = '';
            foreach ($val as $cell_key => $cell_val)
            {
                //Escaping quotes.
                $cell_val = str_replace($qut, "$qut$qut", $cell_val);
                $tmp .= "$col_sep$qut$cell_val$qut";
            }
            $output .= substr($tmp, 1).$row_sep;
        }

        return $output;
    }
    
}

?>
