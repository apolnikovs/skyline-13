{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page_title|escape:'html'}
    {$PageId = $JobBookingPage}
{/block}


{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
   
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
   
   <script>
    $(function() {
        
        $('#jbDateOfPurchaseElement .ui-combobox-input').css('width', '80px');
        
    });
    </script>

    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
{/block}


{block name=scripts}
  
   {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
   {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
   {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
   <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

   <script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>

<script type="text/javascript">

    /*
    $(document).on("change", "#jbUnitTypeIDElement input", function() {
	
	$.post("{$_subdomain}/Job/getRARequired", { id: $(this).val() }, function(response) {
	    if(response == 1) {
		$("#jbAuthorisationNoElement").hide();
	    } else {
		$("#jbAuthorisationNoElement").show();
	    }
	});
    
    });
    */
   
   {********************** Book Collection Handling *****************}
   function BookCollectionFormData() {

        this.CollectionDate = '';
        this.PreferredCourier = '';
        this.ConsignmentNo = '';
        this.Name = $('#jbColAddCompanyName').val();;
        this.Postcode = $('#jbColAddPostalCode').val();
        this.BuildingName = $('#jbColAddBuildingName').val();
        this.Street = $('#jbColAddStreet').val();
        this.Area = $('#jbColAddArea').val();
        this.Town = $('#jbColAddCity').val();
        this.County = '';
        this.Country = '';
        this.Email = $('#jbColAddEmail').val();
        this.Phone = $('#jbColAddPhone').val(); 
        this.PreferredCourierID = null;
        this.CountyID = $('#jbColAddCounty').val();
        this.CountryID = $('#jbColAddCountry').val();
    
        this.update = function() {
            
            // Note: Book Collection Form no longer allows to update
            //       collection address.  So do not copy values back to
            //       job booking form.
            //
            /*$('#jbColAddCompanyName').val(this.Name);
            $('#jbColAddPostalCode').val(this.Postcode);
            $('#jbColAddBuildingName').val(this.BuildingName);
            $('#jbColAddStreet').val(this.Street);
            $('#jbColAddArea').val(this.Area);
            $('#jbColAddCity').val(this.Town);
            $('#jbColAddEmail').val(this.Email);
            $('#jbColAddPhone').val(this.Phone);
            $('#jbColAddCounty').val(this.CountyID);
            setValue("#jbColAddCounty");
            $('#jbColAddCountry').val(this.CountryID);
            setValue("#jbColAddCountry");*/

        };
   }
   
   $(document).on('click', '#BookCollectionButton', 
            function() {
                $.colorbox({
                    href :	'{$_subdomain}/Courier/BookCollectionForm', 
                    width:	'700px',
                    scrolling:	false,
                    overlayClose: false,
                    fixed: true
                });
                return false;
            }
    );
    {********************** Book Collection End **********************}
    
    function enableManualAddress() {
	setTimeout(function() { 
	    $("#jbColAddPostalCode,#jbColAddCompanyName,#jbColAddBuildingName,#jbColAddStreet,#jbColAddArea,#jbColAddCity,#jbColAddEmail,#jbColAddPhone,#ColAddPhoneExt").removeAttr("disabled");
	    $("#jb_p_c_a_county, #jb_p_c_a_country").find("input").each(function() {
		$(this).removeAttr("disabled");
	    });
	    $("#jb_p_c_a_county, #jb_p_c_a_country").find("a").each(function() {
		$(this).css("display","block");
	    });
	},500)
    }


    function disableManualAddress() {
	setTimeout(function() { 
	    $("#jbColAddPostalCode,#jbColAddCompanyName,#jbColAddBuildingName,#jbColAddStreet,#jbColAddArea,#jbColAddCity,#jbColAddEmail,#jbColAddPhone,#ColAddPhoneExt").attr("disabled","disabled");
	    $("#jb_p_c_a_county, #jb_p_c_a_country").find("input, a").each(function() {
		$(this).attr("disabled","disabled");
	    });
	    $("#jb_p_c_a_county, #jb_p_c_a_country").find("a").each(function() {
		$(this).css("display","none");
	    });
	},500)
    }


    $(document).on("click", "#isProductAtBranchYes, #isProductAtBranchNo", function() {
	enableManualAddress();
    });


      $.validator.addMethod('noFutureDate', function() {
        
        if($("#DateOfPurchase").val()!='')
        {
                var str1 = $("#DateOfPurchase").val();
                if(str1.length==10)
                {    
                    var dt1  = str1.substring(0,2);
                    var mon1 = str1.substring(3,5);
                    var yr1  = str1.substring(6,10);
                    var sltDate = yr1+mon1+dt1;

                    sltDate = parseInt(sltDate);

                   // alert(sltDate);

                    if(sltDate<={$currentDate})
                    {
                      //alert("correct");
                        return true;

                    }  
                    else
                    {
                      //  alert("wrong");
                        return false;
                    }
                }
                else
                {

                    return false;
                }
        }
        else
        {

             return true;
        }
        
    }, "{$page['Errors']['purchase_date_valid']|escape:'html'}"); 
      
      
      
      //This function assigns the data to the elements on Catalogue no change  -- starts here....
       function assignDetails ($rowData)
        {
               
               var $pickDataIdList = new Array('#jbHiddenStockCode', '#jbModelName', '#jbProductDescription', '#jbUnitTypeID', '#jbManufacturerID');

                for($i=0;$i<$pickDataIdList.length;$i++)
                {
                    $($pickDataIdList[$i]).val($rowData[$i]).removeAttr('disabled').removeClass("auto-hint");
                    
                    if($pickDataIdList[$i]!='#jbModelName')
                    {
                        $($pickDataIdList[$i]).blur();
                    }
                    
                    if($pickDataIdList[$i]=='#jbManufacturerID' || $pickDataIdList[$i]=='#jbUnitTypeID')
                        {
                            setValue($pickDataIdList[$i]);
                            
                            if($pickDataIdList[$i]=='#jbUnitTypeID' && $('#jbUnitTypeID').val()=='-1')
                            {
                                  $('#RequestNewUnitType').trigger('click');  
                            }
                        }
                }
        }
       //This function assigns the data to the elements on Catalogue no change -- ends here..      
       
       
       
       //This function assigns the data to the elements on model no change  --starts here....
       function assignModelDetails ($rowData)
        {
               
               var $pickDataIdList = new Array('#jbProductDescription', '#jbUnitTypeID', '#jbManufacturerID');

                for($i=0;$i<$pickDataIdList.length;$i++)
                {
                    $($pickDataIdList[$i]).val($rowData[$i]).removeAttr('disabled');
                    $($pickDataIdList[$i]).blur();
                    
                    if($pickDataIdList[$i]=='#jbManufacturerID' || $pickDataIdList[$i]=='#jbUnitTypeID')
                        {
                           
                            setValue($pickDataIdList[$i]);
                            
                            if($pickDataIdList[$i]=='#jbUnitTypeID' && $('#jbUnitTypeID').val()=='-1')
                            {
                                  $('#RequestNewUnitType').trigger('click');  
                            }
                            
                        }
                }
        }
       //This function assigns the data to the elements on model no change -- ends here..
       
       
       
       
       //This function assigns Service Provider details from popup search from. -- starts here....
       function assignServiceProvider ($rowData)
        {

               var $pickDataIdList = new Array('#ServiceProviderID', '#ServiceProvideName', '#product_location_label');

                $($pickDataIdList[0]).val($rowData[0]);
                $($pickDataIdList[1]).html($rowData[1]).css({ "color":"red", "font-size":"" });
                 
                if($("[name='ProductLocation']").val()=="Service Provider")
                {
                    
                
                
                
                         $.post("{$_subdomain}/Data/ServiceProviderAddress/"+$rowData[0],        

                            '',      
                            function(data){
                                
                                var p = eval("(" + data + ")");

                                $($pickDataIdList[2]).html(p).css({ "color":"red", "font-size":"" });
                            
                            }); //Post ends here...
                
                
                }  
                 
                $("#ServiceProvidersPage").fadeOut();
                $("#jobConfirmation").fadeIn("slow");

                $(this).colorbox.resize({
                    height: ($('#jobConfirmation').height()+150)+"px"
                });
                
        }
       //This function assigns Service Provider details from popup search from. -- ends here..      
       
       
       
       
       
      
       
      
      
       $(document).ready(function() {
       
       
	    var jbReportedFaultOptions = {  
		"maxCharacterSize": 2000,  
		"originalStyle":    "jobBookingTextCount",  
		"warningStyle":	    "warningDisplayInfo",  
		"warningNumber":    1800,  
		"displayFormat":    "#input Characters | #left Characters Left | #words Words"
	    };
	    $("#jbReportedFault").textareaCount(jbReportedFaultOptions, function() {
		if($("#jbReportedFault").val().length > 200) {
		    $("#jbReportedFault").css("color", "red");
		} else {
		    $("#jbReportedFault").css("color", "inherit");
		}
	    });  
       
       
            $(document).on("click", "#clearORFields", function() {
                $("#OriginalRetailerPart1, #OriginalRetailerPart2, #OriginalRetailerPart3").val("");
                return false;
            });
            
            
            $(document).on("click", "#deleteOriginalRetailer", function() {
                
                
                
                if(confirm("{$page['Errors']['retailer_delete_confirm']|escape:'html'}"))
                {
                        
                         $.post("{$_subdomain}/Data/saveOriginalRetailer/BranchID="+$("#jbBranchID").val()+"/",        

                            { RetailerName: $( "#OriginalRetailerFullPart" ).val() },   
                                
                            function(data){
                                // DATA NEXT SENT TO COLORBOX
                                var p = eval("(" + data + ")");
                                if(p)
                                {
                                       $( "#OriginalRetailerFullPart" ).val('');
                                       $( "#OriginalRetailerFullPart" ).focus();
                                }
                                
                                
                            }); //Post ends here...
                        
                }
                
                return false;
                
            });
            
            
            
       
                    // Uppercase every first letter of a word
                        jQuery.fn.ucwords = function() {
                          return this.each(function(){
                            var val = $(this).val(), newVal = '';
                            val = val.split(' ');

                            for(var c=0; c < val.length; c++) {
                              newVal += val[c].substring(0,1).toUpperCase() + val[c].substring(1,val[c].length).toLowerCase() + (c+1==val.length ? '' : ' ');
                            }
                           
                            $(this).val(newVal);
                          });
                        }

       
       
            



                      //click handler for job types radio buttons starts here.
                      $(document).on('click', "[name=JobTypeID]", 
                            function() {

                                $stDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';

                                var $JobTypeID = $(this).val();
                                
                              
                                if($JobTypeID && $JobTypeID!='')
                                    {
                                       // $.post("{$_subdomain}/Data/getServiceTypes/"+urlencode($JobTypeID)+"/{$jb_datarow.NetworkID|escape:'html'}/{$jb_datarow.BrandID|escape:'html'}/{$jb_datarow.ClientID|escape:'html'}/",        
                                        $.post("{$_subdomain}/Data/getServiceTypes/"+urlencode($JobTypeID)+"/{$jb_datarow.NetworkID|escape:'html'}//{$jb_datarow.ClientID|escape:'html'}/",         
                                        '',      
                                        function(data){
                                                var $jtServiceTypes  = eval("(" + data + ")");

                                                if($jtServiceTypes)
                                                {
                                                    for(var $i=0;$i<$jtServiceTypes.length;$i++)
                                                    {
                                                        if($jtServiceTypes[$i]['ServiceTypeID'] == "{$jb_datarow.ServiceTypeID}")
                                                            var condition = "selected='selected'";
                                                        else
                                                            var condition = "";
                                                        $stDropDownList += '<option value="'+$jtServiceTypes[$i]['ServiceTypeID']+'" '+condition+'>'+$jtServiceTypes[$i]['Name']+'</option>';
                                                    }
                                                }

                                                $("#jbServiceTypeID").html($stDropDownList);

                                        });

                                    }

                            });        
                     //click handler for job types radio buttons ends here. 
                     
                     
                     
                     
                     
                     
                     //click handler for customer type radio buttons starts here.
                      $(document).on('click', "[name=CustomerType]", 
                            function() {

                                 var $CustomerType = $(this).val();
                                
                              
                                    if($CustomerType=='Business')
                                    {

                                          $("#jbCompanyName").removeAttr("disabled"); 
                                          $("#jbCompanyNameElement").show();              
                                          $("#jbContactTitleLabel").html("{$page['Labels']['contact']|escape:'html'} {$page['Labels']['name']|escape:'html'}:<sup>*</sup>");  
                                          $("#jbContactFirstNameLabel").html("{$page['Labels']['business']|escape:'html'} {$page['Labels']['forename']|escape:'html'}:"); 
                                          $("#jbContactLastNameLabel").html("{$page['Labels']['business']|escape:'html'} {$page['Labels']['surname']|escape:'html'}:<sup>*</sup>"); 
                                          
                                          $("#jbContactEmailLabel").html('{$page['Labels']['business']|escape:'html'} {$page['Labels']['email']|escape:'html'}:<sup id="jbContactEmailSup" >*</sup>'); 
                                          $("#jbContactHomePhoneLabel").html('{$page['Labels']['business']|escape:'html'} {$page['Labels']['daytime_phone']|escape:'html'}:');
                                          $("#jbContactMobileLabel").html('{$page['Labels']['business']|escape:'html'} {$page['Labels']['mobile_no']|escape:'html'}:<sup id="jbContactMobileSup" >*</sup>');
                                          $("#jbPostalCodeLabel").html('{$page['Labels']['business']|escape:'html'} {$page['Labels']['postcode']|escape:'html'}:<sup>*</sup>');
                                          
                                          $("#jbColAddCompanyNameLabel").html('{$page['Labels']['business_name']|escape:'html'}:');
                                            
                                    }
                                    else
                                    {
                                        
                                        $("#jbCompanyName").attr("disabled", "disabled"); 
                                        $("#jbCompanyNameElement").hide(); 
                                        $("#jbContactTitleLabel").html("{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['name']|escape:'html'}:<sup>*</sup>");  
                                        $("#jbContactFirstNameLabel").html("{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['forename']|escape:'html'}:"); 
                                        $("#jbContactLastNameLabel").html("{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['surname']|escape:'html'}:<sup>*</sup>"); 
                                        
                                        
                                        $("#jbContactEmailLabel").html('{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['email']|escape:'html'}:<sup id="jbContactEmailSup" >*</sup>'); 
                                        $("#jbContactHomePhoneLabel").html('{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['daytime_phone']|escape:'html'}:');
                                        $("#jbContactMobileLabel").html('{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['mobile_no']|escape:'html'}:<sup id="jbContactMobileSup" >*</sup>');
                                        $("#jbPostalCodeLabel").html('{$page['Labels']['consumer']|escape:'html'} {$page['Labels']['postcode']|escape:'html'}:<sup>*</sup>');
                                        
                                        $("#jbColAddCompanyNameLabel").html('{$page['Labels']['customer_name']|escape:'html'}:');
                                            
                                    }

                            });        
                     //click handler for customer type radio buttons ends here. 
                     
                     
                     
                     
       
       
                    /* Add a click handler to the find servicer button of job booking form starts here*/  

                    $(document).on('click', '#find_service_centre_btn', 
                            function() {
                            
                            
                            $url_string = "NetworkID="+urlencode($("#jbNetworkID").val())+"/ClientID="+urlencode($("#jbClientID").val())+"/CountryID="+urlencode($("#jbCountry").val());
                            
                            $url_string += "/ManufacturerID="+urlencode($("#jbManufacturerID").val())+"/JobTypeID="+urlencode($("#jbJobTypeID").val());
                            $url_string += "/ServiceTypeID="+urlencode($("#jbServiceTypeID").val())+"/UnitTypeID="+urlencode($("#jbUnitTypeID").val());
                            $url_string += "/CountyID="+urlencode($("#jbCounty").val())+"/Town="+urlencode($("#jbCity").val());
                            
                             $.post("{$_subdomain}/Data/findServiceCentre/"+$url_string,        

                                    '',      
                                    function(data){
                                          
                                           var p = eval("(" + data + ")");

                                           alert("Service Centre ID: "+p);
                                           
                                          

                                    });

                            
                            
                            
                            });
                     /* Add a click handler to the find servicer button of job booking form ends here*/            
       
       
                    //Click handler for relaod branch list button - starts here..    
                    $(document).on('click', '#jbBranchesReload', 
                                            function() {
                                            
                                            $branchDropDownList = '<option value="" selected="selected">Select from drop down</option>';   
                                            
                                            //Getting branches  for selected client and network.   
                                            $.post("{$_subdomain}/Data/getBranches/"+urlencode($("#jbClientID").val())+"/"+urlencode($("#jbNetworkID").val()),        

                                            '',      
                                            function(data){
                                                    var $clientBranches = eval("(" + data + ")");

                                                    if($clientBranches)
                                                    {
                                                        for(var $i=0;$i<$clientBranches.length;$i++)
                                                        {

                                                            $branchDropDownList += '<option value="'+$clientBranches[$i]['BranchID']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                        }
                                                    }

                                                    $("#jbBranchID").html('');
                                                    $("#jbBranchID").html($branchDropDownList);
                                                    
                                                    $( "#jbBranchID" ).combobox({ change: function() {  
                                
                                                        $sltBranchID = $("#jbBranchID").val();

                                                        fillCollectionAddress($sltBranchID);

                                                    } });

                                            });

                                            
                                            $('#jbBranchesReload').attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/loader.gif").delay(2000).queue(function(nxt) {
                                                
                                                $('#jbBranchesReload').delay(1000).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/refresh-icon.png");
                                                
                                                 $(this).dequeue();
                                          });





                                            

                                               
                                            });  
                       //Click handler for relaod branch list button - ends here..                          
       
       
       
       
       
                        //Click handler for relaod branch list (top one) button - starts here..    
                    $(document).on('click', '#jbBranchesReload1', 
                                            function() {
                                            
                                            $branchDropDownList = '<option value="" selected="selected">Select from drop down</option>';   
                                            
                                            //Getting branches  for selected client and network.   
                                            $.post("{$_subdomain}/Data/getBranchAddress/ClientID="+urlencode($("#jbClientID").val())+"/NetworkID="+urlencode($("#jbNetworkID").val()),        

                                            '',      
                                            function(data){
                                                    var $clientBranches = eval("(" + data + ")");

                                                    if($clientBranches)
                                                    {
                                                        for(var $i=0;$i<$clientBranches.length;$i++)
                                                        {

                                                            $branchDropDownList += '<option value="'+$clientBranches[$i]['BuildingNameNumber']+';;'+$clientBranches[$i]['Street']+';;'+$clientBranches[$i]['LocalArea']+';;'+$clientBranches[$i]['TownCity']+';;'+$clientBranches[$i]['CountyID']+';;'+$clientBranches[$i]['CountryID']+';;'+$clientBranches[$i]['PostalCode']+';;'+$clientBranches[$i]['ContactPhone']+';;'+$clientBranches[$i]['ContactPhoneExt']+';;'+$clientBranches[$i]['ContactEmail']+';;'+$clientBranches[$i]['BranchName']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                                        }
                                                    }

                                                    $("#jbBranchAddress").html('');
                                                    $("#jbBranchAddress").html($branchDropDownList);
                                                    
                                                    $("#jbBranchAddress").combobox({ change: function() { autoFillBranchAddress() } });  

                                            });

                                            
                                            $('#jbBranchesReload1').attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/loader.gif").delay(2000).queue(function(nxt) {
                                                
                                                $('#jbBranchesReload1').delay(1000).attr("src", "{$_subdomain}/css/Skins/{$_theme}/images/refresh-icon.png");
                                                
                                                 $(this).dequeue();
                                          });

                                                $("#jbFreeTextAddress").trigger("click");

                                               
                                            });  
                       //Click handler for relaod branch list (top one) button - ends here..                          
       
                       
       
       
       
       
                     //No email feature on job confirmation page - starts here..
                       $(document).on('click', '#email_link', 
                                               function() {


                                                   $("#noEmailText").hide();
                                                   $("#noEmailTextBox").show();
                                                   $("#jcContactEmail").focus();
                                                   return false;

                                               });

                                
                        $(document).on('click', '#save_address_link', 
                                                 function() {
                                                 
                                                     if(validateEmail($("#jcContactEmail").val()))
                                                     {
                                                         var $newText = $("#jcContactEmail").val()+"<br><span class='fieldValueSub' >{$page['Text']['email_text_on_jc_page']|escape:'html'}</span>";    

                                                         $("[name='ContactEmail']").val($("#jcContactEmail").val());

                                                         $("#jcEmailElement").html($newText);

                                                     }
                                                     else if($("#jcContactEmail").val())
                                                     {
                                                        $('#noEmailTextBoxError').show();
                                                     }
                                                     else 
                                                     {
                                                     
                                                         var $newText = '<span class="fieldValueSub" style="color:red;" id="noEmailText" >{$page['Text']['no_email_text1']|escape:'html'}<br><a href="#" id="email_link" style="text-decoration:underline;" >{$page['Text']['no_email_text2']|escape:'html'}</a> {$page['Text']['no_email_text3']|escape:'html'}</span>';    

                                                        $newText += '<span class="fieldValueSub" id="noEmailTextBox" style="display:none;" >'
                                                        $newText += '<input  type="text" name="jcContactEmail" class="text auto-hint"  value="" id="jcContactEmail" >'
                                                        $newText += '&nbsp;<a href="#" id="save_address_link" style="color:green;text-decoration:underline;" >{$page['Buttons']['save_address']|escape:'html'}</a>';
                                                        $newText += '</span><label class="fieldError" id="noEmailTextBoxError" style="display:none;"  >{$page['Errors']['valid_email']|escape:'html'}</label>';                   


                                                         $("#jcEmailElement").html($newText);
                                                     }
                                                     return false;

                                                 });
                     //No email feature on job confirmation page - ends here..                            
       
       
       
       
                     {if $jb_datarow.ReferralNumberRequired} 
                 
                        var $ReferralNumberRequired = true;
                        
                        $("#jbReferralNoElement").removeClass('not-compulsory');
                        
                    {else}
                        
                       var $ReferralNumberRequired = false;  
                       
                       $("#jbReferralNoElement").addClass('not-compulsory');
                        
                    {/if}
       
                   
                    /* Add a click handler to the confirm booking of job booking form starts here*/  

                    $(document).on('click', '#confirm_booking_btn', 
                            function() {
                            
                                      

                                      $('#jbContactHomePhone').blur();
                                      $('#ContactHomePhoneExt').blur();
                                      $('#jbContactMobile').blur();  
                                      
                                      
                                      if($("#DateOfPurchase").val()=="dd/mm/yyyy")
                                      {
                                          $("#DateOfPurchase").val('');
                                      }
                                      
                                      if($("#jbContactMobile").val()=="{$page['Text']['mobile']|escape:'html'}")
                                      {
                                                $("#jbContactMobile").val('');
                                      }
                                      
                                      
                                      
                                      if($("#jbProductDescription").val()=="{$page['Text']['text_description_title']|escape:'html'}")
                                      {
                                                $("#jbProductDescription").val('');
                                      }
                                      
                                      if($("#jbModelName").val()=="{$page['Text']['model_number_title']|escape:'html'}")
                                      {
                                                $("#jbModelName").val('');
                                      }
                                      
                                     
                                     //Validating and posting form data.
                                      
                                       $('.auto-hint').each(function() {
                                                    $this = $(this);
                                                    if ($this.val() == $this.attr('title')) {
                                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                                        if ($this.hasClass('auto-pwd')) {
                                                            $this.prop('type','password');
                                                        }
                                                    }
                                        } );
                                      
                                      
                                        //If the branch is not selected then we are assing default branch id.
                                         if($("#jbBranchID").val()=="-1" || $("#jbBranchID").val()=='')
                                         {
                                                $("#jbBranchID").val($("#jbDefaultBranchID").val());
                                         }
                                      
                                      
                                        if($("#jbProductLocation").val()=='Branch')
                                        {
                                                $("#jbColAddPostalCode").removeAttr("disabled");
                                                $("#postSelect_1").removeAttr("disabled");
                                                $("#jbColAddStreet").removeAttr("disabled");
                                                $("#jbColAddCity").removeAttr("disabled");
                                                $("#jbColAddCountry").removeAttr("disabled"); 
                                                $("#jbColAddEmail").removeAttr("disabled"); 
                                                $("#jbColAddPhone").removeAttr("disabled"); 
                                                $("#ColAddPhoneExt").removeAttr("disabled");
                                        }
                                        
                                        
                                       {if $jb_settings.CustomerTypeChk2 eq 1} var $CustomerTypeChk2 = true; {else} var $CustomerTypeChk2 = false; {/if}
                                       {if $jb_settings.ContactTitleChk2 eq 1} var $ContactTitleChk2 = true; {else} var $ContactTitleChk2 = false; {/if}
                                       {if $jb_settings.BranchIDAddressChk2 eq 1} var $BranchIDAddressChk2 = true; {else} var $BranchIDAddressChk2 = false; {/if}
                                       {if $jb_settings.ContactEmailChk2 eq 1} var $ContactEmailChk2 = true; {else} var $ContactEmailChk2 = false; {/if}
                                       {if $jb_settings.ContactHomePhoneChk2 eq 1} var $ContactHomePhoneChk2 = true; {else} var $ContactHomePhoneChk2 = false; {/if}
                                       {if $jb_settings.ContactMobileChk2 eq 1} var $ContactMobileChk2 = true; {else} var $ContactMobileChk2 = false; {/if}   
                                       {if $jb_settings.ContactAltMobileChk2 eq 1} var $ContactAltMobileChk2 = true; {else} var $ContactAltMobileChk2 = false; {/if}
                                       {if $jb_settings.PostalCodeChk2 eq 1} var $PostalCodeChk2 = true; {else} var $PostalCodeChk2 = false; {/if}
                                       {if $jb_settings.CompanyNameChk2 eq 1} var $CompanyNameChk2 = true; {else} var $CompanyNameChk2 = false; {/if}
                                       {if $jb_settings.BuildingNameNumberChk2 eq 1} var $BuildingNameNumberChk2 = true; {else} var $BuildingNameNumberChk2 = false; {/if}
                                       {if $jb_settings.StreetChk2 eq 1} var $StreetChk2 = true; {else} var $StreetChk2 = false; {/if}
                                       {if $jb_settings.LocalAreaChk2 eq 1} var $LocalAreaChk2 = true; {else} var $LocalAreaChk2 = false; {/if}
                                       {if $jb_settings.TownCityChk2 eq 1} var $TownCityChk2 = true; {else} var $TownCityChk2 = false; {/if}
                                       {if $jb_settings.CountyIDChk2 eq 1} var $CountyIDChk2 = true; {else} var $CountyIDChk2 = false; {/if}
                                       {if $jb_settings.CountryIDChk2 eq 1} var $CountryIDChk2 = true; {else} var $CountryIDChk2 = false; {/if}
                                       {if $jb_settings.ProductLocationChk2 eq 1} var $ProductLocationChk2 = true; {else} var $ProductLocationChk2 = false; {/if}
                                       {if $jb_settings.ColBranchAddressIDChk2 eq 1} var $ColBranchAddressIDChk2 = true; {else} var $ColBranchAddressIDChk2 = false; {/if}
                                       {if $jb_settings.isProductAtBranchChk2 eq 1} var $isProductAtBranchChk2 = true; {else} var $isProductAtBranchChk2 = false; {/if}
                                       {if $jb_settings.ColAddPostcodeChk2 eq 1} var $ColAddPostcodeChk2 = true; {else} var $ColAddPostcodeChk2 = false; {/if}
                                       {if $jb_settings.ColAddCompanyNameChk2 eq 1} var $ColAddCompanyNameChk2 = true; {else} var $ColAddCompanyNameChk2 = false; {/if}
                                       {if $jb_settings.ColAddBuildingNameNumberChk2 eq 1} var $ColAddBuildingNameNumberChk2 = true; {else} var $ColAddBuildingNameNumberChk2 = false; {/if}
                                       {if $jb_settings.ColAddStreetChk2 eq 1} var $ColAddStreetChk2 = true; {else} var $ColAddStreetChk2 = false; {/if}
                                      {if $jb_settings.ColAddLocalAreaChk2 eq 1} var $ColAddLocalAreaChk2 = true; {else} var $ColAddLocalAreaChk2 = false; {/if}     
                                       {if $jb_settings.ColAddTownCityChk2 eq 1} var $ColAddTownCityChk2 = true; {else} var $ColAddTownCityChk2 = false; {/if}
                                       {if $jb_settings.ColAddCountyIDChk2 eq 1} var $ColAddCountyIDChk2 = true; {else} var $ColAddCountyIDChk2 = false; {/if}
                                       {if $jb_settings.ColAddCountryIDChk2 eq 1} var $ColAddCountryIDChk2 = true; {else} var $ColAddCountryIDChk2 = false; {/if}
                                       {if $jb_settings.ColAddEmailChk2 eq 1} var $ColAddEmailChk2 = true; {else} var $ColAddEmailChk2 = false; {/if}
                                       {if $jb_settings.ColAddPhoneChk2 eq 1} var $ColAddPhoneChk2 = true; {else} var $ColAddPhoneChk2 = false; {/if}
                                       {if $jb_settings.StockCodeChk2 eq 1} var $StockCodeChk2 = true; {else} var $StockCodeChk2 = false; {/if}
                                       {if $jb_settings.ModelNameChk2 eq 1} var $ModelNameChk2 = true; {else} var $ModelNameChk2 = false; {/if}
                                       {if $jb_settings.ProductDescriptionChk2 eq 1} var $ProductDescriptionChk2 = true; {else} var $ProductDescriptionChk2 = false; {/if}   
                                       {if $jb_settings.ManufacturerIDChk2 eq 1} var $ManufacturerIDChk2 = true; {else} var $ManufacturerIDChk2 = false; {/if}
                                       {if $jb_settings.UnitTypeIDChk2 eq 1} var $UnitTypeIDChk2 = true; {else} var $UnitTypeIDChk2 = false; {/if}
                                       {if $jb_settings.ImeiNoChk2 eq 1} var $ImeiNoChk2 = true; {else} var $ImeiNoChk2 = false; {/if}
                                       {if $jb_settings.SerialNoChk2 eq 1} var $SerialNoChk2 = true; {else} var $SerialNoChk2 = false; {/if}
                                       {if $jb_settings.DateOfPurchaseChk2 eq 1} var $DateOfPurchaseChk2 = true; {else} var $DateOfPurchaseChk2 = false; {/if}
                                       {if $jb_settings.ServiceTypeIDChk2 eq 1} var $ServiceTypeIDChk2 = true; {else} var $ServiceTypeIDChk2 = false; {/if}
                                       {if $jb_settings.ReportedFaultChk2 eq 1} var $ReportedFaultChk2 = true; {else} var $ReportedFaultChk2 = false; {/if}
                                       {if $jb_settings.AccessoriesChk2 eq 1} var $AccessoriesChk2 = true; {else} var $AccessoriesChk2 = false; {/if}
                                       {if $jb_settings.UnitConditionChk2 eq 1} var $UnitConditionChk2 = true; {else} var $UnitConditionChk2 = false; {/if}    
                                       {if $jb_settings.MobilePhoneNetworkIDChk2 eq 1} var $MobilePhoneNetworkIDChk2 = true; {else} var $MobilePhoneNetworkIDChk2 = false; {/if} 
                                       {if $jb_settings.OriginalRetailerChk2 eq 1} var $OriginalRetailerChk2 = true; {else} var $OriginalRetailerChk2 = false; {/if} 
                                       {if $jb_settings.ReceiptNoChk2 eq 1} var $ReceiptNoChk2 = true; {else} var $ReceiptNoChk2 = false; {/if} 
                                       {if $jb_settings.InsurerChk2 eq 1} var $InsurerChk2 = true; {else} var $InsurerChk2 = false; {/if} 
                                       {if $jb_settings.PolicyNoChk2 eq 1} var $PolicyNoChk2 = true; {else} var $PolicyNoChk2 = false; {/if} 
                                       {if $jb_settings.AuthorisationNoChk2 eq 1} var $AuthorisationNoChk2 = true; {else} var $AuthorisationNoChk2 = false; {/if} 
                                       {if $jb_settings.NotesChk2 eq 1} var $NotesChk2 = true; {else} var $NotesChk2 = false; {/if} 
                                       {if $jb_settings.ReferralNoChk2 eq 1} var $ReferralNoChk2 = true; {else} var $ReferralNoChk2 = false; {/if} 
                                     
                                      

                                        $('#jobBookingForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                               
                                               CustomerType:
                                                   {
                                                       required: $CustomerTypeChk2
                                                   },
                                                CompanyName:
                                                   {
                                                       required: $CompanyNameChk2
                                                   },
                                                CustomerTitleID:
                                                    {
                                                        required: $ContactTitleChk2
                                                    },
                                                ContactLastName:
                                                    {
                                                         required: $ContactTitleChk2
                                                    },
                                               BranchIDAddress:
                                                    {
                                                        required: $BranchIDAddressChk2
                                                    },             
                                               PostalCode:
                                                    {
                                                        required: $PostalCodeChk2
                                                    },
                                               
                                               postSelect:
                                                    {
                                                        required: true
                                                    },
                                               BuildingNameNumber:
                                                    {
                                                        required: $BuildingNameNumberChk2
                                                    },      
                                               Street:
                                                    {
                                                        required: $StreetChk2
                                                    },
                                               LocalArea:
                                                    {
                                                        required: $LocalAreaChk2
                                                    },                                             
                                               City:
                                                    {
                                                        required: $TownCityChk2
                                                    },
                                                            
                                              CountyID:
                                                    {
                                                        required: $CountyIDChk2
                                                    },               
                                              
                                              CountryID:
                                                    {
                                                         required: $CountryIDChk2
                                                                
                                                    },
                                                    
                                              ContactEmail:
                                                    {
                                                       required: function (element) {
                                                            if($("#jbNoContactEmail").is(':checked'))
                                                            {
                                                                    
                                                               $("#jbContactEmailSup").fadeOut();  
                                                               $(".formError [for='jbContactEmail']").fadeOut(); 
                                                                    
                                                                return false;
                                                            }
                                                            else
                                                            {
                                                               $("#jbContactEmailSup").fadeIn();  
                                                               $(".formError [for='jbContactEmail']").fadeIn(); 
                                                                return $ContactEmailChk2;
                                                            }
                                                        },    
                                                        email: true
                                                    },
                                              ContactHomePhone:
                                                    {
                                                        required: $ContactHomePhoneChk2,
                                                        digits: true
                                                    },
                                              ContactMobile:
                                                    {
                                                        required: function (element) {
                                                            if($("#jbNoContactMobile").is(':checked'))
                                                            {
                                                                    
                                                               $("#jbContactMobileSup").fadeOut();  
                                                               $(".formError [for='jbContactMobile']").fadeOut(); 
                                                                    
                                                                return false;
                                                            }
                                                            else
                                                            {
                                                               $("#jbContactMobileSup").fadeIn();  
                                                               $(".formError [for='jbContactMobile']").fadeIn(); 
                                                                return $ContactMobileChk2;
                                                            }
                                                        },
                                                        digits: true
                                                    },
                                                    
                                         /*    ContactAltMobile:
                                                    {
                                                        required: $ContactAltMobileChk2
                                                    }, */    
                                                            
                                              JobTypeID:
                                                    {
                                                        required: true
                                                    },
                                              ServiceTypeID:
                                                    {
                                                        required: $ServiceTypeIDChk2
                                                    },
                                             /* RepairTypeID:
                                                    {
                                                        required: true
                                                    },  */ 
                                              LocateBy:
                                                    {
                                                        required: true
                                                    },
							    
					    isProductAtBranch: { required: $isProductAtBranchChk2 },
					    
                                              /*StockCode:
                                                    {
                                                            required: $stockCodeRequred
                                                    },*/

                                              ModelName:
                                                    {
                                                            required: function (element) {
                                                            if($("[name='LocateBy']:checked").val()=="1")
                                                            {
                                                                return $ModelNameChk2;
                                                            }
                                                            else
                                                            {
                                                                return false;
                                                            }
                                                        }
                                                    },      
                                                    
                                              ProductDescription:
                                                    {
                                                        required: function (element) {
                                                            if($("[name='LocateBy']:checked").val()=="2")
                                                            {
                                                                
                                                                return $ProductDescriptionChk2;
                                                            }
                                                            else
                                                            {
                                                                return false;
                                                            }
                                                        }
                                                    },  
                                                    
                                              
                                               ColAddPostcode:
                                                    {
                                                        required: $ColAddPostcodeChk2
                                                    },
                                                postColAddSelect:
                                                    {
                                                        required: true
                                                    },      
                                                ColAddCompanyName:
                                                    {
                                                        required: $ColAddCompanyNameChk2
                                                    }, 
                                                ColAddBuildingNameNumber:
                                                    {
                                                        required: $ColAddBuildingNameNumberChk2
                                                    },             
                                              
                                               ColAddStreet:
                                                    {
                                                        required: $ColAddStreetChk2
                                                    },
                                                            
                                               ColAddLocalArea:
                                                    {
                                                        required: $ColAddLocalAreaChk2
                                                    },             
                                              
                                               ColAddCity:
                                                    {
                                                        required: $ColAddTownCityChk2
                                                    },
                                                  
                                                ColAddCountyID:
                                                    {
                                                       required: $ColAddCountyIDChk2
                                                    },                 
                                              
                                               ColAddCountryID:
                                                    {
                                                       required: $ColAddCountryIDChk2
                                                    },
                                               ColAddEmail:
                                                    {
                                                       required: $ColAddEmailChk2
                                                    },             
                                               ColAddPhone:
                                                    {
                                                       required: $ColAddPhoneChk2
                                                    },  
                                               StockCode:
                                                    {
                                                       required: $StockCodeChk2
                                                    },     
                                                    
                                                    
                                              UnitTypeID:
                                                    {
                                                        required: $UnitTypeIDChk2
                                                    },  
                                              ManufacturerID:
                                                    {
                                                        required: $ManufacturerIDChk2
                                                    },
                                             
                                              ReportedFault:
                                                    {
                                                        required: $ReportedFaultChk2
                                                    },
                                              ProductLocation:
                                                    {
                                                        required: $ProductLocationChk2
                                                    },   
                                              BranchID:
                                                 {
                                                     required: $ColBranchAddressIDChk2
                                                 },  
                                              
                                                    
                                              DateOfPurchase:
                                                    {
                                                       required: $DateOfPurchaseChk2, 
                                                       dateITA: true,  
                                                       noFutureDate:  true
                                                    },
                                              ReferralNo:
                                                    {
                                                        required: $ReferralNoChk2
                                                    },
                                              SerialNo:
                                                    {
                                                        required: $SerialNoChk2
                                                    },              
                                                            
					    ImeiNo: {	
						required: function() {
						    if($("#jbProductLocation").val() == "Branch" || $("#jbProductLocation").val() == "Service Provider") {
							return $ImeiNoChk2;
						    } else {
							return false;
						    }
						}
						//minlength: 14  
					    },
                                            HiddenAccessoriesFlag: {	
						required: function() {
						    if($("#jbProductLocation").val() == "Branch" || $("#jbProductLocation").val() == "Service Provider") {
							return $AccessoriesChk2;
						    } else {
							return false;
						    }
						}
					    },
                                            UnitCondition:
                                                    {
                                                        required: $UnitConditionChk2
                                                    },
                                            MobilePhoneNetworkID:
                                                    {
                                                        required: $MobilePhoneNetworkIDChk2
                                                    },
                                            OriginalRetailerFullPart:
                                                    {
                                                        required: $OriginalRetailerChk2
                                                    },
                                            OriginalRetailerPart1:
                                                    {
                                                        required: $OriginalRetailerChk2
                                                    },             
                                                    
                                            ReceiptNo:
                                                    {
                                                        required: $ReceiptNoChk2
                                                    },
                                                            
                                            Insurer:
                                                    {
                                                        required: $InsurerChk2
                                                    },
                                                    
                                            PolicyNo:
                                                    {
                                                        required: $PolicyNoChk2
                                                    },
                                              
                                                    
                                            AuthorisationNo:
                                                    {
                                                        required: $AuthorisationNoChk2
                                                    },
                                            Notes:
                                                    {
                                                        required: $NotesChk2
                                                    }
                                                            

                                            },
                                            messages: {
                                                        CompanyName:
                                                        {
                                                            required: "{$page['Errors']['company_name']|escape:'html'}"
                                                        },
                                                        CustomerTitleID:
                                                        {
                                                            required: "{$page['Errors']['contact_title']|escape:'html'}"
                                                        },
                                                       /* ContactFirstName:
                                                        {
                                                            required: "{$page['Errors']['contact_first_name']|escape:'html'}"
                                                        },*/
                                                        ContactLastName:
                                                        {
                                                            required: "{$page['Errors']['contact_last_name']|escape:'html'}"

                                                        },
                                                        PostalCode:
                                                        {
                                                            required: "{$page['Errors']['postal_code']|escape:'html'}"

                                                        },
                                                        postSelect:
                                                        {
                                                            required: "{$page['Errors']['select_address']|escape:'html'}"

                                                        },
                                                        Street:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"

                                                        },
                                                      
                                                        City:
                                                        {
                                                            required: "{$page['Errors']['city']|escape:'html'}"

                                                        },
                                                       
                                                        CountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"

                                                        },
                                                        ContactEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"

                                                        },
                                                        ContactHomePhone:
                                                        {
                                                          //  required: "{$page['Errors']['daytime_phone']|escape:'html'}",
                                                            digits: "{$page['Errors']['valid_daytime_phone']|escape:'html'}"

                                                        },
                                                        ContactMobile:
                                                        {
                                                            digits: "{$page['Errors']['valid_mobile_no']|escape:'html'}"
                                                        },
                                                        JobTypeID:
                                                        {
                                                            required: "{$page['Errors']['job_type']|escape:'html'}"
                                                        },
                                                        ServiceTypeID:
                                                        {
                                                            required: "{$page['Errors']['service_type']|escape:'html'}"
                                                        },
                                                       /* RepairTypeID:
                                                        {
                                                            required: "{$page['Errors']['repair_type']|escape:'html'}"
                                                        },*/
                                                        LocateBy:
                                                        {
                                                            required: "{$page['Errors']['locate_by']|escape:'html'}"
                                                        },
								
							isProductAtBranch: { required: "Please select product location." },
                                                        
                                                       /* StockCode:
                                                        {
                                                             required: "{$page['Errors']['stock_code']|escape:'html'}"
                                                        },*/
                                                        
                                                        ModelName:
                                                        {
                                                             required: "{$page['Errors']['model_name']|escape:'html'}"
                                                        },
                                                        
                                                        ProductDescription:
                                                        {
                                                             required: "{$page['Errors']['product_description']|escape:'html'}"
                                                        },
                                                       /* BranchID:
                                                        {
                                                            required: "{$page['Errors']['branch']|escape:'html'}"
                                                        },*/ 
                                                        
                                                        
                                                        ColAddPostcode:
                                                        {
                                                            required: "{$page['Errors']['postal_code']|escape:'html'}"

                                                        },
                                                        postColAddSelect:
                                                        {
                                                            required: "{$page['Errors']['select_address']|escape:'html'}"

                                                        },
                                                        ColAddStreet:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"

                                                        },
                                                        ColAddCity:
                                                        {
                                                            required: "{$page['Errors']['city']|escape:'html'}"

                                                        },
                                                        ColAddCountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"

                                                        },
                                                        
                                                        
                                                        
                                                        
                                                        UnitTypeID:
                                                        {
                                                            required: "{$page['Errors']['product_type']|escape:'html'}"
                                                        },  
                                                         ManufacturerID:
                                                        {
                                                            required: "{$page['Errors']['manufacturer']|escape:'html'}"
                                                        },
                                                        ReportedFault:
                                                        {
                                                            required: "{$page['Errors']['reported_fault']|escape:'html'}"
                                                        },
                                                        ProductLocation:
                                                        {
                                                            required: "{$page['Errors']['product_location']|escape:'html'}"
                                                        },
                                                        DateOfPurchase:
                                                        {
                                                            dateITA: "{$page['Errors']['purchase_date']|escape:'html'}",
                                                            noFutureDate: "{$page['Errors']['purchase_date_valid']|escape:'html'}"
                                                           
                                                        },
                                                        ReferralNo:
                                                        {
                                                            required: "{$page['Errors']['referral_number']|escape:'html'}"
                                                        },
							ImeiNo: { 
							    required: "Please enter IMEI number."
							    //minlength: "IMEI number must be at least 14 characters long."
							},
                                                        HiddenAccessoriesFlag: {
                                                            required: "Please select Accessories."
                                                        }        
                                                        
                                                        
                                                    }, 
                                            errorPlacement: function(error, element) {
                                                
                                                error.insertAfter( element );
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                    
                                                    
                                                if($("#SMSPopupPage").val()=="1")
                                                {

                                                        if($("#jbUnitTypeID").val() && $("#jbUnitTypeID").val()!='-1')
                                                        {
                                                             if( $("#jbUnitTypeID option[value='"+$("#jbUnitTypeID").val()+"']").text().toLowerCase() == "mobile phone" )
                                                             {

                                                                 var $mNo = $("#jbContactMobile").val();  

                                                                 if($("#jbContactMobile").val()=="{$page['Text']['mobile']|escape:'html'}")
                                                                           {
                                                                                     $mNo = '';
                                                                           } 

                                                                 if($("#ContactAltMobile").val()=="{$page['Text']['mobile']|escape:'html'}")
                                                                 {
                                                                     $aMNo = '';
                                                                 }
                                                                 else
                                                                 {
                                                                     $aMNo = $("#ContactAltMobile").val();    
                                                                 }

                                                                 //It opens color box popup page.              
                                                                 $.colorbox( { href: '{$_subdomain}/Popup/newMobileNumber/mNo='+urlencode($mNo)+'/ssct='+urlencode($("#SendServiceCompletionText").val())+'/aMNo='+$aMNo+'/'+ Math.random(),
                                                                                 title: "{$page['Text']['service_completion_text_message']|escape:'html'}",
                                                                                 opacity: 0.75,
                                                                                 height:510,
                                                                                 width:740,
                                                                                 overlayClose: false,
                                                                                 escKey: false, 
                                                                                 onComplete: function(){

                                                                                  $(this).colorbox.resize({
                                                                                     height: ($('#MobileNumberEntryDiv').height()+150)+"px"                                
                                                                                 });

                                                                                 }

                                                                              }); 
                                                                 return false;
                                                             }
                                                        }

                                                } 
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                   // $('#jbPostalCode').val() = $('#jbPostalCode').val().toUpperCase();
                                                    
                                                   // $("[disabled='disabled']").removeAttr("disabled");
                                                    
                                                    
                                                    //It opens color box popup page.              
                                                    $.colorbox( { href: '{$_subdomain}/Job/preview/'+ Math.random(),
                                                                    title: '',
                                                                    data: $('#jobBookingForm').serializeArray(), 
                                                                    opacity: 0.75,
                                                                    height:500,
                                                                    width:900,
                                                                    overlayClose: false,
                                                                    escKey: false, 
                                                                    onComplete: function(){
                                                                    
                                                                    
                                                                     $(this).colorbox.resize({
                                                                        height: ($('#jobConfirmation').height()+150)+"px"                                
                                                                    });
                                                                    
                                                                    
                                                                    
                                                                    }
                                                                   
                                                                 }); 
                                                                 
                                                     
                                                }
                                            });
                            
                            
                            
                              }      
                        );
                     /* Add a click handler to the confirm booking of job booking form ends here*/ 
       
       
       
                  
       
       
       
                $('.capitalizeText').blur(function(){
                   $(this).ucwords();
                });

       
       
                 
       
                   
            //Model number's text transform handling starts here..       
            $(document).on({
                    keypress: function() {
                   
                        textTransform("#jbModelName", "{$page['Text']['model_number_title']|escape:'html'}", "uppercaseText");
                         
                    },
                    blur: function() {
                        
                        textTransform("#jbModelName", "{$page['Text']['model_number_title']|escape:'html'}", "uppercaseText");
                    },
                    click: function() {
                       
                       textTransform("#jbModelName", "{$page['Text']['model_number_title']|escape:'html'}", "uppercaseText");
                    },
                    focus: function() {
                    
                      
                        textTransform("#jbModelName", "{$page['Text']['model_number_title']|escape:'html'}", "uppercaseText");
                    }
                }, "#jbModelName");
            //Model number's text transform handling ends here..
                   
                   
            
               
                  
                  
       
             
       
       
           
           //Locate product details fields enable/disable based on option selection
            locateByDetails($("[name='LocateBy']:checked").val(), false);
           
             /* Add a click handler to the locate by radio buttons - strats here*/
                $(document).on('click', "[name='LocateBy']", 
                    function() {
                   
                    
                        locateByDetails($(this).val(), true);
                    
                    
                    }
                    
                  );  
             /* Add a click handler to the locate by radio buttons - ends here*/
             
             
             
             
                /* Add a click handler to the finish button*/
                $(document).on('click', '#finish_btn', 
                    function() {
                    
                         $.colorbox.close();
             
                         });
             
             
                
                       
            //Post code look up button mouse handlers starts here..           
            $("#quick_find_btn").mouseover(function(){
                $("#quick_find_btn").animate({ backgroundPosition:"0 -21" },0);
            });
            $("#quick_find_btn").mouseout(function(){
                $("#quick_find_btn").animate({ backgroundPosition:"0 0" },0);
            });
                       
            //Post code look up button mouse handlers ends here..           
                       
             
           
             
             
             
              /* Add a click handler to the no mail checkbox - strats here*/
                $(document).on('click', '#jbNoContactEmail', 
                    function() {
                    
                        if($("#jbNoContactEmail").is(':checked'))
                        {

                            $("#jbContactEmailSup").fadeOut();  
                            $(".fieldError[for='jbContactEmail']").fadeOut(); 
                            
                             //It opens color box popup page.              
                            $.colorbox( { href: '{$_subdomain}/Job/noEmailPage/'+ Math.random(),
                                            title: '',
                                            opacity: 0.75,
                                            height:510,
                                            width:740,
                                            overlayClose: false,
                                            escKey: false, 
                                            onLoad: function() {
                                                $('#cboxClose').remove();
                                            },
                                            onComplete: function(){


                                             $(this).colorbox.resize({
                                                height: ($('#noEmailPage').height()+150)+"px"                                
                                            });


                                            }

                                         }); 

                          
                        }
                        else
                        {
                            $("#jbContactEmailSup").fadeIn();  
                            if(!$("#jbContactEmail").val())
                            {
                                $(".fieldError[for='jbContactEmail']").fadeIn();
                            }
                           
                        }
                    
                    
                    }
                    
                  );  
             /* Add a click handler to the no mail checkbox - ends here*/
   
   
   
   
   
   
              /* Add a click handler to the no mobile checkbox - strats here*/
                $(document).on('click', '#jbNoContactMobile', 
                    function() {
                    
                        if($("#jbNoContactMobile").is(':checked'))
                        {

                            $("#jbContactMobileSup").fadeOut();  
                            $(".fieldError[for='jbContactMobile']").fadeOut(); 
                        }
                        else
                        {
                            $("#jbContactMobileSup").fadeIn();  
                            if(!$("#jbContactMobile").val())
                            {
                                $(".fieldError[for='jbContactMobile']").fadeIn();
                            }
                           
                        }
                    
                    }
                    
                  );  
             /* Add a click handler to the no mobile checkbox - ends here*/  
   
   
   
   
   
   
             /* Add a click handler to request new unit type - strats here*/
                $(document).on('click', '#RequestNewUnitType', 
                    function() {
                    
                        
                           
                            
                             //It opens color box popup page.              
                            $.colorbox( { href: '{$_subdomain}/Popup/unitTypeEntry/'+ Math.random(),
                                            title: "{$page['Text']['unit_type_entry']|escape:'html'}",
                                            opacity: 0.75,
                                            height:510,
                                            width:740,
                                            overlayClose: false,
                                            escKey: false, 
                                            onComplete: function(){

                                             $(this).colorbox.resize({
                                                height: ($('#UnitTypeEntryDiv').height()+150)+"px"                                
                                            });

                                            }

                                         }); 

                    
                    }
                    
                  );  
             /* Add a click handler to request new unit type - ends here*/
   
   
   
                    
                    
                    
    /* Add a click handler to the submit booking of job confirm booking form starts here*/  
    $(document).on('click', '#submit_booking_btn', function() {
                                                         
	$("#submit_booking_btn").hide();
        $("#edit_booking_btn").hide();
        $("#cancel_booking_btn").hide();
        
        
	$("#processDisplayText").show();

	$("#submit_booking_btn").queue(function() {

	    $('#jobConfirmationForm').validate({

		rules:  {
		  data_protection_act: { required: true }
		},
		    
		messages: {
		    data_protection_act: { required: "{$page['Errors']['data_protection_act']|escape:'html'}" }
		},
		    
		errorPlacement: function(error, element) {
		    error.insertAfter( element );
		    $("#processDisplayText").hide();
		    $("#submit_booking_btn").show();
                    $("#edit_booking_btn").show();
                    $("#cancel_booking_btn").show();
		},
			
		errorClass:	'fieldError',
		onkeyup:	false,
		onblur:		false,
		errorElement:	'label',

		submitHandler: function() {

		    $.post("{$_subdomain}/Job/processData/",        

		    $("#jobConfirmationForm").serialize(),      
		    
		    function(data) {

			// DATA NEXT SENT TO COLORBOX
			var p = eval("(" + data + ")");

			if(p['status']=="SUCCESS") {
			    //$(this).colorbox.close();
			    var url = "{$_subdomain}/Job/confirmed/"+urlencode(p['jobId']);    
			    $(location).attr('href',url);
			} else {    
			   // $(this).colorbox.close();
			    //var url = "{$_subdomain}/Job/notConfirmed";    
			  //  $(location).attr('href',url);
                          $("#edit_booking_btn").show();
                          $("#cancel_booking_btn").show();
                          $("#processDisplayText").html(p['message']);
                          
			}

		    });    

		}
		
	    });

	   $(this).dequeue();

	});  
                                            
    });

                /* Add a click handler to the submit booking of job confirm booking form ends here*/ 
      
                    
               
               
                 /* Add a click/submit handler to the finish button of data protection form starts here*/  
                $(document).on('click', '#data_protection_finish_btn', function() {



                                                         $('#dataProtectionForm').validate({


                                                               rules:  {
                                                                            data_protection_act:
                                                                            {
                                                                                    required: true
                                                                            },
                                                                            contact_method:
                                                                            {
                                                                                    required: function (){
                                                                                        if($("#data_protection_act_1").is(':checked'))
                                                                                        {
                                                                                                return true;
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return false;
                                                                                        }
                                                                                    },
                                                                                    minlength: 1
                                                                            }
                                                                            
                                                                            
                                                                },
                                                                
                                                                
                                                                
                                                                
                                                                messages: {
                                                                            data_protection_act:
                                                                            {
                                                                                    required: "{$page['Errors']['future_contact']|escape:'html'}"
                                                                            },
                                                                            contact_method:
                                                                            {
                                                                                    required: "{$page['Errors']['contact_method']|escape:'html'}"
                                                                            }
                                                                },

                                                                errorPlacement: function(error, element) {

                                                                        error.insertAfter( element.parent("span") );
                                                                },
                                                                errorClass: 'fieldError',
                                                                onkeyup: false,
                                                                onblur: false,
                                                                errorElement: 'label',

                                                                submitHandler: function() {

                                                                            
                                                                            $("#dataProtectionPage").fadeOut();
                                                                            $("#jobConfirmation").fadeIn("slow");

                                                                            $(this).colorbox.resize({
                                                                                height: ($('#jobConfirmation').height()+150)+"px"
                                                                            });
                                                                            
                                                                            if($("#data_protection_act_1").is(':checked'))
                                                                            {
                                                                                $("#data_protection_act_yes").attr('checked', true);
                                                                                
                                                                                if($("#contact_method_1").is(':checked'))
                                                                                {
                                                                                       $("#contact_method_mail").val('1'); 
                                                                                }
                                                                                else
                                                                                {
                                                                                       $("#contact_method_mail").val('0');
                                                                                }
                                                                                
                                                                                if($("#contact_method_2").is(':checked'))
                                                                                {
                                                                                       $("#contact_method_letter").val('1'); 
                                                                                }
                                                                                else
                                                                                {
                                                                                       $("#contact_method_letter").val('0');
                                                                                }
                                                                                
                                                                                
                                                                                if($("#contact_method_3").is(':checked'))
                                                                                {
                                                                                       $("#contact_method_sms").val('1'); 
                                                                                }
                                                                                else
                                                                                {
                                                                                       $("#contact_method_sms").val('0');
                                                                                }
                                                                                
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                 $("#data_protection_act_no").attr('checked', true);
                                                                                 $("#contact_method_mail").val('0');
                                                                                 $("#contact_method_letter").val('0');
                                                                                 $("#contact_method_sms").val('0');
                                                                            }
                                                                            
                                                                           

                                                                            return false;
                                                                                  
                                                                               

                                                                        }
                                                                });

                });

                /* Add a click/submit handler to the finish button of data protection form ends here*/  
               
              
               //This function is used for to transform text.
               function textTransform($element, $defaultText, $className)
               {
                  if($($element).val() && $($element).val().toLowerCase()!=$defaultText.toLowerCase())
                  {
                         $($element).addClass($className); 
                  }
                  else
                  {
                      $($element).removeClass($className);
                  }
               }
                    
                    
                    
               
               /* Add a click handler to the more info/No radio button of Data Protection Policy - strats here*/
              function showDataProtectionPage() {
              
                            $("#jobConfirmation").fadeOut();
                            $("#dataProtectionPage").fadeIn("slow");
                         
                            $(this).colorbox.resize({
                             
                               height: ($('#dataProtectionPage').height()+150)+"px"
                                
                            });
              
               }

               $(document).on('click', '#data_protection_act_moreinfo', 
                    function() {
                           
                         showDataProtectionPage();
                         return false;
                    }      
                );
              
              $(document).on('click', '#data_protection_act_no', 
                    function() {
                           
                         showDataProtectionPage();
                    }      
                );
             /* Add a click handler to the more info/No radio button of Data Protection Policy - ends here*/
                    
                
                
                
               /* Add a click handler to the change/default links of Job confirmation page - strats here*/
              function showServiceProvidersPage() {
              
                            $("#jobConfirmation").fadeOut();
                            $("#ServiceProvidersPage").fadeIn("slow");
                          
                            $(this).colorbox.resize({
                             
                               height: ($('#ServiceProvidersPage').height()+150)+"px"
                                
                            });
              
               }

               $(document).on('click', '#ServiceProvideChangeLink', 
                    function() {
                           
                         showServiceProvidersPage();
                         return false;
                    }      
                );
                
               
              
               $(document).on('click', '#ServiceProvideDefaultLink', 
                    function() {
                           
                            $('#ServiceProviderID').val($('#DefaultServiceProviderID').val()); 
                            
                            if(!$('#DefaultServiceProviderID').val())
                            {
                                $('#ServiceProvideName').html($('#DefaultServiceProvideName').val()).css({ "color":"red", "font-size":"11px" });
                                $('#product_location_label').html($('#default_product_location').val()).css({ "color":"red", "font-size":"11px" });
                            }
                            else
                            {    
                                $('#ServiceProvideName').html($('#DefaultServiceProvideName').val()).css("color","");
                                $('#product_location_label').html($('#default_product_location').val()).css("color","");
                            }
                  
                          return false;
                    }      
                );
               
                $(document).on('click', '#sp_dt_finish_btn', 
                    function() {
                           
                             $("#ServiceProvidersPage").fadeOut();
                             $("#jobConfirmation").fadeIn("slow");

                             $(this).colorbox.resize({
                                height: ($('#jobConfirmation').height()+150)+"px"
                             });
                    }      
                );
                
                
                
             /* Add a click handler to the change/default links of Job confirmation page  - ends here*/
                    
                 
                    
                    
       
       
              /* Add a click handler to the cancel job booking button of job booking confirmation popup page - strats here*/
                $(document).on('click', '#cancel_booking_btn', 
                    function() {
                           
                         
                            $("#jobConfirmation").fadeOut();
                            $("#jobCancelationRequest").fadeIn("slow");
                         
                            
                         
                            $(this).colorbox.resize({
                             
                               height: ($('#jobCancelationRequest').height()+150)+"px" 
                                
                            });
                         
                            return false;
                        
                    }      
                );
              /* Add a click handler to the cancel job booking button of job booking confirmation popup page - ends here*/
              
              
              
              
              
               /* Add a click handler to the 'Abort the cancelation' button of Job Cancelation Request popup page - strats here*/
                $(document).on('click', '#abort_cancelation_btn', 
                    function() {
                         
                         $("#jobCancelationRequest").fadeOut();
                         $("#jobConfirmation").fadeIn("slow");
                          
                         
                         
                         $(this).colorbox.resize({
                            height: ($('#jobConfirmation').height()+150)+"px"                                  
                         });
                         
                         return false;
                        
                    }      
                );
               /* Add a click handler to the 'Abort the cancelation' button of Job Cancelation Request popup page - ends here*/
              
              
              
               /* Add a click handler to the 'Proceed with the cancellation' button of Job Cancelation Request popup page - strats here*/
                $(document).on('click', '#proceed_cancellation_btn', 
                    function() {
                           
                         $(this).colorbox.close();
                         
                         var url = "{$_subdomain}/index/";    
                         $(location).attr('href',url);

                         
                         return false;
                        
                    }      
                );
               /* Add a click handler to the 'Proceed with the cancellation' button of Job Cancelation Request popup page - ends here*/
              
              
              
              
              /* Add a click handler to the 'Edit Booking Details' button of Summary Details Check popup page - strats here*/
                $(document).on('click', '#edit_booking_btn', 
                    function() {
                           
                         
                         if($('#data_protection_act_yes').is(':checked'))
                         {
                            $("#jb_data_protection_act").val($('#data_protection_act_yes').val()); 
                         }    
                         else  if($('#data_protection_act_no').is(':checked'))
                         {
                            $("#jb_data_protection_act").val($('#data_protection_act_no').val()); 
                         }
                             
                         if($("#jbContactEmail").val()!='')
                         {
                            $("#jbNoContactEmail").attr("checked", false);
                         }
                         
                         if($("#jbContactMobile").val()!='')
                         {
                            $("#jbNoContactMobile").attr("checked", false);
                         }
                        
                         $("#SMSPopupPage").val('1');
                           
                         $(this).colorbox.close();
                         
                         return false;
                        
                    }      
                );
               /* Add a click handler to the 'Edit Booking Details' button of Summary Details Check popup page - ends here*/
              
              
              
              
              
              
               /* Add a click handler to the Free text address link of job booking page - strats here*/
                $(document).on('click', '#jbFreeTextAddress', 
                    function() {
                                
                                var addressFieldsArray = new Array("#jbBranchAddress", "#jbBuildingName", "#jbStreet", "#jbArea", "#jbCity", "#jbCounty", "#jbCountry", "#jbPostalCode", "#jbContactHomePhone", "#ContactHomePhoneExt", "#jbContactEmail", "#jbCompanyName", "#jbContactMobile");
                                
                                
                                for($i=0;$i<addressFieldsArray.length;$i++)
                                {
                                    $(addressFieldsArray[$i]).val('').blur();
                                    
                                    if(addressFieldsArray[$i]=="#jbCounty" || addressFieldsArray[$i]=="#jbCountry")
                                    {
                                        setValue(addressFieldsArray[$i]);
                                    }
                                    
                                }  
                                
                    }      
                );
              /* Add a click handler to the Free text address link dropdown of job booking page - ends here*/
              
              
              
              
              
              
              /* Add a click handler to the Branch Location Details Icon - strats here*/
                $(document).on('click', '#BranchLocationDetailsIcon', 
                    function() {
                                
                          if($(this).attr('class')=='plusIcon')
                          {
                              $(this).removeClass('plusIcon').addClass("minusIcon");
                              $(this).attr('src', '{$_subdomain}/css/Skins/{$_theme}/images/minus-icon.png');
                              
                              collectionAddressFields(true, false);
                          }
                          else
                          {
                              $(this).removeClass('minusIcon').addClass("plusIcon");
                              $(this).attr('src', '{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png');
                              collectionAddressFields(false, false);
                          }

                         //plusIcon     
                                
                    }      
                );
              /* Add a click handler to the Branch Location Details Icon - ends here*/
              
              
              
              
                
             
                {if $jb_datarow.JobTypeID neq ''}
        
                    $("#JTRB_{$jb_datarow.JobTypeID}").trigger("click");
                  
                {/if}
                    
                 
              {*   //Customer type trigger when page loaded.
                {if $jb_datarow.CustomerType neq ''}
        
                    $("#CustomerType{$jb_datarow.CustomerType}").trigger("click");
                  
                {/if}    *}
                   
                  
                    
                   
                 function collectionAddressFields($showFlag, $ClearDataFlag)  
                 {
                     
                     //clear the data.
                     if($ClearDataFlag===false)
                     {
                         if($showFlag)
                         {
                            $("#jbBranchIDElement").show();     
                         }
                         else
                         {
                             $("#jbBranchIDElement").hide();
                         }
                     }
                     else
                     {
                        $("#jbColAddPostalCode").val('');
                        $("#jbColAddCompanyName").val('');
                        $("#jbColAddBuildingName").val('');
                        $("#jbColAddStreet").val('');
                        $("#jbColAddArea").val('');
                        $("#jbColAddCity").val('');
                        $("#jbColAddCounty").val('');
                        setValue("#jbColAddCounty");

                        $("#jbColAddCountry").val('');
                        setValue("#jbColAddCountry");

                        $("#jbColAddEmail").val('');
                        $("#jbColAddPhone").val('');
                        $("#ColAddPhoneExt").val('');
                     }
                    
                     
                     if($showFlag)
                     {
                            $("#jbColAddPostalCode").removeAttr("disabled");
                            $("#postSelect_1").removeAttr("disabled");
                            $("#jbColAddStreet").removeAttr("disabled");
                            $("#jbColAddCity").removeAttr("disabled");
                            $("#jbColAddCountry").removeAttr("disabled"); 
                            $("#jbColAddEmail").removeAttr("disabled"); 
                            $("#jbColAddPhone").removeAttr("disabled"); 
                            $("#ColAddPhoneExt").removeAttr("disabled");
                            
                             
                            $("#jbColAddPostalCodeElement").show();
                            $("#jbColAddCompanyNameElement").show();
                            $("#jb_p_c_a_house_name").show();
                            $("#jb_p_c_a_street").show();
                            $("#jb_p_c_a_area").show();
                            $("#jb_p_c_a_town").show();
                            $("#jb_p_c_a_county").show();
                            $("#jb_p_c_a_country").show();    
                            $( "#jbColAddCounty" ).combobox({ change: function() { setCountryBasedOnCounty("#jbColAddCounty", "#jbColAddCountry", "#jb_p_c_a_country") } });
                            $( "#jbColAddCountry" ).combobox();  
                            $("#jb_p_c_a_email").show();
                            $("#jb_p_c_a_phone").show();
                     }
                     else {
                     
                        $("#jbColAddPostalCode").attr("disabled", "disabled");
                        $("#postSelect_1").attr("disabled", "disabled");
                        $("#jbColAddStreet").attr("disabled", "disabled");
                        $("#jbColAddCity").attr("disabled", "disabled");
                        $("#jbColAddCountry").attr("disabled", "disabled");
                        $("#jbColAddEmail").attr("disabled", "disabled");
                        $("#jbColAddPhone").attr("disabled", "disabled");
                        $("#ColAddPhoneExt").attr("disabled", "disabled");
                        
                        
                        
                     
                        $("#jbColAddPostalCodeElement").hide();
                        $("#jbColAddCompanyNameElement").hide();
                        $("#jb_p_c_a_house_name").hide();
                        $("#jb_p_c_a_street").hide();
                        $("#jb_p_c_a_area").hide();
                        $("#jb_p_c_a_town").hide();
                        $("#jb_p_c_a_county").hide();
                        $("#jb_p_c_a_country").hide();
                        $("#jb_p_c_a_email").hide();
                        $("#jb_p_c_a_phone").hide();
                     
                     }
                     
                     
                 }  
                   
                
                    
              {*      
                 {if $jb_datarow.JobTypeCode eq 2}   //Store stock repair job type.
                 
                  *}
                  
                     
                    $("#jbBranchAddress").combobox({ change: function() { autoFillBranchAddress() } });    
                  
                    //This is called when branch addreess dropdown changed.
                    function autoFillBranchAddress()
                    {
                           
                            selected = $('#jbBranchAddress').val();


                            if(selected)
                            {
                                var addressFieldsArray = new Array("#jbBuildingName", "#jbStreet", "#jbArea", "#jbCity", "#jbCounty", "#jbCountry", "#jbPostalCode", "#jbContactHomePhone", "#ContactHomePhoneExt", "#jbContactEmail", "#jbCompanyName");

                                var selected_split = selected.split(";;"); 	

                                for($i=0;$i<addressFieldsArray.length;$i++)
                                {
                                    $(addressFieldsArray[$i]).val(trim(selected_split[$i])).blur();
                                    if(addressFieldsArray[$i]=="#jbCounty" || addressFieldsArray[$i]=="#jbCountry")
                                    {
                                        setValue(addressFieldsArray[$i]);
                                    }
                                }
                            } 
                       
                    }
                    autoFillBranchAddress();
                    
                    
                    // $("#jbDateOfPurchaseElement").hide();
                   
                  {*  $("#jbInsurerElement").hide();
                    $("#jbPolicyNoElement").hide();
                    $("#jbAuthorisationNoElement").hide();
                    $("#jbNotesElement").hide();
                    $("#jbReceiptNoLabel").html("{$page['Labels']['sales_invoice_no']|escape:'html'}:");
                    
                    $("#jbCustomerTypeElement").hide(); 

                 {elseif $jb_datarow.JobTypeCode eq 3}   //Installation Job type
                     
                    $("#jbSerialNoElement").hide();
                    $("#jbOriginalRetailerElementTop").hide();
                    $("#jbOriginalRetailerElement").hide();
                    $("#jbDateOfPurchaseElement").hide();
                    $("#jbInsurerElement").hide();
                    $("#jbPolicyNoElement").hide();
                    $("#jbAuthorisationNoElement").hide();
                    $("#jbNotesElement").hide();
                    $("#jbReportedFaultLabel").html("{$page['Labels']['installation_notes']|escape:'html'}:<sup>*</sup>");
                    $("#jbModelNameStar").hide();
                    $("#jbManufacturerIDStar").hide();
                 
                 {elseif $jb_datarow.JobTypeCode eq 4}   //Other services Job type
                     
                  
                    $("#jbReportedFaultLabel").html("{$page['Labels']['service_specific_instructions']|escape:'html'}:<sup>*</sup>");
                  
                 {/if} 
                  *}
                    
                
                 $("#jbBranchIDElement").hide(); 
                 collectionAddressFields(false);
                 $( "#jbBranchID" ).combobox({ change: function() {         
                    $sltBranchID = $("#jbBranchID").val();
                    fillCollectionAddress($sltBranchID);
                } });
                 
              function fillCollectionAddress($sltBranchID)
              {
                  
                  //  selected = $("#branch_address_"+$sltBranchID).html();
                    var selected = false;
                    
                    //alert($sltBranchID);
                    
                    if($sltBranchID=="-1")
                    {
                         
                            $("#jbProductLocation").val("Other");
                            $("#jbProductLocation").blur();
                            setValue("#jbProductLocation");
                           
                            $("#jbBranchID").val('').blur();
                            setValue("#jbBranchID");
                            
                            $("#jbBranchIDElement").hide(); 
                            collectionAddressFields(true);
                            
                    }
                    
                    if($sltBranchID)
                    {
                        //Getting branches  for selected client and network.   
                        $.post("{$_subdomain}/Data/getBranchAddress/"+urlencode($sltBranchID),        

                        '',      
                        function(data){
                               
                                var $BA = eval("(" + data + ")");
                                
                                if($BA)
                                {
                                  selected = new Array ($BA['BuildingNameNumber'], $BA['Street'], $BA['LocalArea'], $BA['TownCity'], $BA['CountyID'], $BA['CountryID'], $BA['PostalCode'], $BA['ContactPhone'], $BA['ContactPhoneExt'], $BA['ContactEmail']);                                   
                               
                           
                                    $("#jbColAddCompanyName").val($("#jbBranchID option[value='"+$("#jbBranchID").val()+"']").html());

                                    var addressFieldsArray = new Array("#jbColAddBuildingName", "#jbColAddStreet", "#jbColAddArea", "#jbColAddCity", "#jbColAddCounty", "#jbColAddCountry", "#jbColAddPostalCode", "#jbColAddPhone", "#ColAddPhoneExt", "#jbColAddEmail");


                                    for($i=0;$i<addressFieldsArray.length;$i++)
                                    {
                                        $(addressFieldsArray[$i]).val(trim(selected[$i])).blur();
                                        if(addressFieldsArray[$i]=="#jbColAddCounty" || addressFieldsArray[$i]=="#jbColAddCountry")
                                        {
                                            setValue(addressFieldsArray[$i]);
                                        }
                                    } 
                           
                                }
                                else
                                    {
                                        collectionAddressFields(true);
                                }
                                
                                
                                
                                
                        });
                        
                    }
                    else {
                        collectionAddressFields(true);
                    }
                   

              }
                
                

	    //Auto complete for drop downs starts here...

	    //Contact title dropdown
	    $("#jbContactTitle").combobox();
	    
	    $("#jbCounty").combobox({
		change: function() { 
		    setCountryBasedOnCounty("#jbCounty", "#jbCountry", "#jb_p_country") 
		} 
	    });
	    
	    $("#jbCountry").combobox();
	    $("#jbManufacturerID").combobox({
		change: function() {
		    $.post("{$_subdomain}/Job/getRARequired", { id: $("#jbManufacturerID").val() }, function(response) {
			if(response == 1) {
			    $("#jbAuthorisationNoElement").hide();
			} else {
			    $("#jbAuthorisationNoElement").show();
			}
		    });
                    
		}
	    });
            
            
            function unitTypeChange()
            {
                if($("#jbUnitTypeID").val() == '-1') {
                    $('#RequestNewUnitType').trigger('click');
                }    
                $("#ImeiNoP").remove();
                $.post("{$_subdomain}/Data/isImeiRequired", { unitTypeID: $("#jbUnitTypeID").val(), modelName: $("#jbModelName").val() }, function(response) {
                    if(response) {				   

                        var html = '<p id="ImeiNoP">\
                                        <label for="ImeiNo">IMEI No:</label>\
                                        &nbsp;&nbsp;\
                                        <input type="text" name="ImeiNo" minlength="'+ response.IMEILengthFrom +'" maxlength="'+ response.IMEILengthTo +'" class="text uppercaseText" id="ImeiNo" value="{$jb_datarow.ImeiNo|escape:'html'}" />\
                                    </p>';
                        $(html).insertAfter("#jbUnitTypeIDElement");
                        if($("#jbProductLocation").val() == "Branch" || $("#jbProductLocation").val() == "Service Provider") {
                            $("#ImeiNoP label").html($("#ImeiNoP label").html() + "<sup>*</sup>");
                        }
                    }
                }, "json" );
                $.post("{$_subdomain}/Data/getUnitTypeAccessories", { unitTypeID: $("#jbUnitTypeID").val() }, function(response) {
                    var $html='';
                    var p = eval("(" + response + ")");
                    if(p) {
                        $html = '<select name="AccessoriesCB[]" multiple="multiple" id="AccessoriesCB" >';
                        for($a=0;$a<p.length;$a++)
                        {    
                             $html += '<option value="'+p[$a][0]+'" >'+p[$a][1]+'</option>';
                        } 
                        $html += '</select>';
                    }
                    else
                       $html = '<input  type="text" maxlength="200" style="width:335px;" class="text" name="Accessories" id="jbAccessories" value="" >';
                    $("#AccessoriesSpan1").html($html);
                    $("#AccessoriesCB").multiselect( {  noneSelectedText: "{$page['Text']['select_accessories']|escape:'html'}", minWidth: 100, height: "auto", show:['slide', 500]  } );
                    $(".ui-multiselect").css("width", "342px");
                });
            }
            
            
	    
	    $("#jbUnitTypeID").combobox({
		change: function() {
                    unitTypeChange();
                    checkManufacturerWanrranty();
		}
	    });
            
	    if($("#jbUnitTypeID").val() !== '-1') {
                unitTypeChange();
            }
            
	    $("#jbServiceTypeID").combobox({ change: function() { 
          
                     checkManufacturerWanrranty();
  
              } });
            
            $("#jbMobilePhoneNetworkID").combobox();
            
            
            
	    
	    
	    $("#jbProductLocation").combobox({ 
		change: function() { 
                
		    if($("#ImeiNoP")) {
			if($("#jbProductLocation").val() == "Branch" || $("#jbProductLocation").val() == "Service Provider") {
			    if($("#ImeiNoP label sup").length == 0) {
				$("#ImeiNoP label").html($("#ImeiNoP label").html() + "<sup>*</sup>");
			    }
			} else if($("#ImeiNoP label sup").length != 0) {
			    $("#ImeiNoP label sup").remove();
			}
		    }
                
                    if($("#jbProductLocation").val()=="Branch")
                    {
                        $('#BranchLocationDetailsIcon').show();    
                    }   
                    else
                    {
                        $('#BranchLocationDetailsIcon').hide(); 
                    }
                    
                
          
		    if($("#jbProductLocation").val()=="Branch" || $("#jbProductLocation").val()=="Other") {

			if($("#jbProductLocation").val()=="Other") {
			    $("#jbBranchIDElement").hide();
			    //$("#isProductAtBranchP").css("display","block");
			    
			    //SSSS
			    $("#isProductAtBranchYes").removeAttr("disabled");
			    $("#isProductAtBranchNo").removeAttr("disabled");
                            $("#isProductAtBranchP").show();
			    
			    if($("#jbProductLocation").is(":checked")) {
				enableManualAddress();
			    } else {
				disableManualAddress();
			    }
			} else {    
			    $("#jbBranchIDElement").show();
			    //$("#isProductAtBranchP").css("display","none");
			    //SSSS
			    $("#isProductAtBranchYes").attr("disabled", "disabled");
			    $("#isProductAtBranchNo").attr("disabled", "disabled");
                            $("#isProductAtBranchP").hide();
			}

			setValue("#jbBranchID");

			collectionAddressFields(true);

			if($("#jbProductLocation").val()!="Other") {    
			    $( "#jbBranchID" ).combobox({ 
				change: function() {  
				    $sltBranchID = $("#jbBranchID").val();
				    fillCollectionAddress($sltBranchID);
				} 
			    });
			    
                            $sltBranchID = $("#jbBranchID").val();
			    
                            fillCollectionAddress($sltBranchID);
                            
                            $(window).delay(1000).queue(function() {
                            
                            
                                    if($("#jbProductLocation").val()=="Branch")
                                    {

                                          if($('#jbBranchID').val()=='' || $("#jbColAddPostalCode").val()=='' || $("#jbColAddStreet").val()=='' || $("#jbColAddCity").val()=='' || $("#jbColAddCountry").val()=='')
                                          {

                                              $('#BranchLocationDetailsIcon').removeClass('plusIcon').addClass("minusIcon");
                                              $('#BranchLocationDetailsIcon').attr('src', '{$_subdomain}/css/Skins/{$_theme}/images/minus-icon.png');

                                              collectionAddressFields(true, false);
                                          }
                                          else
                                          {
                                              $('#BranchLocationDetailsIcon').removeClass('minusIcon').addClass("plusIcon");
                                              $('#BranchLocationDetailsIcon').attr('src', '{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png');
                                              collectionAddressFields(false, false);
                                          }   

                                    }
                                $(this).dequeue();
                                                                    
                            });//set delay ends here...    
                            
			}

		    } else {
			    
			//SSSS
			$("#isProductAtBranchYes").attr("disabled", "disabled");
			$("#isProductAtBranchNo").attr("disabled", "disabled");
			$("#isProductAtBranchP").hide();
			//$("#isProductAtBranchP").css("display","none");
			
			$("#jbBranchIDElement").hide();
			collectionAddressFields(false);

		    }
                    
                    
                    if($("#jbProductLocation").val()=="Branch" || $("#jbProductLocation").val()=="Service Provider")
                    {
                     
                        $("#jbAccessoriesElement").show();
                        $("#jbAccessoriesElement").removeClass('not-compulsory');
                        $("#jbUnitConditionElement").show();
                    }
                    else
                    {
                        $("#jbAccessoriesElement").hide();
                        $("#jbAccessoriesElement").addClass('not-compulsory');
                        $("#jbUnitConditionElement").hide();    
                    }
                    
                    
  
		}
		
	    });
              
              
            {if $jb_datarow.ShowStockCode eq 1}
    
             $( "#jbStockCode" ).combobox({ change: function() { 
          
                       assignDetails($("#jbStockCode").val().split(";;"));
                       
                       $("#jbStockCode").focus();
  
              } });
           
            {/if}

              
            
          
		$( "#jbModelName" ).autocomplete({
			
                        
                        source: function( request, response ) {
                                $.ajax({
                                    url: "{$_subdomain}/Data/getModelNumbers/list=1/manufacturer="+$("#jbManufacturerID").val()+"/",
                                    dataType: "json",
                                    data: {
                                        featureClass: "P",
                                        style: "full",
                                        maxRows: 12,
                                        name_startsWith: request.term
                                    },
                                    success: function( data ) {
                                        response( $.map( data.models, function( item ) {
                                            return {
                                                label: item.ModelNumber,
                                                value: item.ModelNumber
                                            }
                                        }));
                                    }
                                });
                        },
                        
                        minLength: 2,
                        
                        
                        change: function(event, ui) { 
                       
                        if($( "#jbModelName" ).val()) {
                        
                            
                            $.post("{$_subdomain}/Data/getModelNumbers/manufacturer="+$("#jbManufacturerID").val()+"/",        

                            { ModelNumber: $( "#jbModelName" ).val() },   
                                
                            function(data){
                                // DATA NEXT SENT TO COLORBOX
                                var p = eval("(" + data + ")");
                                if(p)
                                {
                                       
                                       assignModelDetails(p); 
                                       $( "#jbModelName" ).focus();
                                }
                                
                                
                            }); //Post ends here...
                        
                        
                        }
                        
                        },
                        
                        select: function(event, ui) { 
                         
                            if(ui.item && ui.item.value) {
                                
                                $.post("{$_subdomain}/Data/getModelNumbers/manufacturer="+$("#jbManufacturerID").val()+"/",        

                                { ModelNumber: ui.item.value },   

                                function(data){
                                    // DATA NEXT SENT TO COLORBOX
                                    var p = eval("(" + data + ")");
                                    if(p)
                                    {
                                           assignModelDetails(p);
                                           $( "#jbModelName" ).focus();
                                    } 
                                    
                                }); //Post ends here...
                        
                            }
                        }
                    
		});  
                
                
                
                
                
              $( "#OriginalRetailerFullPart" ).autocomplete({
			
                        
                        source: function( request, response ) {
                                $.ajax({
                                    url: "{$_subdomain}/Data/getRetailerNames/list=1/BranchID="+$("#jbBranchID").val()+"/",
                                    dataType: "json",
                                    data: {
                                        featureClass: "P",
                                        style: "full",
                                        maxRows: 12,
                                        name_startsWith: request.term
                                    },
                                    success: function( data ) {
                                        response( $.map( data.retailers, function( item ) {
                                            return {
                                                label: item.RetailerName,
                                                value: item.RetailerName
                                            }
                                        }));
                                    }
                                });
                        },
                        
                        minLength: 2,
                        
                        
                        change: function(event, ui) { 
                       
                      {*  if($( "#OriginalRetailerFullPart" ).val()) {
                        
                            
                            $.post("{$_subdomain}/Data/saveOriginalRetailer/BranchID="+$("#jbBranchID").val()+"/",        

                            { RetailerName: $( "#OriginalRetailerFullPart" ).val() },   
                                
                            function(data){
                                // DATA NEXT SENT TO COLORBOX
                                var p = eval("(" + data + ")");
                                if(p)
                                {
                                       
                                       $( "#OriginalRetailerFullPart" ).focus();
                                }
                                
                                
                            }); //Post ends here...
                        
                        
                        }*}
                        
                        },
                        
                        select: function(event, ui) { 
                         
                           
                        }
                    
		});  
                
                
                
              
              
              
             
               $( "#jbCustomerName input:first" ).css("width", "60px");
               $( "#jbCustomerName select:first" ).css("width", "60px");
               
               
               
              
              $("#DateOfPurchaseDateOfPurchaseDay").combobox({ change: function() {  
                                
                                                       purchaseDateSet(); 

                                                    } });
              $("#DateOfPurchaseDateOfPurchaseMonth").combobox({ change: function() {  
                                
                                                       purchaseDateSet(); 

                                                    } });
              $("#DateOfPurchaseDateOfPurchaseYear").combobox({ change: function() {  
                                
                                                       purchaseDateSet(); 

                                                    } });
              
              
              {if $jb_datarow.DateOfPurchase eq ''}

                $("#DateOfPurchaseDateOfPurchaseDay").val('');
                setValue("#DateOfPurchaseDateOfPurchaseDay", ":first");

                $("#DateOfPurchaseDateOfPurchaseMonth").val('');
                setValue("#DateOfPurchaseDateOfPurchaseMonth", ":eq(1)");

                $("#DateOfPurchaseDateOfPurchaseYear").val('');
                setValue("#DateOfPurchaseDateOfPurchaseYear", ":eq(2)");
                
              {/if}
              
              
              $( "#DateOfPurchase" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#DateOfPurchase").val()!="dd/mm/yyyy")
                        {    
                            $("#DateOfPurchase").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                            $dateArray = dateText.split("/"); 
                            
                            
                            $("#DateOfPurchaseDateOfPurchaseDay").val($dateArray[0]);
                            setValue("#DateOfPurchaseDateOfPurchaseDay", ":first");
                            
                            $("#DateOfPurchaseDateOfPurchaseMonth").val($dateArray[1]);
                            setValue("#DateOfPurchaseDateOfPurchaseMonth", ":eq(1)");
                            
                            $("#DateOfPurchaseDateOfPurchaseYear").val($dateArray[2]);
                            setValue("#DateOfPurchaseDateOfPurchaseYear", ":eq(2)");
                            
                             
                        }
                    });
               
              
          
           //Making first letter of each word capital of last name.           
           $('#jbContactLastName').blur(function(evt){
                var txt = $(this).val();
                $(this).val(ucwords(txt,false));
            });
            
            
            //Removing spaces and non numeric characters from telephone numbers
             $('#jbContactHomePhone').blur(function(evt){
                var txt = $(this).val();
                txt =  txt.replace(/[^0-9$.,]/g, '');
                $(this).val(txt);
            });
            
            $('#ContactHomePhoneExt').blur(function(evt){
                var txt = $(this).val();
                txt =  txt.replace(/[^0-9$.,]/g, '');
                $(this).val(txt);
            });
            
            $('#jbContactMobile').blur(function(evt){
                var txt = $(this).val();
                txt =  txt.replace(/[^0-9$.,]/g, '');
                $(this).val(txt);
            });
            
            

 
          
             
            
             function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
             {
                    $($countyId+" option:selected").each(function () {

      
                         $selected_cc_id =  $($countyId+" option:selected").attr('id');

                         $country_id_array = $selected_cc_id.split('_');

                            
                         if($country_id_array[1]!='0')
                         {

                             $($countryId).val($country_id_array[1]);
                                     
                             setValue($countryId);
                                     
                                
                         }
                         else
                         {
                              $($parentCountryId+" input:first").val('');
                         }

                 });
             }
 
              //Auto complete for drop downs ends here...
              

	    //Prefill values when job is being booked directly from product page.
	      
	    if($("#jbStockCodePrefill")) {
		$("#jbStockCodeElement .ui-combobox-input").val($("#jbStockCodePrefill").val());
	    }
	    if($.type($('#jbModelNamePrefill').val()) !== "undefined") {
		$("#jbModelName").val($("#jbModelNamePrefill").val());
	    }
	    if($.type($("#jbUnitTypeIDPrefill").val()) !== "undefined") {
		$("#jbUnitTypeIDElement .ui-combobox-input").val($("#jbUnitTypeIDPrefill").val());
	    }
	    if($.type($("#jbManufacturerIDPrefill").val()) !== "undefined") {
		$("#jbManufacturerIDElement .ui-combobox-input").val($("#jbManufacturerIDPrefill").val());
	    } 
	
        
               /* =======================================================
                *
                * Initialise input auto-hint functions...
                *
                * ======================================================= */

                $('.auto-hint').focus(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                        else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).blur(function() {
                        $this = $(this);
                        if ($this.val() == '' && $this.attr('title') != '')  {
                            $this.val($this.attr('title')).addClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','text');
                            }
                        } else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).each(function(){
                        $this = $(this);
                        
                        if ($this.attr('title') == '') { return; }
                        if ($this.val() == '') { 
                            if ($this.attr('type') == 'password') {
                                $this.addClass('auto-pwd').prop('type','text');
                            }
                            $this.val($this.attr('title')); 
                        } else {
                        
                                $this.removeClass('auto-hint'); 
                        }
                        $this.attr('autocomplete','off');
                    } );
       
       
       
               /* =======================================================
                *
                * set tab on return for input elements with form submit on auto-submit class...
                *
                * ======================================================= */

                $('input[type=text],input[type=password]').keypress( function( e ) {
                        if (e.which == 13) {
                            $(this).blur();
                            if ($(this).hasClass('auto-submit')) {
                                $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                                } );
                                $(this).get(0).form.onsubmit();
                            } else {
                                $next = $(this).attr('tabIndex') + 1;
                                $('[tabIndex="'+$next+'"]').focus();
                            }
                            return false;
                        }
                    } );  
        
        
        
                    $(document).on('keyup', '#postSelect', 
                    function(e) {
        
                    
                    
                    var key = (e.which) ? e.which : e.keyCode;
                    if( key == 13 ){ 
                
                        addFields($("#postSelect").val());
                    }
                
                
                   });
                   
                   
                   $(document).on('keyup', '#postSelect_1', 
                    function(e) {
        
                    
                    var key = (e.which) ? e.which : e.keyCode;
                    if( key == 13 ){ 
                
                        addAddressFields($("#postSelect_1").val());
                    }
                
                
                   });
        
                {if $jb_datarow.ProductLocation neq ''}
                    
                    $("#jbProductLocation").val("{$jb_datarow.ProductLocation}");
                    $("#jbProductLocation").blur();
                    setValue("#jbProductLocation");
                    
                {/if}
                    
                    
                {if $jb_datarow.ManufacturerID neq ''}
                    
                    $("#jbManufacturerID").val("{$jb_datarow.ManufacturerID}");
                    $("#jbManufacturerID").blur();
                    setValue("#jbManufacturerID");
                    
                {/if}    
                    
                    
               
               
               $(document).on('click', '#jbAccessoriesRadio', 
                    function(e) {
                    
                        $('#jbAccessories').val('');
                        $('.ui-multiselect-none').trigger("click");
                        
                        setHiddenAccessoriesFlag();
                        
                    });
                    
             $(document).on('click', '.ui-multiselect-all', 
                    function(e) {
                    
                        checkCheckboxStatus();
                        setHiddenAccessoriesFlag();
                        
                    });
                    
                    
             $(document).on('click', '.ui-multiselect-none', 
                    function(e) {
                    
                        checkCheckboxStatus();
                        setHiddenAccessoriesFlag();
                        
                    });        
                    
                    
                    
                    
               $(document).on('click', '.ui-multiselect-checkboxes input', 
                    function(e) {
                    
                        checkCheckboxStatus();
                        setHiddenAccessoriesFlag();
                        
                    });     
                    
               function checkCheckboxStatus ()
               {
                   
                      var  $CheckFlag = false;
                         
                         $('.ui-multiselect-checkboxes input').each(function() {
                                
                               
                                if($(this).is(':checked')) {

                                       $CheckFlag = true;
                                       
                                    }
                               
                         }); 
                         
                        if($CheckFlag)
                        {
                            $('#jbAccessoriesRadio').removeAttr("checked");
                        }
                   return $CheckFlag;
               }    
                    
                   
               $(document).on({
                    keyup: function() {
                   
                         if($('#jbAccessories').val()!='')
                         {
                                 $('#jbAccessoriesRadio').removeAttr("checked");
                                 
                         }
                         setHiddenAccessoriesFlag();
                    },
                    blur: function() {
                        
                         if($('#jbAccessories').val()!='')
                         {
                                 $('#jbAccessoriesRadio').removeAttr("checked");
                                
                         }
                         setHiddenAccessoriesFlag();
                    },
                    click: function() {
                       
                         if($('#jbAccessories').val()!='')
                         {
                                 $('#jbAccessoriesRadio').removeAttr("checked");
                                
                         } 
                         setHiddenAccessoriesFlag();
                    },
                    focus: function() {
                        
                         if($('#jbAccessories').val()!='')
                         {
                                 $('#jbAccessoriesRadio').removeAttr("checked");
                                 
                         } 
                         setHiddenAccessoriesFlag();
                    }
                }, "#jbAccessories");
                 
               
               function setHiddenAccessoriesFlag()
               {
                   
                   if($('#jbAccessoriesRadio').is(':checked') || ($('#jbAccessories').length > 0 && $('#jbAccessories').val()!='') || checkCheckboxStatus())
                   {
                        $('#HiddenAccessoriesFlag').val('1');
                        
                   }    
                   else
                   {
                        $('#HiddenAccessoriesFlag').val(''); 
                        
                   }
               }    
               
               
                    
               $(document).on('click', '#DisplayCompulsory', 
                    function(e) {     
                    
                        if($(this).is(':checked'))
                        {    
                            hideNotCompulsoryElements();
                            var $serialText = 1;
                        }
                        else
                        {
                            showNotCompulsoryElements();
                            var $serialText = 0;
                        }
                        
                       if($serialText)
                       {
                            $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=jobBooking/Type=cf",        
                                { 'StatusID[]': $serialText }, 
                                function(data) {
                                   
                                }
                            ); //Post ends here...
                    
                       }
                       else
                       {
                            $.post("{$_subdomain}/Data/updateUserStatusPreferences/Page=jobBooking/Type=cf",        
                                { }, 
                                function(data) {
                                   
                                }
                            ); //Post ends here...
                       }
                    
                    });
                    
                {if $jb_datarow.DisplayCompulsory}
            
                    hideNotCompulsoryElements();     
                    
              
                {/if}
       
	});
         
         function hideNotCompulsoryElements()
         {
          
            $(window).delay(1000).queue(function() {
            

                $('.not-compulsory').each(function() {

                        if($(this).is(':visible')) {

                                $(this).addClass('visible-element');
                                $(this).hide();

                            }

                 }); 
                 
                  $(this).dequeue();
            });
         }
         
         
         function showNotCompulsoryElements()
         {
            $(window).delay(1000).queue(function() {
            
            
                    $('.not-compulsory').each(function() {

                            if($(this).hasClass('visible-element')) {

                                    $(this).removeClass('visible-element');
                                    $(this).show();

                                }

                     }); 
                     
                $(this).dequeue();
            });     
         }
         
          function addressLookup() {
            $.ajax( { type:'POST',
                    dataType:'html',
                    data:'postcode=' + $('#jbPostalCode').val(),
                    success:function(data, textStatus) { 
                              
                              if(data=='EM1011' || data.indexOf("EM1011")!=-1) {
                                  
                                    $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                    $("#selectOutput").slideDown("slow");
                                    
                                    $("#jbBuildingName").val('');	
                                    $("#jbStreet").val('');
                                    $("#jbArea").val('');
                                    $("#jbCity").val('');
                                    
                                    $("#jbCounty").val('').blur();
                                    setValue("#jbCounty");
                                    $("#jbCountry").val('').blur();
                                    setValue("#jbCountry");

                                    $("#jbCompanyName").val('').blur();
                                    $("#jbPostalCode").focus();

                                     
                                   
                              } else {
                              
                                 $('#selectOutput').html(data); 
                                 $("#selectOutput").slideDown("slow");
                                 $("#postSelect").removeClass();
                                 $("#postSelect").removeAttr("style");
                                 location.href = "#jbContactMobile";
                                 $("#postSelect").focus();
                                 
                              }
                                       
                           
                           
                          
                    },
                    beforeSend:function(XMLHttpRequest){ 
                            $('#fetch').fadeIn('medium'); 
                    },
                    complete:function(XMLHttpRequest, textStatus) { 
                            $('#fetch').fadeOut('medium') 
                            setTimeout("$('#quickButton').fadeIn('medium')",500);
                    },
                    url:'{$_subdomain}/index/addresslookup' 
                } ); 
            return false;
        }
        
        
        function purchaseDateSet()
        {
            
           if($('#DateOfPurchaseDateOfPurchaseDay').val() && $('#DateOfPurchaseDateOfPurchaseMonth').val() && $('#DateOfPurchaseDateOfPurchaseYear').val())
           {
                   
                   $('#DateOfPurchase').val($('#DateOfPurchaseDateOfPurchaseDay').val()+'/'+$('#DateOfPurchaseDateOfPurchaseMonth').val()+'/'+$('#DateOfPurchaseDateOfPurchaseYear').val());
                   
                   checkManufacturerWanrranty();
           } 
           
        }
       
        function checkManufacturerWanrranty()
        {
            if($('#DateOfPurchase').val() && $('#jbServiceTypeID').val() && $('#jbUnitTypeID').val())
            {
                    $.post("{$_subdomain}/Data/checkManufacturerWanrranty/",        
                            { 'DateOfPurchase': $('#DateOfPurchase').val(), 'ServiceTypeID':$('#jbServiceTypeID').val(),  'UnitTypeID':$('#jbUnitTypeID').val() }, 
                            function(data) {
                    
                                var p = eval("(" + data + ")");
                            
                                if(p)
                                {
                                    {*if(confirm("{$page['Text']['warranty_warning']}"))
                                    {
                                            
                                    }
                                    else
                                    {
                                        $('#jbServiceTypeID').val('');
                                        $('#jbServiceTypeID').blur();
                                        setValue('#jbServiceTypeID');
                                    }*}

                                   //It opens color box popup page.              
                                    $.colorbox( { href: '{$_subdomain}/Popup/warrantyWarning/'+ Math.random(),
                                                    title: "{$page['Text']['manufacturers_warranty_period']|escape:'html'}",
                                                    opacity: 0.75,
                                                    height:510,
                                                    width:740,
                                                    overlayClose: false,
                                                    escKey: false, 
                                                    onComplete: function(){

                                                     $(this).colorbox.resize({
                                                        height: ($('#WarrantyWarningDiv').height()+150)+"px"                                
                                                    });

                                                    }

                                                 });      



                                }    
                                else
                                {
                                    
                                }
                            }
                        ); //Post ends here...
            }
        }
        
       
        function addFields(selected) {	
        
            if(selected)
            {
                var selected_split = selected.split(","); 	

                
                $("#jbBuildingName").val(trim(selected_split[0])).blur();	
                $("#jbStreet").val(trim(selected_split[1])).blur();
                $("#jbArea").val(trim(selected_split[2])).blur();
                $("#jbCity").val(trim(selected_split[3])).blur();
                $("#jbCounty").val(trim(selected_split[4])).blur();
                setValue("#jbCounty");
                $("#jbCountry").val(trim(selected_split[5])).blur();
                setValue("#jbCountry");
                
                $("#jbCompanyName").val(trim(selected_split[6])).blur();
                
                
                
                
                {*if($('#CustomerTypeBusiness').is(':checked'))
                    {
                        $("#jbCompanyName").focus();
                    }
                 else {
                    
                    if(selected_split[6])
                    {
                            $("#jbCompanyName").removeAttr("disabled"); 
                            $("#jbCompanyNameElement").show();  
                            $("#jbCompanyName").focus();
                    }
                    else
                    {
                            $("#jbCompanyName").attr("disabled", "disabled"); 
                            $("#jbCompanyNameElement").hide();  
                             $("#jbBuildingName").focus();
                    }
                   
                 }*}
                    
                
                
                $("#selectOutput").slideUp("slow");
                
            }    
        }
        
        
        
        function addAddressFields(selected, $flag) {	
        
            if(selected)
            {
                var selected_split = selected.split(","); 	

                
                $("#jbColAddBuildingName").val(trim(selected_split[0])).blur();	
                $("#jbColAddStreet").val(trim(selected_split[1])).blur();
                $("#jbColAddArea").val(trim(selected_split[2])).blur();
                $("#jbColAddCity").val(trim(selected_split[3])).blur();
                
                $("#jbColAddCounty").val(trim(selected_split[4])).blur();
                setValue("#jbColAddCounty");
                
                $("#jbColAddCountry").val(trim(selected_split[5])).blur();
                setValue("#jbColAddCountry");
                
                 $("#jbColAddCompanyName").val(trim(selected_split[6])).blur();
                
                $("#jbColAddCompanyName").val(trim(selected_split[6])).blur();
                
                $("#selectColAddOutput").slideUp("slow");
                
            }    
        }
        
        
        
        
      
      
       function addressLookup2() {
            $.ajax( { type:'POST',
                    dataType:'html',
                    data:'postcode=' + $('#jbColAddPostalCode').val(),
                    success:function(data, textStatus) { 
                              
                              if(data=='EM1011' || data.indexOf("EM1011")!=-1) {
                                  
                                    $('#selectColAddOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                    $("#selectColAddOutput").slideDown("slow");
                                    
                                   
                                    $("#jbColAddCompanyName").val('');
                                    $("#jbColAddBuildingName").val('');	
                                    $("#jbColAddStreet").val('');
                                    $("#jbColAddArea").val('');
                                    $("#jbColAddCity").val('');
                                    $("#jbColAddCounty").val('').blur();
                                    setValue("#jbColAddCounty");
                                    $("#jbColAddCountry").val('').blur();
                                    setValue("#jbColAddCountry");
                                    $("#jbColAddEmail").val('');
                                    $("#jbColAddPhone").val('');
                                    $("#ColAddPhoneExt").val('');
                                    
                                    $("#jbColAddPostalCode").focus();
                                  
                                   
                              } else {
                              
                                 $('#selectColAddOutput').html(data); 
                                 $("#selectColAddOutput").slideDown("slow");
                                 $("#postSelect_1").removeClass();
                                 $("#postSelect_1").removeAttr("style");
                                 
                                
                                 location.href = "#jbColAddPostalCode";
                                 $("#postSelect_1").focus();
                              }
                                       
                           
                           
                          
                    },
                    beforeSend:function(XMLHttpRequest){ 
                            $('#fetch').fadeIn('medium'); 
                    },
                    complete:function(XMLHttpRequest, textStatus) { 
                            $('#fetch').fadeOut('medium') 
                            setTimeout("$('#quickButton').fadeIn('medium')",500);
                    },
                    url:'{$_subdomain}/index/addresslookup/1' 
                } ); 
            return false;
        }
      
        
        
        
        
        
        
       
       function locateByDetails($eNum, $focus)
       {
          
           if($eNum=='1')
           {
                
                $("#jbModelNameRow").show("fast"); 
                $("#jbModelName").attr("title", "{$page['Text']['model_number_title']|escape:'html'}"); 
                
                if($focus)
                {
                    $("#jbModelName").focus();
                }
                $("#jbProductDescriptionRow").hide("fast"); 
                $("#jbProductDescription").val('');
                 
           }
           else  if($eNum=='2')
           {
                $("#jbProductDescriptionRow").show("fast"); 
                $("#jbProductDescription").attr("title", "{$page['Text']['text_description_title']|escape:'html'}"); 
                if($focus)
                {
                    $("#jbProductDescription").focus();
                }
                
                $("#jbModelName").val(''); 
                $("#jbModelNameRow").hide("fast");
                 
           }
       }
         
       
</script>

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> /  {$page_title|escape:'html'}
    </div>
</div>
        
<div class="main" id="home">
                               
    <div class="jobBookingPanel" >
         <form id="jobBookingForm" name="jobBookingForm" method="post"  action="#" class="inline" autocomplete="off" >
       
             
            <fieldset>
            <legend title="AP7108" >{$page_legend|escape:'html'}</legend>
            
            
            <p>
               <label >&nbsp;</label>
               <span class="jobBookingTopText" > {$page['Text']['instruction_text1']|escape:'html'}<span >*</span> {$page['Text']['instruction_text2']|escape:'html'}</span>
               
               <span style="float:right;" ><input type="checkbox" name="DisplayCompulsory" id="DisplayCompulsory" value="1"  {if $jb_datarow.DisplayCompulsory} checked="checked" {/if} > Display compulsory fields only</span>
               <br><br>
            </p>
           
              
           {if $jb_settings.CustomerTypeChk1 eq 1} 
            
            <p id="jbCustomerTypeElement"  {if $jb_settings.CustomerTypeChk2 neq 1} class="not-compulsory" {/if} >
               <label for="jbCustomerType" >{$page['Labels']['customer_type']|escape:'html'}: {if $jb_settings.CustomerTypeChk2 eq 1} <sup>*</sup> {/if} </label>
               &nbsp;&nbsp; 
               <input  type="radio" name="CustomerType" id="CustomerTypeConsumer" value="Consumer" {if $jb_datarow.CustomerType eq "Consumer"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['consumer']|escape:'html'}</span>   
               <input  type="radio" name="CustomerType" id="CustomerTypeBusiness" value="Business" {if $jb_datarow.CustomerType eq "Business"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['business']|escape:'html'}</span>    
            </p> 
            
           {/if}
            
          
         {if $jb_settings.ContactTitleChk1 eq 1}   
            
         <p id="jbCustomerNameTop" {if $jb_settings.ContactTitleChk2 neq 1} class="not-compulsory" {/if} >
            <label>&nbsp;</label>
            <label style="width:60px;" class="topTextLabel"  >{$page['Labels']['title']|escape:'html'}:<span style="color:red;" >{if $jb_settings.ContactTitleChk2 eq 1} <sup>*</sup> {/if}</span></label>
            <label style="width:150px;" class="topTextLabel not-compulsory"  >{$page['Labels']['forename']|escape:'html'}: </label>
            <label style="width:150px;" class="topTextLabel" >{$page['Labels']['surname']|escape:'html'}:<span style="color:red;" >{if $jb_settings.ContactTitleChk2 eq 1} <sup>*</sup> {/if}</span></label>
         </p>  
              
         <p id="jbCustomerName" {if $jb_settings.ContactTitleChk2 neq 1} class="not-compulsory" {/if} >
            <label for="jbContactTitle" id="jbContactTitleLabel" >{$page['Labels']['name']|escape:'html'}:{if $jb_settings.ContactTitleChk2 eq 1} <sup>*</sup> {/if}</label>
               &nbsp;&nbsp;  
               <select  name="CustomerTitleID" id="jbContactTitle" title="{$page['Text']['contact_title']|escape:'html'}"  class="text auto-hint" style="width:60px;" >
                   
                   <option value="" {if $jb_datarow.ContactTitle eq ''}selected="selected"{/if}   ></option>
                   {foreach from=$selectTitle item=stitle}
                    <option value="{$stitle.TitleID}" {if $jb_datarow.ContactTitle eq $stitle.TitleID}selected="selected"{/if} >{$stitle.Name|escape:'html'}</option>
                    {/foreach}
                   
                </select> 
                    
               <input type="text" style="width:150px;" class="text lowercaseText capitalizeText not-compulsory" name="ContactFirstName" value="{$jb_datarow.ContactFirstName|escape:'html'}"  id="jbContactFirstName" >
                
               <input type="text" style="width:150px;" class="text" name="ContactLastName" value="{$jb_datarow.ContactLastName|escape:'html'}" id="jbContactLastName" > 
            </p>
        
          {/if}  
            
        
        <p>
           <br> <hr style="height:1px" >
        </p>
        
        
        
        {if $jb_settings.BranchIDAddressChk1 eq 1}
            
            <p style="text-align:center;padding:10px;"  {if $jb_settings.BranchIDAddressChk2 neq 1} class="not-compulsory" {/if}  >
               <span class="blueText"  >{$page['Text']['branch_address_info']|escape:'html'}</span>
            </p>
            <p id="jbBranchAddressElement" {if $jb_settings.BranchIDAddressChk2 neq 1} class="not-compulsory" {/if}  >
                <label for="jbBranchAddress" >{$page['Labels']['branch_address']|escape:'html'}:{if $jb_settings.BranchIDAddressChk2 eq 1} <sup>*</sup> {/if}</label>
                   &nbsp;&nbsp; <select  name="BranchIDAddress" id="jbBranchAddress"   class="text" >

                       <option value="" {if $BranchIDAddress eq ''}selected="selected"{/if}   >{$page['Text']['select_default_option']|escape:'html'}</option>
                       {foreach from=$BranchDetails item=BA}
                           
                        <option value="{$BA.BuildingNameNumber|escape:'html'};;{$BA.Street|escape:'html'};;{$BA.LocalArea|escape:'html'};;{$BA.TownCity|escape:'html'};;{$BA.CountyID|escape:'html'};;{$BA.CountryID|escape:'html'};;{$BA.PostalCode|escape:'html'};;{$BA.ContactPhone|escape:'html'};;{$BA.ContactPhoneExt|escape:'html'};;{$BA.ContactEmail|escape:'html'};;{$BA.BranchName|escape:'html'}" {if $BranchIDAddress eq $BA.BranchID}selected="selected"{/if} >{$BA.BranchName|escape:'html'}</option>
                        
                       {/foreach}

                    </select>
                 
                 {if $displayAddBranchLink}   
                     
                     <a href="{$_subdomain}/OrganisationSetup/branches/{$jb_datarow.NetworkID|escape:'html'}/{$jb_datarow.ClientID|escape:'html'}/jobPage=1" target="_blank"  title="{$page['Text']['add_branch']|escape:'html'}" ><img alt="{$page['Text']['add_branch']|escape:'html'}" title="{$page['Text']['add_branch']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png" width="24" height="24" style="position:relative;top:6px;cursor:pointer;" /></a> 
                     
                     <img id="jbBranchesReload1" alt="{$page['Text']['reload_branch']|escape:'html'}" title="{$page['Text']['reload_branch']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/refresh-icon.png" width="20" height="20" style="position:relative;top:3px;cursor:pointer;" />
           
                     <a href="#" id="jbFreeTextAddress" title="{$page['Text']['free_text_address_title']|escape:'html'}" ></a>
                     
                 {else}    
                     
                  &nbsp;&nbsp;
                  <a href="#" id="jbFreeTextAddress" title="{$page['Text']['free_text_address_title']|escape:'html'}" >{$page['Text']['free_text_address']|escape:'html'}</a>
                     
                 {/if}      
                 
                  
                  
            </p>
        
        {/if}
        
        
        {if $jb_settings.ContactEmailChk1 eq 1}
            <p {if $jb_settings.ContactEmailChk2 neq 1} class="not-compulsory" {/if} >
               <label for="jbContactEmail" id="jbContactEmailLabel" >{$page['Labels']['email']|escape:'html'}:{if $jb_settings.ContactEmailChk2 eq 1} <sup id="jbContactEmailSup" >*</sup> {/if}</label>
                 &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$jb_datarow.ContactEmail|escape:'html'}" id="jbContactEmail" >
                <br>
                 <span class="text jobBookingValueInner" >

                 <input  type="checkbox" name="NoContactEmail"  id="jbNoContactEmail" tabindex="100"  >{$page['Text']['no_email']|escape:'html'}
                 </span><br>

            </p>
        {/if}
        
        
        
        {if $jb_settings.ContactHomePhoneChk1 eq 1}
            <p {if $jb_settings.ContactHomePhoneChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbContactHomePhone" id="jbContactHomePhoneLabel" >{$page['Labels']['daytime_phone']|escape:'html'}: {if $jb_settings.ContactHomePhoneChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactHomePhone" value="{$jb_datarow.ContactHomePhone|escape:'html'}" id="jbContactHomePhone" >
                <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactHomePhoneExt" value="{$jb_datarow.ContactHomePhoneExt|escape:'html'}" id="ContactHomePhoneExt" >

            </p>
        {/if}
        
        {if $jb_settings.ContactMobileChk1 eq 1}
            <p {if $jb_settings.ContactMobileChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbContactMobile" id="jbContactMobileLabel" >{$page['Labels']['mobile_no']|escape:'html'}: {if $jb_settings.ContactMobileChk2 eq 1} <sup id="jbContactMobileSup" >*</sup> {/if}</label>
                &nbsp;&nbsp; <input type="text" class="text auto-hint" name="ContactMobile" class="text auto-hint" id="jbContactMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$jb_datarow.ContactMobile|escape:'html'}" >

                <br>
                 <span class="text jobBookingValueInner" >

                 <input  type="checkbox" name="NoContactMobile"  id="jbNoContactMobile" tabindex="100"  >{$page['Text']['no_mobile']|escape:'html'}
                 </span><br>

            </p>
        {/if}
        
        {if $jb_settings.ContactAltMobileChk1 eq 1}
            <p id="jbContactAltMobileElement"  {if $jb_settings.ContactAltMobileChk2 neq 1} class="not-compulsory" {/if} style="display:none;" >

                <label for="jbContactAltMobile" id="jbContactAltMobileLabel" >{$page['Labels']['alt_sms_no']|escape:'html'}:{if $jb_settings.ContactAltMobileChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input type="text" class="text auto-hint" name="ContactAltMobile" id="ContactAltMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="" >

                <br>

            </p>
        {/if}
        
        
        
        <p>
           <br> <hr style="height:1px" >
        </p>
        
        
       
           
            {if $jb_datarow.PostcodeLookup eq 1}

                        {if $jb_settings.PostalCodeChk1 eq 1}
                            <p {if $jb_settings.PostalCodeChk2 neq 1} class="not-compulsory" {/if} >
                                <label for="jbPostalCode" id="jbPostalCodeLabel" >{$page['Labels']['postcode']|escape:'html'}:{if $jb_settings.PostalCodeChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 230px;" type="text" name="PostalCode" id="jbPostalCode" value="{$jb_datarow.PostalCode|escape:'html'}" >&nbsp;

                                <input type="button" name="quick_find_btn" style="position: relative; top:1px;" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="addressLookup();return false;"   value="Click to find Address" >

                                &nbsp;&nbsp;<span id="fetch" style="display: none" class="blueText" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" ></span>

                            </p>
                            <p id="selectOutput" ></p>
                        {/if}     

                        {if $jb_settings.CompanyNameChk1 eq 1}
                            <p  id="jbCompanyNameElement" {if $jb_settings.CompanyNameChk2 neq 1} class="not-compulsory" {/if} >
                                <label for="jbCompanyName">{$page['Labels']['business_name']|escape:'html'}:{if $jb_settings.CompanyNameChk2 eq 1} <sup>*</sup> {/if}</label>
                                 &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="CompanyName" value="{$jb_datarow.CompanyName|escape:'html'}"  id="jbCompanyName" >

                            </p> 
                        {/if}    
                        
                        {if $jb_settings.BuildingNameNumberChk1 eq 1}
                            <p id="jb_p_house_name" {if $jb_settings.BuildingNameNumberChk2 neq 1} class="not-compulsory" {/if}  >
                                <label for="jbBuildingName" >{$page['Labels']['building_name']|escape:'html'}:{if $jb_settings.BuildingNameNumberChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="BuildingNameNumber" value="{$jb_datarow.BuildingName|escape:'html'}" id="jbBuildingName" >          
                            </p>
                        {/if}

                        {if $jb_settings.StreetChk1 eq 1}
                            <p id="jb_p_street" {if $jb_settings.StreetChk2 neq 1} class="not-compulsory" {/if}  >
                                <label for="jbStreet" >{$page['Labels']['street']|escape:'html'}:{if $jb_settings.StreetChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="Street" value="{$jb_datarow.Street|escape:'html'}" id="jbStreet" >          
                            </p>
                        {/if}    

                        {if $jb_settings.LocalAreaChk1 eq 1}
                            <p id="jb_p_area" {if $jb_settings.LocalAreaChk2 neq 1} class="not-compulsory" {/if} >
                                <label for="jbArea" >{$page['Labels']['area']|escape:'html'}:{if $jb_settings.LocalAreaChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="LocalArea" value="{$jb_datarow.Area|escape:'html'}" id="jbArea" >          
                            </p>
                        {/if}    

                        {if $jb_settings.TownCityChk1 eq 1}
                            <p id="jb_p_town"  {if $jb_settings.TownCityChk2 neq 1} class="not-compulsory" {/if} >
                                <label for="jbCity" >{$page['Labels']['city']|escape:'html'}:{if $jb_settings.TownCityChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="City" value="{$jb_datarow.City|escape:'html'}" id="jbCity" >          
                            </p>
                        {/if}

                        {if $jb_settings.CountyIDChk1 eq 1}
                            <p id="jb_p_county" {if $jb_settings.CountyIDChk2 neq 1} class="not-compulsory" {/if}  >
                                <label for="jbCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:{if $jb_settings.CountyIDChk2 eq 1} <sup>*</sup> {/if}</label>
                                &nbsp;&nbsp;  


                                <select name="CountyID" id="jbCounty" class="text" >
                                    <option value="" id="country_0_county_0" {if $jb_datarow.County eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $countries as $country}


                                        {foreach $country.Counties as $county}
                                        <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $jb_datarow.County eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                        {/foreach}


                                    {/foreach}

                                </select>

                            </p>
                        {/if}

                    {else}

{*
                        <p  id="jbCompanyNameElement" >
                            <label for="jbCompanyName">{$page['Labels']['business_name']|escape:'html'}:<sup>*</sup></label>
                             &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="CompanyName" value="{$jb_datarow.CompanyName|escape:'html'}"  id="jbCompanyName" >

                        </p>

                        <p>
                            <label for="jbBuildingName" >{$page['Labels']['building_name']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="BuildingName" value="{$jb_datarow.BuildingName|escape:'html'}" id="jbBuildingName" >          
                        </p>

                        <p>
                            <label for="jbStreet" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="Street" value="{$jb_datarow.Street|escape:'html'}" id="jbStreet" >          
                        </p>

                        <p>
                            <label for="jbArea" class="not-compulsory" >{$page['Labels']['area']|escape:'html'}:</label>
                            &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="Area" value="{$jb_datarow.Area|escape:'html'}" id="jbArea" >          
                        </p>


                        <p>
                            <label for="jbCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="City" value="{$jb_datarow.City|escape:'html'}" id="jbCity" >          
                        </p>

                        <p>
                            <label for="jbCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                            <select name="CountyID" id="jbCounty" class="text" >
                                <option value="" id="country_0_county_0" {if $jb_datarow.County eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $countries as $country}


                                    {foreach $country.Counties as $county}
                                    <option value="{$county.CountyID}" id="country_{$country.CountryID}_county_{$county.CountyID}" {if $jb_datarow.County eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                    {/foreach}


                                {/foreach}

                            </sexlect>        
                        </p>

                         <p>
                            <label for="jbPostalCode" id="jbPostalCodeLabel" >{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                            &nbsp;&nbsp; <input style="width: 230px;" class="text uppercaseText" type="text" name="postcode" id="jbPostalCode" value="{$jb_datarow.PostalCode|escape:'html'}" >
                        </p>
*}

            {/if}  
        
        
       
        
        
        {if $jb_settings.CountryIDChk1 eq 1}   
            <p id="jb_p_country" {if $jb_settings.CountryIDChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbCountry" >{$page['Labels']['country']|escape:'html'}: {if $jb_settings.CountryIDChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; 
                    <select name="CountryID" id="jbCountry" >
                    <option value="" {if $jb_datarow.Country eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $countries as $country}

                        <option value="{$country.CountryID}" {if $jb_datarow.Country eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                    {/foreach}

                </select>
            </p>
        {/if}
            
            
        
        
        
        <p>
            
           <br>  <hr style="height:1px" >
        </p>
        
        {if $jb_settings.ProductLocationChk1 eq 1}
            
            <p id="jbProductLocationElement" {if $jb_settings.ProductLocationChk2 neq 1} class="not-compulsory" {/if} >

                <label for="jbProductLocation" >{$page['Labels']['product_location']|escape:'html'}:{if $jb_settings.ProductLocationChk2 eq 1} <sup>*</sup> {/if}</label> &nbsp;&nbsp; 

                <select name="ProductLocation" id="jbProductLocation" class="text"  >

                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $product_location_rows as $plocation}
                        {if $plocation.ProductLocation eq $jb_datarow.ProductLocation}
                            <option value="{$plocation.ProductLocation}" selected="selected"  >{$plocation.Name|escape:'html'}</option>
                        {else}
                            <option value="{$plocation.ProductLocation}" >{$plocation.Name|escape:'html'}</option>
                        {/if}
                    {/foreach}

                </select>


                <img id="BranchLocationDetailsIcon" class="plusIcon" alt="{$page['Text']['expand']|escape:'html'}" title="{$page['Text']['expand']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png" width="24" height="24" style="position:relative;top:6px;cursor:pointer;" />


            </p>
        
        {/if}
             
        {if $jb_settings.ColBranchAddressIDChk1 eq 1} 
           
            <p id="jbBranchIDElement" title="{$page['Text']['collect_from_hint']|escape:'html'}"  {if $jb_settings.ColBranchAddressIDChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbBranchID" >{$page['Labels']['collect_from']|escape:'html'}:{if $jb_settings.ColBranchAddressIDChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <select name="BranchID" id="jbBranchID" class="text"  >

                    <option value="" selected="selected"  >{$page['Text']['pseudo_branch']|escape:'html'}</option>
                    <option value="-1" {if $jb_datarow.AutoPopulateBranchID=="-1"} selected="selected" {/if} >{$page['Text']['pseudo_branch']|escape:'html'}</option>

                    {foreach $branches as $branch}
                         <option value="{$branch.BranchID}" {if $branch.BranchID eq $jb_datarow.AutoPopulateBranchID} selected="selected" {/if} >{$branch.BranchName|escape:'html'}</option>
                    {/foreach}


                 </select>
                 
                 {if $displayAddBranchLink}   
                     
                     <a href="{$_subdomain}/OrganisationSetup/branches/{$jb_datarow.NetworkID|escape:'html'}/{$jb_datarow.ClientID|escape:'html'}/jobPage=1" target="_blank"  title="{$page['Text']['add_branch']|escape:'html'}" ><img alt="{$page['Text']['add_branch']|escape:'html'}" title="{$page['Text']['add_branch']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png" width="24" height="24" style="position:relative;top:6px;cursor:pointer;" /></a> 
                     
                     <img id="jbBranchesReload" alt="{$page['Text']['reload_branch']|escape:'html'}" title="{$page['Text']['reload_branch']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/refresh-icon.png" width="20" height="20" style="position:relative;top:3px;cursor:pointer;" />
                     
                 {/if}    

             </p>
             
        {/if}     
             
             
	    {foreach from=$BranchDetails item=BA}
                <span id="branch_address_{$BA.BranchID|escape:'html'}" style="display:none;" >{$BA.BuildingNameNumber|escape:'html'};;{$BA.Street|escape:'html'};;{$BA.LocalArea|escape:'html'};;{$BA.TownCity|escape:'html'};;{$BA.CountyID|escape:'html'};;{$BA.CountryID|escape:'html'};;{$BA.PostalCode|escape:'html'};;{$BA.ContactPhone|escape:'html'};;{$BA.ContactPhoneExt|escape:'html'};;{$BA.ContactEmail|escape:'html'}</span>
            {/foreach}
             
             
	    <input type="hidden" name="DefaultBranchID" id="jbDefaultBranchID"  value="{$jb_datarow.DefaultBranchID|escape:'html'}" />
            <input type="hidden" name="repJobId" id="repJobId"  value="{$replicateJobId}" />
            <input type="hidden" name="repType" id="repType"  value="{$replicateType}" />
             
            {if $jb_settings.isProductAtBranchChk1 eq 1} 
                
                <p id="isProductAtBranchP" style="display:none; margin:5px 0px 10px 0px;"   {if $jb_settings.isProductAtBranchChk2 neq 1} class="not-compulsory" {/if} >
                    <label>Is the product at one of your branches awaiting collection?{if $jb_settings.isProductAtBranchChk2 eq 1} <sup>*</sup> {/if}</label>
                    &nbsp;&nbsp; 
                    <input type="radio" name="isProductAtBranch" id="isProductAtBranchYes" value="Yes" />Yes
                    <input type="radio" name="isProductAtBranch" id="isProductAtBranchNo" value="No" />No, it is at another address
                </p>
                
	    {/if}

            {if $jb_settings.ColAddPostcodeChk1 eq 1}
                <p id="jbColAddPostalCodeElement" {if $jb_settings.ColAddPostcodeChk2 neq 1} class="not-compulsory" {/if} >
                    <label for="jbColAddPostalCode" id="jbColAddPostalCodeLabel" >
                        {$page['Labels']['collection_postcode']|escape:'html'}:{if $jb_settings.ColAddPostcodeChk2 eq 1} <sup>*</sup> {/if}
                    </label>
                    &nbsp;&nbsp; 
                    <input class="text uppercaseText" style="width:230px;" type="text" name="ColAddPostcode" id="jbColAddPostalCode" value="{$jb_datarow.ColAddPostalCode|escape:'html'}" />&nbsp;
                    <input type="button" name="quick_find_btn2" id="quick_find_btn2" style="position: relative; top:1px;" class="textSubmitButton postCodeLookUpBtn"  onclick="addressLookup2();return false;"   value="Click to find Address" >
                    &nbsp;&nbsp;
                    <span id="fetch" style="display: none" class="blueText" >
                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >
                    </span>
                </p>
                <p id="selectColAddOutput" ></p>
            {/if}    

           

           {if $jb_settings.ColAddCompanyNameChk1 eq 1}
           <p  id="jbColAddCompanyNameElement" {if $jb_settings.ColAddCompanyNameChk2 neq 1} class="not-compulsory" {/if} >
               <label for="jbColAddCompanyName" id="jbColAddCompanyNameLabel">
                   {$page['Labels']['customer_name']|escape:'html'}: {if $jb_settings.ColAddCompanyNameChk2 eq 1} <sup>*</sup> {/if}
               </label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddCompanyName" value="{$jb_datarow.ColAddCompanyName|escape:'html'}"  id="jbColAddCompanyName" >

           </p> 
           {/if}

           {if $jb_settings.ColAddBuildingNameNumberChk1 eq 1}
                <p id="jb_p_c_a_house_name" {if $jb_settings.ColAddBuildingNameNumberChk2 neq 1} class="not-compulsory" {/if}  >
                    <label for="jbColAddBuildingName" >{$page['Labels']['building_name']|escape:'html'}: {if $jb_settings.ColAddBuildingNameNumberChk2 eq 1} <sup>*</sup> {/if}</label>
                    &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="ColAddBuildingNameNumber" value="{$jb_datarow.ColAddBuildingName|escape:'html'}" id="jbColAddBuildingName" >          
                </p>
           {/if}

           {if $jb_settings.ColAddStreetChk1 eq 1} 
            <p id="jb_p_c_a_street"  {if $jb_settings.ColAddStreetChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbColAddStreet" >{$page['Labels']['street']|escape:'html'}:{if $jb_settings.ColAddStreetChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="ColAddStreet" value="{$jb_datarow.ColAddStreet|escape:'html'}" id="jbColAddStreet" >          
            </p>
           {/if}

           {if $jb_settings.ColAddLocalAreaChk1 eq 1}
                <p id="jb_p_c_a_area" {if $jb_settings.ColAddLocalAreaChk2 neq 1} class="not-compulsory" {/if} >
                    <label for="jbColAddArea"  >{$page['Labels']['area']|escape:'html'}:{if $jb_settings.ColAddLocalAreaChk2 eq 1} <sup>*</sup> {/if}</label>
                    &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddLocalArea" value="{$jb_datarow.ColAddArea|escape:'html'}" id="jbColAddArea" >          
                </p>
            {/if}    

           {if $jb_settings.ColAddTownCityChk1 eq 1} 
                <p id="jb_p_c_a_town"  {if $jb_settings.ColAddTownCityChk2 neq 1} class="not-compulsory" {/if} >
                    <label for="jbColAddCity" >{$page['Labels']['city']|escape:'html'}:{if $jb_settings.ColAddTownCityChk2 eq 1} <sup>*</sup> {/if}</label>
                    &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddCity" value="{$jb_datarow.ColAddCity|escape:'html'}" id="jbColAddCity" >          
                </p>
            {/if}

           {if $jb_settings.ColAddCountyIDChk1 eq 1} 
            <p id="jb_p_c_a_county" {if $jb_settings.ColAddCountyIDChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbColAddCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:{if $jb_settings.ColAddCountyIDChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp;  


                <select name="ColAddCountyID" id="jbColAddCounty" class="text" >
                    <option value="" id="cacountry_0_county_0" {if $jb_datarow.ColAddCounty eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $countries as $country}


                        {foreach $country.Counties as $county}
                        <option value="{$county.CountyID}"  id="cacountry_{$country.CountryID}_county_{$county.CountyID}" {if $jb_datarow.ColAddCounty eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                        {/foreach}


                    {/foreach}

                </select>

            </p>
           {/if}
           
             
           {if $jb_settings.ColAddCountryIDChk1 eq 1} 
            <p id="jb_p_c_a_country" {if $jb_settings.ColAddCountryIDChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbColAddCountry" >{$page['Labels']['country']|escape:'html'}: {if $jb_settings.ColAddCountryIDChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; 
                    <select name="ColAddCountryID" id="jbColAddCountry" >
                    <option value="" {if $jb_datarow.ColAddCountry eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $countries as $country}

                        <option value="{$country.CountryID}" {if $jb_datarow.ColAddCountry eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                    {/foreach}

                </select>
            </p> 
           {/if}
         
           {if $jb_settings.ColAddEmailChk1 eq 1}
            <p id="jb_p_c_a_email" {if $jb_settings.ColAddEmailChk2 neq 1} class="not-compulsory" {/if}  >
                <label for="jbColAddEmail" >{$page['Labels']['col_email']|escape:'html'}:{if $jb_settings.ColAddEmailChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input type="text" class="text" name="ColAddEmail" value="{$jb_datarow.ColAddEmail|escape:'html'}" id="jbColAddEmail" >          
            </p>
           {/if}
           
           
           {if $jb_settings.ColAddPhoneChk1 eq 1}
            <p id="jb_p_c_a_phone" {if $jb_settings.ColAddPhoneChk2 neq 1} class="not-compulsory" {/if} >
             <label for="jbColAddPhone" >{$page['Labels']['col_phone']|escape:'html'}:{if $jb_settings.ColAddPhoneChk2 eq 1} <sup>*</sup> {/if}</label>
             &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ColAddPhone" value="{$jb_datarow.ColAddPhone|escape:'html'}" id="jbColAddPhone" >
             <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ColAddPhoneExt" value="{$jb_datarow.ColAddPhoneExt|escape:'html'}" id="ColAddPhoneExt" >

            </p>
           {/if}
       
        
        
       
        <p>
           <hr style="height:1px" >
        </p>
        
        
        {if $jb_settings.StockCodeChk1 eq 1}
            
            {if $jb_datarow.ShowStockCode eq 1}  




                <p id="jbStockCodeElement" {if $jb_settings.StockCodeChk2 neq 1} class="not-compulsory" {/if} >
                    <label for="jb_item_catalogue_no" >{$page['Labels']['stock_code']|escape:'html'}: {if $jb_settings.StockCodeChk2 eq 1}  <sup id="jbStockCodeStar" >*</sup> {/if}</label> &nbsp;&nbsp; 

                    
                    <select name="StockCode" id="jbStockCode" class="text" >

                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $stockcode_rows as $stc}

                                    {if $stc.ProductNo eq $jb_datarow.StockCode}
                                    <option value="{$stc.ProductNo};;{$stc.ModelNumber};;{$stc.ModelDescription};;{$stc.UnitTypeID};;{$stc.ManufacturerID}" selected="selected" >{$stc.ProductNo|escape:'html'}</option>
                                    {else}
                                     <option value="{$stc.ProductNo};;{$stc.ModelNumber};;{$stc.ModelDescription};;{$stc.UnitTypeID};;{$stc.ManufacturerID}" >{$stc.ProductNo|escape:'html'}</option>
                                    {/if}

                                    {/foreach}
                     </select>
                     
                     
                     {if isset($prefillValues)}
                        <input type="hidden" value="{$prefillValues['jbStockCodePrefill']}" id="jbStockCodePrefill" />
                    {/if}

                </p>


            {/if}

            <input  type="hidden" name="HiddenStockCode" id="jbHiddenStockCode" value="{$jb_datarow.StockCode|escape:'html'}"  >

       {/if}
        
       
       {if $jb_settings.ModelNameChk1 eq 1}
           
         <p {if $jb_settings.ModelNameChk2 neq 1} class="not-compulsory" {/if} >
             <label>&nbsp;</label>
            &nbsp;&nbsp; 

            <input  type="radio" name="LocateBy"  value="1" {if $jb_datarow.LocateBy eq '1'} checked="checked" {/if}  /> <span class="text auto-hint" >{$page['Labels']['locate_by_model']|escape:'html'}</span> 
            <input  type="radio" name="LocateBy"  value="2" {if $jb_datarow.LocateBy eq '2'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['free_product_description']|escape:'html'}</span> 


         </p>




         <p id="jbModelNameRow" {if $jb_settings.ModelNameChk2 neq 1} class="not-compulsory" {/if} >
             {if isset($prefillValues)}
                 <input type="hidden" value="{$prefillValues['jbModelNamePrefill']}" id="jbModelNamePrefill" />
             {/if}
             <label for="jbModelName" >{$page['Labels']['model_no']|escape:'html'}:
                 {if $jb_settings.ModelNameChk2 eq 1} <sup id="jbModelNameStar" >*</sup> {/if}
                 </label>
             &nbsp;&nbsp; <input  type="text" name="ModelName" class="text auto-hint" id="jbModelName" value="{$jb_datarow.ModelName|escape:'html'}" title="{$page['Text']['model_number_title']|escape:'html'}" >
             <!--<a href="#" id="searchModelNameBtn" >{$page['Buttons']['search']|escape:'html'}</a>-->

         </p>
         
         <p id="jbProductDescriptionRow" {if $jb_settings.ModelNameChk2 neq 1} class="not-compulsory" {/if} >
            <label for="jbProductDescription" >{$page['Labels']['product_description']|escape:'html'}:
                
            {if $jb_settings.ModelNameChk2 eq 1} <sup id="jbProductDescriptionStar" >*</sup> {/if}
            
            </label>
            &nbsp;&nbsp; <input  type="text" name="ProductDescription" class="text auto-hint" id="jbProductDescription" value="{$jb_datarow.ProductDescription|escape:'html'}" title="{$page['Text']['text_description_title']|escape:'html'}" >
         
         </p>
         
        {/if}
        
        
        
       
        
        {if $jb_settings.ManufacturerIDChk1 eq 1}
            
            <p id="jbManufacturerIDElement" {if $jb_settings.ManufacturerIDChk2 neq 1} class="not-compulsory" {/if} >
                {if isset($prefillValues)}
                    <input type="hidden" value="{$prefillValues['jbManufacturerIDPrefill']}" id="jbManufacturerIDPrefill" />
                {/if}
                <label for="jbManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:
                    
                    {if $jb_settings.ManufacturerIDChk2 eq 1} <sup id="jbManufacturerIDStar" >*</sup> {/if}
                    
                
                </label>
                &nbsp;&nbsp;  <select name="ManufacturerID" id="jbManufacturerID" class="text" >

                                <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $mfg_rows as $mfg}

                                {if $mfg.ManufacturerID eq $jb_datarow.ManufacturerID}
                                <option value="{$mfg.ManufacturerID}" selected="selected">{$mfg.ManufacturerName|escape:'html'}</option>
                                {else}
                                 <option value="{$mfg.ManufacturerID}" >{$mfg.ManufacturerName|escape:'html'}</option>
                                 {/if}

                                {/foreach}
                 </select>
            </p>
            
        {/if}
        
        
        
        {if $jb_settings.UnitTypeIDChk1 eq 1}
            
            <p id="jbUnitTypeIDElement" {if $jb_settings.UnitTypeIDChk2 neq 1} class="not-compulsory" {/if} >
               <label for="jbUnitTypeID" >{$page['Labels']['product_type']|escape:'html'}:
                   
                   {if $jb_settings.UnitTypeIDChk2 eq 1} <sup>*</sup> {/if}
               
               </label>
               &nbsp;&nbsp;           
               {if isset($prefillValues)}
                   <input type="hidden" value="{$prefillValues['jbUnitTypeIDPrefill']}" id="jbUnitTypeIDPrefill" />
               {/if}
               <select name="UnitTypeID" id="jbUnitTypeID" class="text" >

                               <option value="" selected="selected" ></option>
                               <option value="-1" >{$page['Text']['not_listed']|escape:'html'}</option>

                               {foreach $product_type_rows as $ptype}

                               {if $ptype.UnitTypeID eq $jb_datarow.UnitTypeID}
                               <option value="{$ptype.UnitTypeID}" selected="selected">{$ptype.UnitTypeName|escape:'html'}</option>
                               {else}
                                <option value="{$ptype.UnitTypeID}" >{$ptype.UnitTypeName|escape:'html'}</option>
                                {/if}

                               {/foreach}
                </select>


                <img id="RequestNewUnitType" style="display:none;" alt="{$page['Text']['request_unittype']|escape:'html'}" title="{$page['Text']['request_unittype']|escape:'html'}" src="{$_subdomain}/css/Skins/{$_theme}/images/plus-icon.png" width="24" height="24" style="position:relative;top:6px;cursor:pointer;" />

           </p>
           
        {/if}
        
        {if $jb_settings.SerialNoChk1 eq 1}
            <p id="jbSerialNoElement" {if $jb_settings.SerialNoChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbSerialNo">{$page['Labels']['serial_no']|escape:'html'}:  {if $jb_settings.SerialNoChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp;
                <input type="text" name="SerialNo" class="text uppercaseText" id="jbSerialNo" value="{$jb_datarow.SerialNo|escape:'html'}" />
            </p>
        {/if}
	
        {if $jb_settings.DateOfPurchaseChk1 eq 1}
            
            <p id="jbDateOfPurchaseElement" {if $jb_settings.DateOfPurchaseChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jb_purchase_date" >{$page['Labels']['purchase_date']|escape:'html'}:  {if $jb_settings.DateOfPurchaseChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp;

                {html_select_date class='selectDateClass'   prefix='DateOfPurchase' field_order='DMY' day_value_format="%02d"   time="{$jb_datarow.DateOfPurchase|escape:'html'|default:"00-00-0000"}" all_id="DateOfPurchase" start_year='-5'   year_empty="{$page['Text']['year']|escape:'html'}" month_empty="{$page['Text']['month']|escape:'html'}" day_empty="{$page['Text']['day']|escape:'html'}"}  

                <input type="text" class="text auto-hint" style="width:80px;" name="DateOfPurchase"  title="dd/mm/yyyy" value="{$jb_datarow.DateOfPurchase|escape:'html'|date_format:'%d/%m/%Y'}" id="DateOfPurchase"  >
            </p>
            
        {/if}
        
        {if $jb_datarow.JobTypeID}
            <input  type="hidden" name="JobTypeID" id="JTRB_{$jb_datarow.JobTypeID}" value="{$jb_datarow.JobTypeID|escape:'html'}" />
        {/if}    
       
        
        {if $jb_settings.ServiceTypeIDChk1 eq 1}
            
            <p id="jbServiceTypeIDElement" {if $jb_settings.ServiceTypeIDChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbServiceTypeID" >{$page['Labels']['service_type']|escape:'html'}: {if $jb_settings.ServiceTypeIDChk2 eq 1} <sup>*</sup> {/if}</label>
               &nbsp;&nbsp; <select name="ServiceTypeID" id="jbServiceTypeID" class="text" >


                                <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $service_type_rows as $stype}

                                {if $stype.ServiceTypeID eq $jb_datarow.ServiceTypeID}
                                <option value="{$stype.ServiceTypeID}" selected="selected">{$stype.Name|escape:'html'}</option>
                                {else}
                                 <option value="{$stype.ServiceTypeID}" >{$stype.Name|escape:'html'}</option>
                                 {/if}

                                {/foreach}


                    </select>
            </p>
            
        {/if} 
            
         
        {if $jb_settings.ReportedFaultChk1 eq 1}
            
            <p {if $jb_settings.ReportedFaultChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbReportedFault" id="jbReportedFaultLabel" >
                    {$page['Labels']['reported_fault']|escape:'html'}: {if $jb_settings.ReportedFaultChk2 eq 1} <sup>*</sup> {/if}
                </label>
                &nbsp;&nbsp; 
                <textarea cols="20" rows="3" maxlength="2000" class="text" name="ReportedFault" id="jbReportedFault">{$jb_datarow.ReportedFault|escape:'html'}</textarea>
            </p>
            
         {/if}   

        
        
        
        {if $jb_settings.AccessoriesChk1 eq 1}
            <p id="jbAccessoriesElement" {if $jb_settings.AccessoriesChk2 neq 1} class="not-compulsory" {/if} style="margin-bottom:1px;" >
                <label for="jbAccessories" >{$page['Labels']['accessories']|escape:'html'}:  {if $jb_settings.AccessoriesChk2 eq 1} <sup>*</sup> {/if}</label>
                 &nbsp;&nbsp; 


                 {$page['Text']['none_text']|escape:'html'}

                 <input  type="radio" name="AccessoriesRadio" id="jbAccessoriesRadio" value="-1" >
                 <span id="AccessoriesSpan1" ><input  type="text" maxlength="200" style="width:335px;" class="text" name="Accessories" id="jbAccessories" value="{$jb_datarow.Accessories|escape:'html'}" ></span> 

                 <input type="hidden" id="HiddenAccessoriesFlag" name="HiddenAccessoriesFlag" value="" >
            </p>
        {/if}
        
        {if $jb_settings.UnitConditionChk1 eq 1}
            <p id="jbUnitConditionElement" {if $jb_settings.UnitConditionChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbUnitCondition" >
                       {$page['Labels']['unit_condition']|escape:'html'}:  {if $jb_settings.UnitConditionChk2 eq 1} <sup>*</sup> {/if}
                </label>
                 &nbsp;&nbsp; 
                  <textarea cols="20" rows="2" maxlength="150" class="text" name="UnitCondition" id="jbUnitCondition" value="{$jb_datarow.UnitCondition|escape:'html'}"> </textarea>

            </p>
        {/if}       
           
        
         
        <p class="not-compulsory" >
              <hr style="height:1px" class="not-compulsory" >
        </p>
         
        {if $jb_settings.MobilePhoneNetworkIDChk1 eq 1}
                <p id="jbMobilePhoneNetworkIDElement" {if $jb_settings.MobilePhoneNetworkIDChk2 neq 1} class="not-compulsory" {/if} >
                    <label for="jbMobilePhoneNetworkID" >{$page['Labels']['mobile_phone_network']|escape:'html'}:  {if $jb_settings.MobilePhoneNetworkIDChk2 eq 1} <sup>*</sup> {/if} </label>
                   &nbsp;&nbsp; <select name="MobilePhoneNetworkID" id="jbMobilePhoneNetworkID" class="text" >


                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $mobile_phone_networks as $mpn}

                                    {if $mpn.MobilePhoneNetworkID eq $jb_datarow.MobilePhoneNetworkID}
                                    <option value="{$mpn.MobilePhoneNetworkID}" selected="selected">{$mpn.MobilePhoneNetworkName|escape:'html'}</option>
                                    {else}
                                     <option value="{$mpn.MobilePhoneNetworkID}" >{$mpn.MobilePhoneNetworkName|escape:'html'}</option>
                                     {/if}

                                    {/foreach}


                        </select>
                </p> 
        {/if}
        
        
         
        {if $jb_settings.OriginalRetailerChk1 eq 1}
            
            {if $jb_datarow.OriginalRetailerFormElement neq 'Dropdown'}

             <p id="jbOriginalRetailerElementTop" {if $jb_settings.OriginalRetailerChk2 neq 1} class="not-compulsory" {/if} >
                <label >&nbsp;</label>
                &nbsp;&nbsp; 
                <label class="oneThirdTextLabel" >Retailer Name</label>
                <label class="oneThirdTextLabel" >Branch  Name</label>
                <label class="oneThirdTextLabel" >Town</label>



             </p>

            {/if}
        
        
        
            <p id="jbOriginalRetailerElement" {if $jb_settings.OriginalRetailerChk2 neq 1} class="not-compulsory" {/if} >


                {if $jb_datarow.OriginalRetailerFormElement eq 'Dropdown'}
                   <label for="OriginalRetailerFullPart" >{$page['Labels']['original_retailer']|escape:'html'}:  {if $jb_settings.OriginalRetailerChk2 eq 1} <sup>*</sup> {/if} </label>
                   &nbsp;&nbsp; 
                   <input  type="text" class="text capitalizeText"  title=""  name="OriginalRetailerFullPart" id="OriginalRetailerFullPart" value="{$jb_datarow.OriginalRetailerPart1|escape:'html'}" maxlength="100" >
                   <a href="#" id="deleteOriginalRetailer"><img src="{$_subdomain}/images/cross-icon.png" /></a>

                {else}
                    <label for="OriginalRetailerPart1" >{$page['Labels']['original_retailer']|escape:'html'}:  {if $jb_settings.OriginalRetailerChk2 eq 1} <sup>*</sup> {/if} </label>
                    &nbsp;&nbsp; 
                    <input  type="text" class="text capitalizeText" style="width:116px" title=""  name="OriginalRetailerPart1" id="OriginalRetailerPart1" value="{$jb_datarow.OriginalRetailerPart1|escape:'html'}" maxlength="25" >
                    &nbsp;<input  type="text" class="text capitalizeText" style="width:116px" title=""  name="OriginalRetailerPart2" id="OriginalRetailerPart2" value="{$jb_datarow.OriginalRetailerPart2|escape:'html'}" maxlength="25" >
                    &nbsp;<input  type="text" class="text capitalizeText" style="width:118px" title=""  name="OriginalRetailerPart3" id="OriginalRetailerPart3" value="{$jb_datarow.OriginalRetailerPart3|escape:'html'}" maxlength="25" >
                    <a href="#" id="clearORFields"><img src="{$_subdomain}/images/cross-icon.png" /></a>

                {/if}
            </p>
            
        {/if}
         
        {if $jb_settings.ReceiptNoChk1 eq 1}
            
            <p {if $jb_settings.ReceiptNoChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbReceiptNo" id="jbReceiptNoLabel" >{$page['Labels']['sales_receipt_no']|escape:'html'}:  {if $jb_settings.ReceiptNoChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="ReceiptNo" id="jbReceiptNo" value="{$jb_datarow.ReceiptNo|escape:'html'}" >
            </p>
            
        {/if}
        
        {if $jb_settings.InsurerChk1 eq 1}
            <p id="jbInsurerElement" {if $jb_settings.InsurerChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbInsurer" >{$page['Labels']['insurer']|escape:'html'}:{if $jb_settings.InsurerChk2 eq 1} <sup>*</sup> {/if}</label>
                 &nbsp;&nbsp; <input  type="text" class="text" name="Insurer" id="jbInsurer" value="{$jb_datarow.Insurer|escape:'html'}" >

            </p>
        {/if}
        
        {if $jb_settings.PolicyNoChk1 eq 1}
         <p id="jbPolicyNoElement" {if $jb_settings.PolicyNoChk2 neq 1} class="not-compulsory" {/if} >
           <label for="jbPolicyNo" >{$page['Labels']['policy_no']|escape:'html'}:{if $jb_settings.PolicyNoChk2 eq 1} <sup>*</sup> {/if}</label>
            &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="PolicyNo" id="jbPolicyNo" value="{$jb_datarow.PolicyNo|escape:'html'}" >
         </p>
        {/if}
        
        {if $jb_settings.AuthorisationNoChk1 eq 1}
            <p id="jbAuthorisationNoElement" {if $jb_settings.AuthorisationNoChk2 neq 1} class="not-compulsory" {/if} >
               <label for="jbAuthorisationNo" >{$page['Labels']['authorisation_no']|escape:'html'}: {if $jb_settings.AuthorisationNoChk2 eq 1} <sup>*</sup> {/if}</label>
               &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="AuthorisationNo"  value="{$jb_datarow.AuthorisationNo|escape:'html'}" id="jbAuthorisationNo" >

           </p>
        {/if}
        
        {if $jb_settings.NotesChk1 eq 1}
            <p id="jbNotesElement" {if $jb_settings.NotesChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbNotes" >{$page['Labels']['additional_notes']|escape:'html'}: {if $jb_settings.NotesChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <textarea  cols="20" rows="3" class="text" name="Notes" id="jbNotes" >{$jb_datarow.Notes|escape:'html'}</textarea>&nbsp;&nbsp;

            </p>
        {/if}
        
        {if $jb_settings.ReferralNoChk1 eq 1}
            <p  id="jbReferralNoElement" {if $jb_settings.ReferralNoChk2 neq 1} class="not-compulsory" {/if} >
                <label for="jbReferralNo" >{$page['Labels']['referral_no']|escape:'html'}: {if $jb_settings.ReferralNoChk2 eq 1} <sup>*</sup> {/if}</label>
                &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="ReferralNo" id="jbReferralNo" value="{$jb_datarow.ReferralNo|escape:'html'}" >

            </p>
        {/if}
        
        <p style="padding-top:6px;" >
          
            
            
            <input type="hidden" name="jb_data_protection_act" id="jb_data_protection_act"  value="{$jb_datarow.data_protection_act|escape:'html'}" >
            
            <input type="hidden" name="BrandCountry" id="jbBrandCountry"  value="{$jb_datarow.BrandCountry|escape:'html'}" >
            <input type="hidden" name="CountryMandatory" id="jbCountryMandatory"  value="{$jb_datarow.CountryMandatory|escape:'html'}" >
           
            <input type="hidden" name="NetworkID" id="jbNetworkID"  value="{$jb_datarow.NetworkID|escape:'html'}" >
            <input type="hidden" name="ClientID" id="jbClientID"  value="{$jb_datarow.ClientID|escape:'html'}" >
            
            
            <input type="hidden" name="JobTypeCode" id="jbJobTypeCode"  value="{$jb_datarow.JobTypeCode|escape:'html'}" >
            
            <input type="hidden" name="JobSourceID" id="jbJobSourceID"  value="{$jb_datarow.JobSourceID|escape:'html'}" >
            
            
            <input type="hidden" name="RequestNewUnitTypeText" id="RequestNewUnitTypeText"  value="" >
            <input type="hidden" name="SendServiceCompletionText" id="SendServiceCompletionText"  value="No" >
            
            {if $jb_datarow.SendRepairCompleteTextMessage eq '1'}
            
            <input type="hidden" name="SMSPopupPage" id="SMSPopupPage"  value="{$jb_datarow.SendRepairCompleteTextMessage|escape:'html'}" >
            
            {/if}
            
            
            
            
          <label>&nbsp;</label>&nbsp;&nbsp;&nbsp;     
          <input type="submit" name="confirm_booking_btn" 
                 id="confirm_booking_btn" class="btnStandard"  
                 value="{$page['Buttons']['confirm_booking']|escape:'html'}" >
            
            {*
            <input type="button" name="find_service_centre_btn" id="find_service_centre_btn" class="textSubmitButton"  value="{$page['Buttons']['find_service_centre']|escape:'html'}" >
            *}
            {if $AP10000}
            {* Note: Removed because Service Centre has not yet been allocated to job before
                     it has been booked.
            <a id="BookCollectionButton" style="float: right;" href="#">
                <span style="text-align: center;display: inline-block;">
                    <img src="{$_subdomain}/css/Skins/{$_theme}/images/icons_skyline_truck_big.png" 
                         alt="{$page['Text']['click_courier_appointment']|escape:'html'}" 
                         title="{$page['Text']['click_courier_appointment']|escape:'html'}" 
                         width="68" height="53"  />
                    <br />
                    {$page['Text']['click_courier_appointment']|escape:'html'}
                </span>
            </a>*}
            {/if}
            
        </p>

         </fieldset> 

        
        </form>
    </div>   
        
                      
    
                                    
</div>
{/block}
