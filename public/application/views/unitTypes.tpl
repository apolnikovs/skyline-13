{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $UnitTypes}
{/block}

{block name=afterJqueryUI}
    
  <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
  <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
  <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
    </style>
{/block}    
    
{block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

    
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>


<script type="text/javascript">
      
    var checked = false;
  
    var $statuses = [
			{foreach from=$statuses item=st}
			   ["{$st.Name}", "{$st.Code}"],
			{/foreach}
                    ]; 
     
    var table;             
                    
        
    function gotoEditPage($sRow) {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
    }


    /**
    *  This is used to change color of the row if its in-active.
    */
   
    function inactiveRow(nRow, aData) {
          
	if(aData[3] == $statuses[1][1] || aData[4] == $statuses[1][1]) {  
            $(nRow).addClass("inactive");
            //$('td:eq(2)', nRow).html($statuses[1][0]);
        } else if(aData[3] == $statuses[0][1] || aData[4] == $statuses[0][1]) {
            $(nRow).addClass("");
            //$('td:eq(2)', nRow).html($statuses[0][0]);
        }
        
	//Getting assigned value for each Unit Type for selected Client.
	/*
        if(aData[4])
	{
            
           $.post("{$_subdomain}/ProductSetup/unitTypes/getAssigned/"+urlencode(aData[4])+"/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

            '',      
            function(data){
                    var p = eval("(" + data + ")");
                    
                    if(p)
                    {   
                        $checked = ' checked="checked" ';
                    }
                    else
                    {
                        $checked = '  ';
                    }
                    
                    
                    $checkbox = '<input type="checkbox" class="checkBoxDataTable" name="assignedUnitTypes[]" '+$checked+' value="'+aData[4]+'" > ';
                    
                    $('td:eq(3)', nRow).html( $checkbox );

            });
	}
        */
    }
    
    
    var currentVal;
    
   
    $(document).on("click", "#UnitTypesResults tr > td:first-child+td", function() {
	if(!$(this).parent().children(":last").find("input").is(":checked")) {
	    return false;
	}
	$(this).css("padding","0");
	var html = "<input type='text' id='turnaround' style='width:80%; height:13px; margin-left:4px; margin-top:3px;' value='" + $(this).text() + "' />";
	$(this).html(html);
	currentVal = $("#turnaround").val();
	$("#turnaround").focus();
	return false;
    });


    $(document).on("click", "#turnaround", function() {
	return false;
    });


    $(document).on("blur", "#turnaround", function() {
	var the = this;
	if(/*$(this).val() != "" && */$(this).val() != currentVal) {
	    var data = table.dataTable().fnGetData($(this).parent().parent().get(0));
	    $.post( "{$_subdomain}/ProductSetup/setClientProductTurnaround", 
		    {	days:	    $("#turnaround").val(),
			clientID:   $("#cId").val(),
			unitID:	    data[0]
		    }, 
		    function(response) {
			//table.dataTable().fnReloadAjax(null,null,null,true);
			var parent = $(the).parent();
			$(parent).text($(the).val());
			$(parent).removeAttr("style");
			$(the).remove();
		    }
	    );
	} else {
	    var parent = $(this).parent();
	    $(parent).text($(this).val());
	    $(parent).removeAttr("style");
	    $(this).remove();
	}
	return false;
    });


    $(document).on("keypress", "#turnaround", function(e) {
	var the = this;
	if(e.which == 13) {
	    if(/*$(this).val() != "" && */$(this).val() != currentVal) {
		var data = table.dataTable().fnGetData($(this).parent().parent().get(0));
		$.post( "{$_subdomain}/ProductSetup/setClientProductTurnaround", 
			{   days:	$("#turnaround").val(),
			    clientID:   $("#cId").val(),
			    unitID:	data[0]
			}, 
			function(response) {
			    //table.dataTable().fnReloadAjax(null,null,null,true);
			    var parent = $(the).parent();
			    $(parent).text($(the).val());
			    $(parent).removeAttr("style");
			    $(the).remove();
			}
		);
	    } else {
		var parent = $(this).parent();
		$(parent).text($(this).val());
		$(parent).removeAttr("style");
		$(this).remove();
	    }
	    return false;
	}
    });
   
   
    /*
    function test() {
	alert("aliard");
    }
    */


    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypes/'+urlencode($("#nId").val()));
            }
        });
        $("#cId").combobox({
            change: function() {
                $showAssigned = '';
                if($("#showAssigned").prop("checked")) {
                    $showAssigned = 1;    
                }
                $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypes/' + urlencode($("#nId").val()) + "/" + urlencode($("#cId").val()) + '/' + $showAssigned);
            }
        });
	//Click handler for finish button.
	$(document).on('click', '#finish_btn', function() {
	    $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');
	});

        //Click handler for save changes button.
        $(document).on('click', '#save_changes_btn', function() {
	
	    sltUnitTypes = '';
                                    
	    $('.checkBoxDataTable').each(function() {
		sltUnitTypes += $(this).val()+',';
	    });
                                       
	    $("#sltUnitTypes").val(sltUnitTypes);  
                                  
	    $.post("{$_subdomain}/ProductSetup/unitTypes/saveChanges/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        
		    $("#UnitTypesResultsForm").serialize(),      
		    function(data){
			var p = eval("(" + data + ")");

			if(p == "OK") {
			    $("#centerInfoText").html("{$page['Text']['save_changes_done']|escape:'html'}").fadeIn('slow').delay("2000").fadeOut('slow');  
			} else {
			    $("#centerInfoText").html("{$page['Text']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow').delay("2000").fadeOut('slow');  
			}
                                        
		    });

	});                    

	//Add a change handler to the network dropdown - strats here
	/*$(document).on('change', '#nId', function() {
	    $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypes/'+urlencode($("#nId").val())); 
	});*/
	//Add a change handler to the network dropdown - ends here
                      
	//Add a change handler to the client dropdown - starts here
	/*$(document).on('change', '#cId', function() {
	    $showAssigned = '';
	    if($("#showAssigned").prop("checked")) {
		$showAssigned = 1;    
	    }
	    $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypes/' + urlencode($("#nId").val()) + "/" + urlencode($("#cId").val()) + '/' + $showAssigned); 
	});*/
	//Add a change handler to the client dropdown - ends here
                      
	//Add a click handler to the show assigned only check box - strats here
	$(document).on('click', '#showAssigned', function() {
	    $showAssigned = '';
	    if($("#showAssigned").prop("checked")) {
		$showAssigned = 1;    
	    }
	    $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypes/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+$showAssigned); 
	});
	//Add a click handler to the show assigned only check box - ends here
                     
	/* =======================================================
	*
	* set tab on return for input elements with form submit on auto-submit class...
	*
	* ======================================================= */

        $('input[type=text],input[type=password]').keypress(function(e) {
	    if(e.which == 13) {
		$(this).blur();
		if($(this).hasClass('auto-submit')) {
		    $('.auto-hint').each(function() {
			$this = $(this);
			if($this.val() == $this.attr('title')) {
			    $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
			    if($this.hasClass('auto-pwd')) {
				$this.prop('type','password');
			    }
			}
		    });
		    $(this).get(0).form.onsubmit();
		} else {
		    $next = $(this).attr('tabIndex') + 1;
		    $('[tabIndex="'+$next+'"]').focus();
		}
		return false;
	    }
	});        
        

	if($("#cId").val()) {
			
	    $("#showAssignedPanel").fadeIn();
	    $columnsList = [ 
		/* UnitTypeID */    { bVisible: false },    
		/* UnitTypeName */  null,
				    {if isset($turnaround)}
					null,
				    {/if}
		/* RepairSkillID */ null,
		/* Status */	    null,
		/* Assigned */	    { bSortable: false, bSearchable: false, sDefaultContent: "" }
	    ];
                            
	    $('#save_changes_btn').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                         
	} else {
			
	    $("#showAssignedPanel").fadeOut();
	    $columnsList = [ 
		/* UnitTypeID */    { bVisible: false },    
		/* UnitTypeName */  null,
		/* RepairSkillID */ null,
		/* Status */	    null
	    ];
	    $('#save_changes_btn').attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');                    
	}
     
     
	{if $SupderAdmin eq true}
	    $displayButtons = "UA";
	    $dblclickCallbackMethod = "gotoEditPage";
	    $tooltipTitle = "{$page['Text']['tooltip_title']|escape:'html'}";
	{else}
	    $displayButtons = "";
	    $dblclickCallbackMethod = "";
	    $tooltipTitle = "";
	{/if}
         
	 
	table = $('#UnitTypesResults').PCCSDataTable({

	    aoColumns:			$columnsList,
	    aaSorting:			[[ 1, "asc" ]],   
	    displayButtons:		$displayButtons,
	    addButtonId:		'addButtonId',
	    addButtonText:		'{$page['Buttons']['insert']|escape:'html'}',
	    createFormTitle:		'{$page['Text']['insert_page_legend']|escape:'html'}',
	    createAppUrl:		'{$_subdomain}/ProductSetup/unitTypes/insert/'+urlencode("{$nId}")+'/',
	    createDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypes/',
	    createFormFocusElementId:   'UnitTypeName',
	    formInsertButton:		'insert_save_btn',

	    frmErrorRules: {
		UnitTypeName: { required: true },
                ImeiRequired: { required: true },
                DOPWarrantyPeriod: { digits: true },
                ProductionDateWarrantyPeriod: { digits: true }

	    },

	    frmErrorMessages: {
		
                UnitTypeName: { required: "{$page['Errors']['name']|escape:'html'}" },
                ImeiRequired: { required: "{$page['Errors']['ImeiRequired']|escape:'html'}" },
                DOPWarrantyPeriod: { digits: "{$page['Errors']['digits_error']|escape:'html'}" },
                ProductionDateWarrantyPeriod: { digits: "{$page['Errors']['digits_error']|escape:'html'}" }
                    
	    },                     

	    popUpFormWidth:		850,
	    popUpFormHeight:		430,
	    updateButtonId:		'updateButtonId',
	    updateButtonText:		'{$page['Buttons']['edit']|escape:'html'}',
	    updateFormTitle:		'{$page['Text']['update_page_legend']|escape:'html'}',
	    updateAppUrl:		'{$_subdomain}/ProductSetup/unitTypes/update/'+urlencode("{$nId}")+'/',
	    updateDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypes/',
	    formUpdateButton:		'update_save_btn',
	    updateFormFocusElementId:   'UnitTypeName',
	    colorboxFormId:		"UnitTypesForm",
	    frmErrorMsgClass:		"fieldError",
	    frmErrorElement:		"label",
	    htmlTablePageId:		'UnitTypesResultsPanel',
	    htmlTableId:		'UnitTypesResults',
	    fetchDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypes/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}")+"/"+urlencode("{$showAssigned}"),
	    formCancelButton:		'cancel_btn',
	    fnRowCallback:		'inactiveRow',
	    searchCloseImage:		'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    dblclickCallbackMethod:	$dblclickCallbackMethod,
	    tooltipTitle:		$tooltipTitle,
	    iDisplayLength:		25,
	    formDataErrorMsgId:		"suggestText",
	    frmErrorSugMsgClass:	"formCommonError",
	    sDom:			'ft<"#dataTables_command">rpli',
	    bottomButtonsDivId:		'dataTables_command'

	});
                      
		      
	$(document).on("click", "#checkAll", function() {

	    if(!checked) {
		$(".checkBoxDataTable").each(function() {
		    $(this).attr("checked","checked");
		});
		checked = true;
		$("#checkAll label").text("Uncheck All");
	    } else {
		$(".checkBoxDataTable").each(function() {
		    $(this).removeAttr("checked");
		});
		checked = false;
		$("#checkAll label").text("Check All");
	    }
	    
	    return false;

	});
                   

    });

    </script>

    
{/block}


{block name=body}

    
<div class="breadcrumb">
    <div>
	<a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
    </div>
</div>

<div class="main" id="home">

    <div class="ServiceAdminTopPanel">
	<form id="UnitTypesTopForm" name="UnitTypesTopForm" method="post" action="#" class="inline">
	    <fieldset>
		<legend title="">{$page['Text']['legend']|escape:'html'}</legend>
		<p>
		    <label>{$page['Text']['description']|escape:'html'}</label>
		</p> 
	    </fieldset> 
	</form>
    </div>  


    <div class="ServiceAdminResultsPanel" id="UnitTypesResultsPanel">
                    
	{if $SupderAdmin == true} 
	    
	    <form name="listsForm" id="unitTypeListsForm">
                {$page['Labels']['service_network_label']|escape:'html'}
		<select name="nId" id="nId">
		    <option value="" {if $nId == ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>
		    {/foreach}
		</select>
		&nbsp;&nbsp;{$page['Labels']['client_label']|escape:'html'}
		<select name="cId" id="cId">
		    <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>
		    {foreach $clients as $client}
			<option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>
		    {/foreach}
		</select>
		<span id="showAssignedPanel" style="display:none;">
		    <input class="assignedTextBox" id="showAssigned" type="checkbox" name="showAssigned" value="1" {if $showAssigned neq ''}checked="checked"{/if} />
		    <span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
		</span>
	    </form>
		
	{else if $NetworkUser eq true}
	    
	    <form name="listsForm">
		<input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" />
                {$page['Labels']['client_label']|escape:'html'}
		<select name="cId" id="cId" >
		    <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>
		    {foreach $clients as $client}
			<option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>
		    {/foreach}
		</select>
		<span id="showAssignedPanel" style="display:none;">
		    <input class="assignedTextBox" id="showAssigned" type="checkbox" name="showAssigned" value="1" {if $showAssigned != ''}checked="checked"{/if} />
		    <span class="text">{$page['Text']['show_assigned']|escape:'html'}</span> 
		</span>
	    </form>
                        
	{else if $ClientUser eq true}
	    
	    <form name="listsForm">
		<input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" /> 
		<input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" />  
		<span id="showAssignedPanel" style="display:none;">
		    <input class="assignedTextBox" id="showAssigned" type="checkbox" name="showAssigned" value="1" {if $showAssigned != ''}checked="checked"{/if} />
		    <span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
		</span>
	    </form>

	{/if}

	<br/><br/>
	
	<form id="UnitTypesResultsForm" class="dataTableCorrections">
	    <table id="UnitTypesResults" border="0" cellpadding="0" cellspacing="0" class="browse">
		<thead>
		    <tr>
			<th></th> 
			<th width="60%" title="{$page['Text']['unit_type']|escape:'html'}">
			    {$page['Text']['unit_type']|escape:'html'}
			</th>
			{if isset($turnaround)}
			    <th>Turnaround</th>
			{/if}
			<th title="{$page['Text']['repair_skill']|escape:'html'}">{$page['Text']['repair_skill']|escape:'html'}</th>
			<th width="10%" title="{$page['Text']['status']|escape:'html'}">{$page['Text']['status']|escape:'html'}</th>
			{if $cId != ''}
			    <th title="{$page['Text']['assigned']|escape:'html'}">{$page['Text']['assigned']|escape:'html'}</th>
			{/if}
		    </tr>
		</thead>
		<tbody>
		</tbody>
	    </table>  
	    <input type="hidden" name="sltUnitTypes" id="sltUnitTypes" />
	</form>
			
    </div>        

    <div class="bottomButtonsPanel">
	<hr/>
	<button id="save_changes_btn" type="button" class="gplus-blue-disabled leftBtn">
	    <span class="label">{$page['Buttons']['save_changes']|escape:'html'}</span>
	</button>
	<button id="finish_btn" type="button" class="gplus-blue rightBtn">
	    <span class="label">{$page['Buttons']['finish']|escape:'html'}</span>
	</button>
	<button id="checkAll" type="button" class="gplus-blue rightBtn"><span class="label">Check All</span></button>
    </div>
	
    <div class="centerInfoText" id="centerInfoText" style="display:none;"></div>
    
    {if $SupderAdmin == false}
	<input type="hidden" name="addButtonId" id="addButtonId" /> 
	<input type="hidden" name="updateButtonId" id="updateButtonId" /> 
    {/if} 

</div>
                        
                        



{/block}



