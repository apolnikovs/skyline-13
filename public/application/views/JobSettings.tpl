{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page_title|escape:'html'}
    {$PageId = $JobBookingSettingsPage}
{/block}


{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
   <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
   
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
   
   <script>
    $(function() {
        
        $('#jbDateOfPurchaseElement .ui-combobox-input').css('width', '80px');
        
        $('.fieldLabel').css('width', '275px');
        
    });
    </script>

    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
{/block}


{block name=scripts}
  
 
    <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

    <script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>

    <script type="text/javascript">


           $(document).ready(function() {
		   
           $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })

           
           
                function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
                {
                       $($countyId+" option:selected").each(function () {


                            $selected_cc_id =  $($countyId+" option:selected").attr('id');

                            $country_id_array = $selected_cc_id.split('_');


                            if($country_id_array[1]!='0')
                            {

                                $($countryId).val($country_id_array[1]);

                                setValue($countryId);


                            }
                            else
                            {
                                 $($parentCountryId+" input:first").val('');
                            }

                    });
                }

                $("#jbCounty").combobox({
                    change: function() { 
                        setCountryBasedOnCounty("#jbCounty", "#jbCountry", "#jb_p_country") 
                    } 
                });

                $("#jbCountry").combobox();
                
                
                
               
                
                
                

                /* =======================================================
                *
                * Initialise input auto-hint functions...
                *
                * ======================================================= */

                $('.auto-hint').focus(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                        else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).blur(function() {
                        $this = $(this);
                        if ($this.val() == '' && $this.attr('title') != '')  {
                            $this.val($this.attr('title')).addClass('auto-hint');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','text');
                            }
                        } else if ($this.val()!='' && $this.val() != $this.attr('title')) {
                             $this.removeClass('auto-hint'); 
                        }
                        
                        
                    } ).each(function(){
                        $this = $(this);
                        
                        if ($this.attr('title') == '') { return; }
                        if ($this.val() == '') { 
                            if ($this.attr('type') == 'password') {
                                $this.addClass('auto-pwd').prop('type','text');
                            }
                            $this.val($this.attr('title')); 
                        } else {
                        
                                $this.removeClass('auto-hint'); 
                        }
                        $this.attr('autocomplete','off');
                    } );
       
       
       
               /* =======================================================
                *
                * set tab on return for input elements with form submit on auto-submit class...
                *
                * ======================================================= */

                $('input[type=text],input[type=password]').keypress( function( e ) {
                        if (e.which == 13) {
                            $(this).blur();
                            if ($(this).hasClass('auto-submit')) {
                                $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                                } );
                                $(this).get(0).form.onsubmit();
                            } else {
                                $next = $(this).attr('tabIndex') + 1;
                                $('[tabIndex="'+$next+'"]').focus();
                            }
                            return false;
                        }
                    } );  
                
  

                   $("#jbContactTitle").combobox();  
                   $( "#jbCustomerName input:first" ).css("width", "60px");
                   $( "#jbCustomerName select:first" ).css("width", "60px");
                   
                   
                   $("#DateOfPurchaseDateOfPurchaseDay").combobox({ change: function() {  
                                
                                                       purchaseDateSet(); 

                                                    } });
                   $("#DateOfPurchaseDateOfPurchaseMonth").combobox({ change: function() {  

                                                             purchaseDateSet(); 

                                                          } });
                   $("#DateOfPurchaseDateOfPurchaseYear").combobox({ change: function() {  

                                                             purchaseDateSet(); 

                                                          } });
                                                          
                                                          
                    {if $datarow.DateOfPurchase eq ''}

                $("#DateOfPurchaseDateOfPurchaseDay").val('');
                setValue("#DateOfPurchaseDateOfPurchaseDay", ":first");

                $("#DateOfPurchaseDateOfPurchaseMonth").val('');
                setValue("#DateOfPurchaseDateOfPurchaseMonth", ":eq(1)");

                $("#DateOfPurchaseDateOfPurchaseYear").val('');
                setValue("#DateOfPurchaseDateOfPurchaseYear", ":eq(2)");
                
              {/if}
                  
                  
                function purchaseDateSet()
                {

                   if($('#DateOfPurchaseDateOfPurchaseDay').val() && $('#DateOfPurchaseDateOfPurchaseMonth').val() && $('#DateOfPurchaseDateOfPurchaseYear').val())
                   {

                           $('#DateOfPurchase').val($('#DateOfPurchaseDateOfPurchaseDay').val()+'/'+$('#DateOfPurchaseDateOfPurchaseMonth').val()+'/'+$('#DateOfPurchaseDateOfPurchaseYear').val());

                   } 

                }
              
              
              $( "#DateOfPurchase" ).datepicker({
			dateFormat: "dd/mm/yy",
                        showOn: "button",
			buttonImage: "{$_subdomain}/css/Skins/{$_theme}/images/calendar.png",
			buttonImageOnly: true,
                        maxDate: '0',
                        changeMonth: true,
                        changeYear: true,
                        onClose: function(dateText, inst) { 
                        
                        if($("#DateOfPurchase").val()!="dd/mm/yyyy")
                        {    
                            $("#DateOfPurchase").removeClass("auto-hint"); 
                        }
                        
                        },
                        onSelect: function(dateText, inst) { 
                        
                            $dateArray = dateText.split("/"); 
                            
                            
                            $("#DateOfPurchaseDateOfPurchaseDay").val($dateArray[0]);
                            setValue("#DateOfPurchaseDateOfPurchaseDay", ":first");
                            
                            $("#DateOfPurchaseDateOfPurchaseMonth").val($dateArray[1]);
                            setValue("#DateOfPurchaseDateOfPurchaseMonth", ":eq(1)");
                            
                            $("#DateOfPurchaseDateOfPurchaseYear").val($dateArray[2]);
                            setValue("#DateOfPurchaseDateOfPurchaseYear", ":eq(2)");
                            
                             
                        }
                    });
                    
                    
                   function getClientsList()
                   {
                       
                            var $NetworkID = $("#NetworkID").val();
                            
                            
                            
                            $clientDropDownList = '<option value="" selected="selected"></option>';     
                            if($NetworkID && $NetworkID!='')
                            {
                               

                                //Getting clients for selected network.   
                                $.post("{$_subdomain}/Data/getClients/"+urlencode($NetworkID),        

                                '',      
                                function(data){
                                        var $networkClients = eval("(" + data + ")");

                                        if($networkClients)
                                        {

                                            for(var $i=0;$i<$networkClients.length;$i++)
                                            {
                                                   $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'"  >'+$networkClients[$i]['ClientName']+'</option>';
                                            }
                                        }

                                         $("#ClientID").html('');
                                         $("#ClientID").html($clientDropDownList);
                                         
                                         if($networkClients.length==1)
                                         {    
                                            $("#ClientID").val($networkClients[0]['ClientID']);
                                         }
                                         else
                                         {
                                            $("#ClientID").val('');
                                         }
                                         
                                         setValue("#ClientID");
                                        
                                       

                                });
                            }
                            else
                            {
                                $("#ClientID").html('');
                                $("#ClientID").html($clientDropDownList);
                                $("#ClientID").val('');
                                setValue("#ClientID");
                            }
                            
                            
                           
                       
                   }    
                   
                   
                   
                   
                   function getBranchesList()
                   {
                       
                            var $ClientID = $("#ClientID").val();
                            
                            
                            
                            $branchDropDownList = '<option value="" selected="selected"></option>';   
                            $colBranchDropDownList = '<option value="-1"  >{$page['Text']['pseudo_branch']|escape:'html'}</option>';   
                            
                            if($ClientID && $ClientID!='')
                            {
                               

                                //Getting branches for selected client.   
                                $.post("{$_subdomain}/Data/getBranches/"+urlencode($ClientID)+"/"+$("#NetworkID").val(),      

                                '',      
                                function(data){
                                        var $clientsBranches = eval("(" + data + ")");

                                        if($clientsBranches)
                                        {

                                            for(var $i=0;$i<$clientsBranches.length;$i++)
                                            {
                                                   $branchDropDownList    += '<option value="'+$clientsBranches[$i]['BranchID']+'"   >'+$clientsBranches[$i]['BranchName']+'</option>';
                                                   $colBranchDropDownList += '<option value="'+$clientsBranches[$i]['BranchID']+'"   >'+$clientsBranches[$i]['BranchName']+'</option>';
                                            }
                                        }

                                         $("#BranchID").html('');
                                         $("#BranchID").html($branchDropDownList);
                                         
                                         $("#jbColBranchAddressID").html('');
                                         $("#jbColBranchAddressID").html($colBranchDropDownList);
                                         
                                         if($clientsBranches.length==1)
                                         {    
                                            $("#BranchID").val($clientsBranches[0]['ClientID']);
                                         }
                                         else
                                         {
                                            $("#BranchID").val('');
                                         }
                                         
                                         setValue("#BranchID");
                                        
                                       

                                });
                            }
                            else
                            {
                                $("#BranchID").html('');
                                $("#BranchID").html($branchDropDownList);
                                $("#BranchID").val('');
                                setValue("#BranchID");
                            }
                            
                            
                           getBranchAddressList();
                       
                   }
                   
                   
                   function getBranchAddressList()
                   {
                    

                                $branchDropDownList1 = '<option value="" selected="selected">Select from drop down</option>';   

                                //Getting branches  for selected client and network.   
                                $.post("{$_subdomain}/Data/getBranchAddress/ClientID="+urlencode($("#ClientID").val())+"/NetworkID="+urlencode($("#NetworkID").val()),        

                                '',      
                                function(data){
                                        var $clientBranches = eval("(" + data + ")");

                                        if($clientBranches)
                                        {
                                            for(var $i=0;$i<$clientBranches.length;$i++)
                                            {

                                                $branchDropDownList1 += '<option value="'+$clientBranches[$i]['BuildingNameNumber']+';;'+$clientBranches[$i]['Street']+';;'+$clientBranches[$i]['LocalArea']+';;'+$clientBranches[$i]['TownCity']+';;'+$clientBranches[$i]['CountyID']+';;'+$clientBranches[$i]['CountryID']+';;'+$clientBranches[$i]['PostalCode']+';;'+$clientBranches[$i]['ContactPhone']+';;'+$clientBranches[$i]['ContactPhoneExt']+';;'+$clientBranches[$i]['ContactEmail']+';;'+$clientBranches[$i]['BranchName']+'" >'+$clientBranches[$i]['BranchName']+'</option>';
                                            }
                                        }

                                        $("#jbBranchAddress").html('');
                                        $("#jbBranchAddress").html($branchDropDownList1);

                                        $("#jbBranchAddress").combobox({ change: function() { autoFillBranchAddress() } });  

                                });


                   }
                    
                    
                    
                   function getStockCodeList()
                   {
                    

                                $stockCodeDropDownList = '<option value="" selected="selected">Select from drop down</option>';   

                                //Getting branches  for selected client and network.   
                                $.post("{$_subdomain}/Data/getStockCodes/ClientID="+urlencode($("#ClientID").val())+"/NetworkID="+urlencode($("#NetworkID").val()),        

                                '',      
                                function(data){
                                        var $stockCodes = eval("(" + data + ")");

                                        if($stockCodes)
                                        {
                                            for(var $i=0;$i<$stockCodes.length;$i++)
                                            {
                                                $stockCodeDropDownList += '<option value="'+$stockCodes[$i]['ProductNo']+';;'+$stockCodes[$i]['ModelNumber']+';;'+$stockCodes[$i]['ModelDescription']+';;'+$stockCodes[$i]['UnitTypeID']+';;'+$stockCodes[$i]['ManufacturerID']+'" >'+$stockCodes[$i]['ProductNo']+'</option>';
                                            }
                                        }

                                        $("#jbStockCode").html('');
                                        $("#jbStockCode").html($stockCodeDropDownList);

                                        $("#jbStockCode").combobox({ change: function() { 
                                        
                                                assignDetails($("#jbStockCode").val().split(";;"));
                       
                                                $("#jbStockCode").focus();
                                                
                                            } });  

                                });


                   } 
                    
                    
                   function getManufacturersList()
                   {
                    

                                $ManufacturersDropDownList = '<option value="" selected="selected">Select from drop down</option>';   

                                //Getting branches  for selected client and network.   
                                $.post("{$_subdomain}/Data/getManufacturers/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ClientID").val()),        

                                '',      
                                function(data){
                                        var $Manufacturers = eval("(" + data + ")");

                                        if($Manufacturers)
                                        {
                                            for(var $i=0;$i<$Manufacturers.length;$i++)
                                            {
                                                $ManufacturersDropDownList += '<option value="'+$Manufacturers[$i]['ManufacturerID']+'" >'+$Manufacturers[$i]['ManufacturerName']+'</option>';
                                            }
                                        }

                                        $("#jbManufacturerID").html('');
                                        $("#jbManufacturerID").html($ManufacturersDropDownList);

                                        $("#jbManufacturerID").combobox();  

                                });


                   } 
                    
                    
                    
                   function getServiceTypesList()
                   {
                    

                                $ServiceTypesDropDownList = '<option value="" selected="selected">Select from drop down</option>';   

                                //Getting branches  for selected client and network.   
                                $.post("{$_subdomain}/Data/getServiceTypes/"+urlencode($("#JobTypeID").val())+'/'+urlencode($("#NetworkID").val())+"//"+urlencode($("#ClientID").val()),        

                                '',      
                                function(data){
                                        var $ServiceTypes = eval("(" + data + ")");

                                        if($ServiceTypes)
                                        {
                                            for(var $i=0;$i<$ServiceTypes.length;$i++)
                                            {
                                                $ServiceTypesDropDownList += '<option value="'+$ServiceTypes[$i]['ServiceTypeID']+'" >'+$ServiceTypes[$i]['Name']+'</option>';
                                            }
                                        }

                                        $("#jbServiceTypeID").html('');
                                        $("#jbServiceTypeID").html($ServiceTypesDropDownList);

                                        $("#jbServiceTypeID").combobox();  

                                });


                   } 
                    
                    
                    
                    
                    
                   function getUnitTypesList()
                   {
                    

                            $UnitTypesDropDownList = '<option value="" selected="selected">Select from drop down</option>';   

                            //Getting branches  for selected client and network.   
                            $.post("{$_subdomain}/Data/getUnitTypes/"+urlencode($("#ClientID").val()),        

                            '',      
                            function(data){
                                    var $UnitTypes = eval("(" + data + ")");

                                    if($UnitTypes)
                                    {
                                        for(var $i=0;$i<$UnitTypes.length;$i++)
                                        {
                                            $UnitTypesDropDownList += '<option value="'+$UnitTypes[$i]['UnitTypeID']+'" >'+$UnitTypes[$i]['UnitTypeName']+'</option>';
                                        }
                                    }

                                    $("#jbUnitTypeID").html('');
                                    $("#jbUnitTypeID").html($UnitTypesDropDownList);

                                    $("#jbUnitTypeID").combobox({
                                            change: function() {




                                                $.post("{$_subdomain}/Data/getUnitTypeAccessories", { unitTypeID: $("#jbUnitTypeID").val() }, function(response) {

                                                    var $html='';

                                                    var p = eval("(" + response + ")");

                                                    if(p) {

                                                        $html = '<select name="AccessoriesCB[]" multiple="multiple" id="AccessoriesCB" >';
                                                        for($a=0;$a<p.length;$a++)
                                                        {    
                                                             $html += '<option value="'+p[$a][0]+'" >'+p[$a][1]+'</option>';
                                                        } 
                                                        $html += '</select>';
                                                    }
                                                    else
                                                    {
                                                       $html = '<input  type="text" maxlength="200" style="width:335px;" class="text" name="Accessories" id="jbAccessories" value="" >';
                                                    }

                                                    $("#AccessoriesSpan1").html($html);



                                                    $("#AccessoriesCB").multiselect( {  noneSelectedText: "{$page['Text']['select_accessories']|escape:'html'}", minWidth: 100, height: "auto", show:['slide', 500]  } );

                                                    $(".ui-multiselect").css("width", "342px");
                                                });





                                            }
                                        });


                            });


                   }
                    
                    
                      
                  
                    //This is called when branch addreess dropdown changed.
                    function autoFillBranchAddress()
                    {
                           
                            selected = $('#jbBranchAddress').val();


                            if(selected)
                            {
                                var addressFieldsArray = new Array("#jbBuildingName", "#jbStreet", "#jbArea", "#jbCity", "#jbCounty", "#jbCountry", "#jbPostalCode", "#jbContactHomePhone", "#ContactHomePhoneExt", "#jbContactEmail", "#jbCompanyName");

                                var selected_split = selected.split(";;"); 	

                                for($i=0;$i<addressFieldsArray.length;$i++)
                                {
                                    $(addressFieldsArray[$i]).val(trim(selected_split[$i])).blur();
                                    if(addressFieldsArray[$i]=="#jbCounty" || addressFieldsArray[$i]=="#jbCountry")
                                    {
                                        setValue(addressFieldsArray[$i]);
                                    }
                                }
                            } 
                       
                    }
                    
                    
                   {if $showNetworkDropDown}
                 
                    $("#NetworkID").combobox({ change: function() { getClientsList(); getServiceTypesList(); } });
                    
                   {/if}     
                                                      
                   {if $showClientDropDown}
                 
                    $("#ClientID").combobox({ change: function() { getBranchesList(); getStockCodeList(); getManufacturersList(); getUnitTypesList(); getServiceTypesList(); } });
                    
                   {/if} 
               
                   {if $showBranchDropDown}
                 
                   // $("#BranchID").combobox({ change: function() { } });
                    
                   {/if}
               
               
                   $("#jbBranchAddress").combobox({ change: function() { autoFillBranchAddress() } });   
               
                   $("#jbProductLocation").combobox();
                   
                   
                   $( "#jbColBranchAddressID" ).combobox();
                   
                   $( "#jbColAddCounty" ).combobox({ change: function() { setCountryBasedOnCounty("#jbColAddCounty", "#jbColAddCountry", "#jb_p_c_a_country") } });
               
                   $( "#jbColAddCountry" ).combobox();
                   
                   
                    $( "#jbStockCode" ).combobox({ change: function() { 
          
                       assignDetails($("#jbStockCode").val().split(";;"));
                       
                       $("#jbStockCode").focus();
  
                    } });
                    
                    
                    
                    //Locate product details fields enable/disable based on option selection
                    locateByDetails($("[name='LocateBy']:checked").val(), false);

                    /* Add a click handler to the locate by radio buttons - strats here*/
                       $(document).on('click', "[name='LocateBy']", 
                           function() {


                               locateByDetails($(this).val(), true);


                           }

                         );  
                    /* Add a click handler to the locate by radio buttons - ends here*/


                    $( "#jbModelName" ).autocomplete({
			
                        
                            source: function( request, response ) {
                                    $.ajax({
                                        url: "{$_subdomain}/Data/getModelNumbers/list=1/manufacturer="+$("#jbManufacturerID").val()+"/",
                                        dataType: "json",
                                        data: {
                                            featureClass: "P",
                                            style: "full",
                                            maxRows: 12,
                                            name_startsWith: request.term
                                        },
                                        success: function( data ) {
                                            response( $.map( data.models, function( item ) {
                                                return {
                                                    label: item.ModelNumber,
                                                    value: item.ModelNumber
                                                }
                                            }));
                                        }
                                    });
                            },

                            minLength: 2,


                            change: function(event, ui) { 

                            if($( "#jbModelName" ).val()) {


                                $.post("{$_subdomain}/Data/getModelNumbers/manufacturer="+$("#jbManufacturerID").val()+"/",        

                                { ModelNumber: $( "#jbModelName" ).val() },   

                                function(data){
                                    // DATA NEXT SENT TO COLORBOX
                                    var p = eval("(" + data + ")");
                                    if(p)
                                    {

                                           assignModelDetails(p); 
                                           $( "#jbModelName" ).focus();
                                    }


                                }); //Post ends here...


                            }

                            },

                            select: function(event, ui) { 

                                if(ui.item && ui.item.value) {

                                    $.post("{$_subdomain}/Data/getModelNumbers/manufacturer="+$("#jbManufacturerID").val()+"/",        

                                    { ModelNumber: ui.item.value },   

                                    function(data){
                                        // DATA NEXT SENT TO COLORBOX
                                        var p = eval("(" + data + ")");
                                        if(p)
                                        {
                                               assignModelDetails(p);
                                               $( "#jbModelName" ).focus();
                                        } 

                                    }); //Post ends here...

                                }
                            }

                    }); 
              
              
                     $("#jbManufacturerID").combobox();
                    
                    
                    
                    $("#jbUnitTypeID").combobox({
                        change: function() {

                         


                            $.post("{$_subdomain}/Data/getUnitTypeAccessories", { unitTypeID: $("#jbUnitTypeID").val() }, function(response) {

                                var $html='';

                                var p = eval("(" + response + ")");

                                if(p) {

                                    $html = '<select name="AccessoriesCB[]" multiple="multiple" id="AccessoriesCB" >';
                                    for($a=0;$a<p.length;$a++)
                                    {    
                                         $html += '<option value="'+p[$a][0]+'" >'+p[$a][1]+'</option>';
                                    } 
                                    $html += '</select>';
                                }
                                else
                                {
                                   $html = '<input  type="text" maxlength="200" style="width:335px;" class="text" name="Accessories" id="jbAccessories" value="" >';
                                }

                                $("#AccessoriesSpan1").html($html);



                                $("#AccessoriesCB").multiselect( {  noneSelectedText: "{$page['Text']['select_accessories']|escape:'html'}", minWidth: 100, height: "auto", show:['slide', 500]  } );

                                $(".ui-multiselect").css("width", "342px");
                            });


                          


                        }
                    });
                    
                   


                    $("#jbServiceTypeID").combobox();

                    $("#jbMobilePhoneNetworkID").combobox();
                    
                    
                    $("#jbServiceTypeList").multiselect( {  noneSelectedText: "Select from drop down", minWidth: 100, height: "auto", show:['slide', 500]  } );
                    $(".ui-multiselect").css("width", "400px");
                    
                    
                    
                     $(document).on('click', '#jbAccessoriesRadio', 
                    function(e) {
                    
                        $('#jbAccessories').val('');
                   /////////////     $('.ui-multiselect-none').trigger("click");
                        
                        setHiddenAccessoriesFlag();
                        
                    });
                    
           {*  $(document).on('click', '.ui-multiselect-all', 
                    function(e) {
                    
                        checkCheckboxStatus();
                        setHiddenAccessoriesFlag();
                        
                    });
                    
                    
             $(document).on('click', '.ui-multiselect-none', 
                    function(e) {
                    
                        checkCheckboxStatus();
                        setHiddenAccessoriesFlag();
                        
                    });        
                    
                    
                    
                    
                    $(document).on('click', '.ui-multiselect-checkboxes input', 
                         function(e) {

                             checkCheckboxStatus();
                             setHiddenAccessoriesFlag();

                         });   *}  

                    function checkCheckboxStatus ()
                    {

                           var  $CheckFlag = false;

                              $('.ui-multiselect-checkboxes input').each(function() {


                                     if($(this).is(':checked')) {

                                            $CheckFlag = true;

                                         }

                              }); 

                             if($CheckFlag)
                             {
                                 $('#jbAccessoriesRadio').removeAttr("checked");
                             }
                        return $CheckFlag;
                    }    


                    $(document).on({
                         keyup: function() {

                              if($('#jbAccessories').val()!='')
                              {
                                      $('#jbAccessoriesRadio').removeAttr("checked");

                              }
                              setHiddenAccessoriesFlag();
                         },
                         blur: function() {

                              if($('#jbAccessories').val()!='')
                              {
                                      $('#jbAccessoriesRadio').removeAttr("checked");

                              }
                              setHiddenAccessoriesFlag();
                         },
                         click: function() {

                              if($('#jbAccessories').val()!='')
                              {
                                      $('#jbAccessoriesRadio').removeAttr("checked");

                              } 
                              setHiddenAccessoriesFlag();
                         },
                         focus: function() {

                              if($('#jbAccessories').val()!='')
                              {
                                      $('#jbAccessoriesRadio').removeAttr("checked");

                              } 
                              setHiddenAccessoriesFlag();
                         }
                     }, "#jbAccessories");


                    function setHiddenAccessoriesFlag()
                    {

                        if($('#jbAccessoriesRadio').is(':checked') || ($('#jbAccessories').length > 0 && $('#jbAccessories').val()!='') || checkCheckboxStatus())
                        {
                             $('#HiddenAccessoriesFlag').val('1');

                        }    
                        else
                        {
                             $('#HiddenAccessoriesFlag').val(''); 

                        }
                    }
              
              
              
               
                   $(document).on('click', '#tagAll', 
                    function(e) {
                    
                            $(".rightCheckboxC1").attr("checked", "checked");
                            $(".rightCheckboxC2").attr("checked", "checked");
                            $(".rightCheckboxC3").attr("checked", "checked");
                         
                            checkStar();
                             
                         return false;
                    });
                    
                    
                    $(document).on('click', '#untagAll', 
                    function(e) {
                    
                            $(".rightCheckboxC1").removeAttr("checked");
                            $(".rightCheckboxC2").removeAttr("checked");
                            $(".rightCheckboxC3").removeAttr("checked");
                            
                            checkStar();
                             
                         return false;
                    });
                    
                    function checkStar()
                    {
                        $('.rightCheckboxC2').each(function() {

                                   $starID = $(this).attr("id").replace("_chk2","Sup");

                                   if($(this).is(':checked')) {

                                          $("#"+$starID).show();

                                       }
                                       else
                                       {
                                             $("#"+$starID).hide();   
                                       }

                            }); 
                     }
                     
                     checkStar();
                         
                     $(document).on('click', '.rightCheckboxC2', 
                        function(e) {

                                    $starID = $(this).attr("id").replace("_chk2","Sup");
                                    
                                    if($(this).is(':checked')) 
                                    {

                                       $("#"+$starID).show();
                                       
                                    }
                                    else
                                    {
                                          $("#"+$starID).hide();   
                                    }

                             
                        });  
                        
                        
                        
                        
                        
                    /* Add a click handler to the save settings of job booking form starts here*/  

                    $(document).on('click', '#saveRecord', 
                            function() {
                            
                                      

                                      $('#jbContactHomePhone').blur();
                                      $('#ContactWorkPhoneExt').blur();
                                      $('#jbContactMobile').blur();  
                                      
                                      
                                      if($("#DateOfPurchase").val()=="dd/mm/yyyy")
                                      {
                                          $("#DateOfPurchase").val('');
                                      }
                                      
                                      if($("#jbContactMobile").val()=="{$page['Text']['mobile']|escape:'html'}")
                                      {
                                                $("#jbContactMobile").val('');
                                      }
                                      
                                      
                                      
                                      if($("#jbProductDescription").val()=="{$page['Text']['text_description_title']|escape:'html'}")
                                      {
                                                $("#jbProductDescription").val('');
                                      }
                                      
                                      if($("#jbModelName").val()=="{$page['Text']['model_number_title']|escape:'html'}")
                                      {
                                                $("#jbModelName").val('');
                                      }
                                      
                                     
                                     //Validating and posting form data.
                                      
                                       $('.auto-hint').each(function() {
                                                    $this = $(this);
                                                    if ($this.val() == $this.attr('title')) {
                                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                                        if ($this.hasClass('auto-pwd')) {
                                                            $this.prop('type','password');
                                                        }
                                                    }
                                        } );
                                        
                                        
                                        if($("label.fieldError:visible").length>0)
                                        {

                                        }
                                        else
                                        {
                                            $("#saveRecord").hide();
                                            $("#cancelChanges").hide();
                                            $("#processDisplayText").show();

                                        }
                                      
                                      
                                      

                                        $('#jobBookingForm').validate({
                                         
                                         
                                            ignore: '',
                                        
                                            rules:  {
                                               
                                                NetworkID:
                                                   {
                                                       required: false
                                                   },
                                                ClientID:
                                                    {
                                                        required: false
                                                    }
                                              
                                            },
                                            messages: {
                                                        NetworkID:
                                                        {
                                                            required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                        ClientID:
                                                        {
                                                            required: "{$page['Errors']['client']|escape:'html'}"
                                                        },
                                                       /* BranchID:
                                                        {
                                                            required: "{$page['Errors']['branch']|escape:'html'}"
                                                        },*/
                                                        JobTypeName:
                                                        {
                                                            required: "{$page['Errors']['button_name']|escape:'html'}"
                                                        }
                                                        
                                                    }, 
                                            errorPlacement: function(error, element) {
                                                
                                                error.insertAfter( element );
                                                
                                                $("#processDisplayText").hide();
                                                $("#saveRecord").show();
                                                $("#cancelChanges").show();
                                                
                                            },
                                            errorClass: 'fieldError',
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: 'label',

                                            submitHandler: function() {
                                    
                                                    
                                                    $.post("{$_subdomain}/Job/settings/",        

                                                             $("#jobBookingForm").serialize(),      
                                                             function(data){
                                                                 // DATA NEXT SENT TO COLORBOX
                                                                 var p = eval("(" + data + ")");

                                                                         if(p['status']=="ERROR")
                                                                         {


                                                                             $("#processDisplayText").hide();
                                                                             $("#saveRecord").show();
                                                                             $("#cancelChanges").show();


                                                                             alert(p['message']);       

                                                                         }
                                                                         else if(p['status']=="OK")
                                                                         {

                                                                               {* $("#processDisplayText").hide();
                                                                                $("#saveRecord").show();
                                                                                $("#cancelChanges").show();
                                                                                *}
                                                                                document.location.href = "{$_subdomain}/index/index/";  

                                                                         }
                                                             }); //Post ends here... 

                                                         
                                                     
                                                }
                                            });
                            
                            
                            
                              }      
                        );
                     /* Add a click handler to the save settings of job booking form ends here*/ 
       
                    
                    
                      //Click handler for cancelChanges.
                        $(document).on('click', '#cancelChanges', 
                      function() {
                      
                        document.location.href = "{$_subdomain}/index/index/";
                        return false;
                          
                      });
                   

            });
            
            
            
             function addressLookup() {
                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#jbPostalCode').val(),
                            success:function(data, textStatus) { 

                                      if(data=='EM1011' || data.indexOf("EM1011")!=-1) {

                                            $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectOutput").slideDown("slow");

                                            $("#jbBuildingName").val('');	
                                            $("#jbStreet").val('');
                                            $("#jbArea").val('');
                                            $("#jbCity").val('');

                                            $("#jbCounty").val('').blur();
                                            setValue("#jbCounty");
                                            $("#jbCountry").val('').blur();
                                            setValue("#jbCountry");

                                            $("#jbCompanyName").val('').blur();
                                            $("#jbPostalCode").focus();



                                      } else {

                                         $('#selectOutput').html(data); 
                                         $("#selectOutput").slideDown("slow");
                                         $("#postSelect").removeClass();
                                         $("#postSelect").removeAttr("style");
                                         location.href = "#jbContactMobile";
                                         $("#postSelect").focus();

                                      }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                    $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                    $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')",500);
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                }
                
                
                
                function addressLookup2() {
                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#jbColAddPostalCode').val(),
                            success:function(data, textStatus) { 

                                      if(data=='EM1011' || data.indexOf("EM1011")!=-1) {

                                            $('#selectColAddOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                            $("#selectColAddOutput").slideDown("slow");


                                            $("#jbColAddCompanyName").val('');
                                            $("#jbColAddBuildingName").val('');	
                                            $("#jbColAddStreet").val('');
                                            $("#jbColAddArea").val('');
                                            $("#jbColAddTownCity").val('');
                                            $("#jbColAddCounty").val('').blur();
                                            setValue("#jbColAddCounty");
                                            $("#jbColAddCountry").val('').blur();
                                            setValue("#jbColAddCountry");
                                            $("#jbColAddEmail").val('');
                                            $("#jbColAddPhone").val('');
                                            $("#ColAddPhoneExt").val('');

                                            $("#jbColAddPostalCode").focus();


                                      } else {

                                         $('#selectColAddOutput').html(data); 
                                         $("#selectColAddOutput").slideDown("slow");
                                         $("#postSelect_1").removeClass();
                                         $("#postSelect_1").removeAttr("style");


                                         location.href = "#jbColAddPostalCode";
                                         $("#postSelect_1").focus();
                                      }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                    $('#fetch').fadeIn('medium'); 
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                    $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')",500);
                            },
                            url:'{$_subdomain}/index/addresslookup/1' 
                        } ); 
                    return false;
                }
                
              //This function assigns the data to the elements on Catalogue no change  -- starts here....
              function assignDetails ($rowData)
              {

                     var $pickDataIdList = new Array('#jbHiddenStockCode', '#jbModelName', '#jbProductDescription', '#jbUnitTypeID', '#jbManufacturerID');

                      for($i=0;$i<$pickDataIdList.length;$i++)
                      {
                          $($pickDataIdList[$i]).val($rowData[$i]).removeAttr('disabled').removeClass("auto-hint");

                          if($pickDataIdList[$i]!='#jbModelName')
                          {
                              $($pickDataIdList[$i]).blur();
                          }

                          if($pickDataIdList[$i]=='#jbManufacturerID' || $pickDataIdList[$i]=='#jbUnitTypeID')
                              {
                                  setValue($pickDataIdList[$i]);

                                  if($pickDataIdList[$i]=='#jbUnitTypeID' && $('#jbUnitTypeID').val()=='-1')
                                  {
                                        $('#RequestNewUnitType').trigger('click');  
                                  }
                              }
                      }
              }
             //This function assigns the data to the elements on Catalogue no change -- ends here..      
              
              
            function locateByDetails($eNum, $focus)
            {

                if($eNum=='1')
                {

                     $("#jbModelNameRow").show("fast"); 
                     $("#jbModelName").attr("title", "{$page['Text']['model_number_title']|escape:'html'}"); 

                     if($focus)
                     {
                         $("#jbModelName").focus();
                     }
                     $("#jbProductDescriptionRow").hide("fast"); 
                     $("#jbProductDescription").val('');

                }
                else  if($eNum=='2')
                {
                     $("#jbProductDescriptionRow").show("fast"); 
                     $("#jbProductDescription").attr("title", "{$page['Text']['text_description_title']|escape:'html'}"); 
                     if($focus)
                     {
                         $("#jbProductDescription").focus();
                     }

                     $("#jbModelName").val(''); 
                     $("#jbModelNameRow").hide("fast");

                }
            }
            
            
             //This function assigns the data to the elements on model no change  --starts here....
             function assignModelDetails ($rowData)
             {

                    var $pickDataIdList = new Array('#jbProductDescription', '#jbUnitTypeID', '#jbManufacturerID');

                     for($i=0;$i<$pickDataIdList.length;$i++)
                     {
                         $($pickDataIdList[$i]).val($rowData[$i]).removeAttr('disabled');
                         $($pickDataIdList[$i]).blur();

                         if($pickDataIdList[$i]=='#jbManufacturerID' || $pickDataIdList[$i]=='#jbUnitTypeID')
                             {

                                 setValue($pickDataIdList[$i]);

                                 if($pickDataIdList[$i]=='#jbUnitTypeID' && $('#jbUnitTypeID').val()=='-1')
                                 {
                                       $('#RequestNewUnitType').trigger('click');  
                                 }

                             }
                     }
             }
            //This function assigns the data to the elements on model no change -- ends here..
       


    </script>

{/block}

{block name=body}
  
    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> /  {$page_title|escape:'html'}
    </div>
</div>
        
<div class="main" id="home">
                               
    <div class="jobBookingPanel" >
         <form id="jobBookingForm" name="jobBookingForm" method="post"  action="#" class="inline" autocomplete="off" >
       
             
            <fieldset>
            <legend>{$page_legend|escape:'html'}</legend>
           
             <p id="JobTypeIDElement" >
                   <label class="fieldLabel" for="JobTypeID" >{$page['Labels']['button_name']|escape:'html'}: <sup >*</sup></label>
                   &nbsp;&nbsp; 
                   {$datarow.JobTypeName|escape:'html'}
                   

                   <input type="hidden" name="JobTypeID" value="{$datarow.JobTypeID|escape:'html'}"  id="JobTypeID" >
                  <br><br>
            </p>
            
            
            <p id="NetworkIDElement" >
               <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}: </label>
               &nbsp;&nbsp; 
               {if $showNetworkDropDown}
               <select name="NetworkID" id="NetworkID">
                   <option value="" selected="selected" ></option>
		    {foreach $networks as $network}
			<option value="{$network.NetworkID}"  {if $datarow.NetworkID eq $network.NetworkID} selected="selected" {/if} >{$network.CompanyName|escape:'html'}</option>
		    {/foreach}
               </select>
               {else}
                    <input name="NetworkID" id="NetworkID" type="hidden" value="{$datarow.NetworkID|escape:'html'}" >
                    {$NetworkName|escape:'html'}
               {/if}
            </p>
            
            
            <p id="ClientIDElement" >
               <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}: </label>
               &nbsp;&nbsp; 
               {if $showClientDropDown}
                <select name="ClientID" id="ClientID" >
                    <option value="" selected="selected" ></option>

                     {foreach $networkClients as $client}
                        
                            <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID} selected="selected" {/if}  >{$client.ClientName|escape:'html'}</option>

                     {/foreach}

                </select>
               {else}
                    <input name="ClientID" id="ClientID" type="hidden" value="{$datarow.ClientID|escape:'html'}" >
                    {$ClientName|escape:'html'}
               {/if}    
            </p>
            
            
            {*<p id="BranchIDElement" >
               <label class="fieldLabel" for="BranchID" >{$page['Labels']['branch']|escape:'html'}: <sup>*</sup></label>
               &nbsp;&nbsp; 
               
                    {if $showBranchDropDown}
                    <select name="BranchID" id="BranchID" >
                        <option value="" selected="selected" ></option>
                         {foreach $branches as $branch}
                             <option value="{$branch.BranchID}"  {if $datarow.BranchID eq $branch.BranchID} selected="selected" {/if} >{$branch.BranchName|escape:'html'}</option>
                         {/foreach}
                    </select>
                    {else}
                         <input name="BranchID" id="BranchID" type="hidden" value="{$datarow.BranchID|escape:'html'}" >
                         {$BranchName|escape:'html'}
                    {/if}
            </p>*}
            
           
                
               
            
            
           {*  {if $showClientDropDown}
               <p id="jbServiceTypeListElement" >
                   <label class="fieldLabel" for="jbServiceTypeList" >{$page['Labels']['service_type_list']|escape:'html'}:<sup>*</sup></label>
                  &nbsp;&nbsp; <select name="ServiceTypeList[]" multiple="multiple" id="jbServiceTypeList" class="text" >


                                   {foreach $service_type_list as $stypel}

                                   <option value="{$stypel.ServiceTypeID}" {if in_array($stypel.ServiceTypeID, $datarow.ServiceTypeList)}selected="selected"{/if} >{$stypel.Name|escape:'html'}</option>


                                   {/foreach}


                       </select>

               </p>
            {/if}*}
            
            
            <p>
               <label class="fieldLabel" >&nbsp;</label>
               <div style="margin-left:300px;" >
               {$page['Labels']['key_text']|escape:'html'}:<br>
               <label style="width:15px;height:15px;background-color:blue;margin:2px;padding:2px;" >&nbsp;&nbsp;&nbsp;</label> {$page['Labels']['displayed_fields']|escape:'html'}<br>
               <label style="width:15px;height:15px;background-color:red;margin:2px;padding:2px;" >&nbsp;&nbsp;&nbsp;</label> {$page['Labels']['compulsory_fields']|escape:'html'}<br>
               <label style="width:15px;height:15px;background-color:green;margin:2px;padding:2px;" >&nbsp;&nbsp;&nbsp;</label> {$page['Labels']['replicate_data']|escape:'html'}<br>
               </div>
               
            </p>
            <p> <span style="float:right;margin-right:10px;" >
                
                    <a href="#" id="tagAll" >{$page['Text']['tag_all']|escape:'html'}</a>&nbsp;&nbsp;
                    <a href="#" id="untagAll" >{$page['Text']['clear_all']|escape:'html'}</a>
                    <br>
                    &nbsp;&nbsp;
                   <label style="width:10px;height:10px;background-color:blue;margin:3px;padding:2px;" >&nbsp;&nbsp;</label>
                   &nbsp;
                   <label style="width:10px;height:10px;background-color:red;margin:3px;padding:2px;" >&nbsp;&nbsp;</label> 
                   &nbsp;
                   <label style="width:10px;height:10px;background-color:green;margin:3px;padding:2px;" >&nbsp;&nbsp;</label>
             
                </span>
            
            
            </p>
                
                
             <p id="jbCustomerTypeElement"  >
               <label for="jbCustomerType" class="fieldLabel" >{$page['Labels']['customer_type']|escape:'html'}:<sup id="CustomerTypeSup" >*</sup></label>
               &nbsp;&nbsp; 
               <input  type="radio" name="CustomerType" id="CustomerTypeConsumer" value="Consumer" {if $datarow.CustomerType eq "Consumer"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['consumer']|escape:'html'}</span>   
               <input  type="radio" name="CustomerType" id="CustomerTypeBusiness" value="Business" {if $datarow.CustomerType eq "Business"} checked="checked" {/if}  /> <span class="text" >{$page['Text']['business']|escape:'html'}</span>    
            
                  <span style="float:right;margin-right:10px;" >
                   <input type="checkbox" name="CustomerTypeChk1" id="CustomerType_chk1" value="1" {if $datarow.CustomerTypeChk1|escape:'html'} checked="checked" {/if}  class="rightCheckboxC1" >&nbsp;&nbsp;
                   <input type="checkbox" name="CustomerTypeChk2" id="CustomerType_chk2" value="1" {if $datarow.CustomerTypeChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                   <input type="checkbox" name="CustomerTypeChk3" id="CustomerType_chk3" value="1" {if $datarow.CustomerTypeChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                   </span> 
                   
             </p> 
            
             <p id="jbCustomerNameTop" >
                <label class="fieldLabel" >&nbsp;</label>
                <label style="width:60px;" class="topTextLabel"  >{$page['Labels']['title']|escape:'html'}:</label>
                <label style="width:150px;" class="topTextLabel"  >{$page['Labels']['forename']|escape:'html'}: </label>
                <label style="width:150px;" class="topTextLabel" >{$page['Labels']['surname']|escape:'html'}:</label>
             
             </p>  

             <p id="jbCustomerName" >
                <label class="fieldLabel" for="jbContactTitle" id="jbContactTitleLabel" >{$page['Labels']['consumer_name']|escape:'html'}:<sup id="ContactTitleSup" >*</sup></label>
                   &nbsp;&nbsp;  
                   <select  name="CustomerTitleID" id="jbContactTitle" title="{$page['Labels']['contact_title']|escape:'html'}"  class="text auto-hint" style="width:60px;" >

                       <option value="" {if $datarow.CustomerTitleID eq ''}selected="selected"{/if}   ></option>
                       {foreach from=$selectTitle item=stitle}
                        <option value="{$stitle.TitleID}" {if $datarow.CustomerTitleID eq $stitle.TitleID}selected="selected"{/if} >{$stitle.Name|escape:'html'}</option>
                        {/foreach}

                    </select> 

                   <input type="text" style="width:150px;" class="text lowercaseText capitalizeText" name="ContactFirstName" value="{$datarow.ContactFirstName|escape:'html'}"  id="jbContactFirstName" >

                   <input type="text" style="width:150px;" class="text" name="ContactLastName" value="{$datarow.ContactLastName|escape:'html'}" id="jbContactLastName" > 
            
                   <span style="float:right;margin-right:10px;" >
                   <input type="checkbox" name="ContactTitleChk1" id="ContactTitle_chk1" value="1" {if $datarow.ContactTitleChk1|escape:'html'} checked="checked" {/if}  class="rightCheckboxC1" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactTitleChk2" id="ContactTitle_chk2" value="1" {if $datarow.ContactTitleChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactTitleChk3" id="ContactTitle_chk3" value="1" {if $datarow.ContactTitleChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                   </span> 
             </p>
        
        
            <p>
               <br> <hr style="height:1px" >
            </p>
            
            
            <p style="text-align:center;padding:10px;" >
               <span class="blueText"  >{$page['Text']['branch_address_info']|escape:'html'}</span>
            </p>
            <p id="jbBranchAddressElement" >
                <label class="fieldLabel" for="jbBranchAddress" >{$page['Labels']['branch_address']|escape:'html'}: <sup id="BranchIDAddressSup" >*</sup></label>
                   &nbsp;&nbsp; <select  name="BranchIDAddress" id="jbBranchAddress"   class="text" >

                        
                        <option value="" selected="selected" ></option>
                        {foreach from=$BranchDetails item=BA}
                           
                        <option value="{$BA.BuildingNameNumber|escape:'html'};;{$BA.Street|escape:'html'};;{$BA.LocalArea|escape:'html'};;{$BA.TownCity|escape:'html'};;{$BA.CountyID|escape:'html'};;{$BA.CountryID|escape:'html'};;{$BA.PostalCode|escape:'html'};;{$BA.ContactPhone|escape:'html'};;{$BA.ContactPhoneExt|escape:'html'};;{$BA.ContactEmail|escape:'html'};;{$BA.BranchName|escape:'html'}" {if $datarow.BranchIDAddress eq $BA.BranchID}selected="selected"{/if} >{$BA.BranchName|escape:'html'}</option>
                        
                        {/foreach}
                    
                    </select>
                       
                  <span style="float:right;margin-right:10px;" >
                   <input type="checkbox" name="BranchIDAddressChk1" id="BranchIDAddress_chk1" value="1" {if $datarow.BranchIDAddressChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                   <input type="checkbox" name="BranchIDAddressChk2" id="BranchIDAddress_chk2" value="1" {if $datarow.BranchIDAddressChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                   <input type="checkbox" name="BranchIDAddressChk3" id="BranchIDAddress_chk3" value="1" {if $datarow.BranchIDAddressChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                  </span>        
                       
            </p>
            
            
            
            <p>
                <label class="fieldLabel" for="jbContactEmail" id="jbContactEmailLabel" >{$page['Labels']['email']|escape:'html'}:<sup id="ContactEmailSup" >*</sup></label>
                  &nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$datarow.ContactEmail|escape:'html'}" id="jbContactEmail" >
                  
     
                  
                  <span style="float:right;margin-right:10px;" >
                   <input type="checkbox" name="ContactEmailChk1" id="ContactEmail_chk1" value="1" {if $datarow.ContactEmailChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactEmailChk2" id="ContactEmail_chk2" value="1" {if $datarow.ContactEmailChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactEmailChk3" id="ContactEmail_chk3" value="1" {if $datarow.ContactEmailChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                   </span>
                  
                  
                 <br>
                  <span class="text jobBookingValueInner" >

                  <input  type="checkbox" name="NoContactEmail"  id="jbNoContactEmail" tabindex="100"  >{$page['Text']['no_email']|escape:'html'}
                  </span><br>


             </p>

             <p>
                 <label class="fieldLabel" for="jbContactHomePhone" id="jbContactHomePhoneLabel" >{$page['Labels']['daytime_phone']|escape:'html'}:<sup id="ContactHomePhoneSup" >*</sup></label>
                 &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactHomePhone" value="{$datarow.ContactHomePhone|escape:'html'}" id="jbContactHomePhone" >
                 <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactWorkPhoneExt" value="{$datarow.ContactWorkPhoneExt|escape:'html'}" id="ContactWorkPhoneExt" >
                 
                 <span style="float:right;margin-right:10px;" >
                 <input type="checkbox" name="ContactHomePhoneChk1" id="ContactHomePhone_chk1" value="1" {if $datarow.ContactHomePhoneChk1|escape:'html'} checked="checked" {/if}  class="rightCheckboxC1" >&nbsp;&nbsp;
                 <input type="checkbox" name="ContactHomePhoneChk2" id="ContactHomePhone_chk2" value="1" {if $datarow.ContactHomePhoneChk2|escape:'html'} checked="checked" {/if}  class="rightCheckboxC2" >&nbsp;&nbsp;
                 <input type="checkbox" name="ContactHomePhoneChk3" id="ContactHomePhone_chk3" value="1" {if $datarow.ContactHomePhoneChk3|escape:'html'} checked="checked" {/if}  class="rightCheckboxC3" >&nbsp;&nbsp;
                 </span>
                   
             </p>

             <p>
                 <label class="fieldLabel" for="jbContactMobile" id="jbContactMobileLabel" >{$page['Labels']['mobile_no']|escape:'html'}:<sup id="ContactMobileSup" >*</sup></label>
                 &nbsp;&nbsp; <input type="text" class="text auto-hint" name="ContactMobile" class="text auto-hint" id="jbContactMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$datarow.ContactMobile|escape:'html'}" >

                 <span style="float:right;margin-right:10px;" >
                     
                 <input type="checkbox" name="ContactMobileChk1" id="ContactMobile_chk1" value="1" {if $datarow.ContactMobileChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                 <input type="checkbox" name="ContactMobileChk2" id="ContactMobile_chk2" value="1" {if $datarow.ContactMobileChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                 <input type="checkbox" name="ContactMobileChk3" id="ContactMobile_chk3" value="1" {if $datarow.ContactMobileChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                 
                 </span>
                 
                 <br>
                  <span class="text jobBookingValueInner" >

                  <input  type="checkbox" name="NoContactMobile"  id="jbNoContactMobile" tabindex="100"  >{$page['Text']['no_mobile']|escape:'html'}
                  </span><br>

             </p>


             <p id="jbContactAltMobileElement"  >

                 <label class="fieldLabel" for="jbContactAltMobile" id="jbContactAltMobileLabel" >{$page['Labels']['alt_sms_no']|escape:'html'}:<sup id="ContactAltMobileSup" >*</sup></label>
                 &nbsp;&nbsp; <input type="text" class="text auto-hint" name="ContactAltMobile" id="ContactAltMobile" title="{$page['Text']['mobile']|escape:'html'}"   value="{$datarow.ContactAltMobile|escape:'html'}" >

                 <span style="float:right;margin-right:10px;" >
                   <input type="checkbox" name="ContactAltMobileChk1" id="ContactAltMobile_chk1" value="1" {if $datarow.ContactAltMobileChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactAltMobileChk2" id="ContactAltMobile_chk2" value="1" {if $datarow.ContactAltMobileChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                   <input type="checkbox" name="ContactAltMobileChk3" id="ContactAltMobile_chk3" value="1" {if $datarow.ContactAltMobileChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                 </span>
                 
             </p>




             <p>
                <br> <hr style="height:1px" >
             </p>
             
             
             
            <p>
                <label class="fieldLabel" for="jbPostalCode" id="jbPostalCodeLabel" >{$page['Labels']['postcode']|escape:'html'}:<sup id="PostalCodeSup" >*</sup></label>
                &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 230px;" type="text" name="PostalCode" id="jbPostalCode" value="{$datarow.PostalCode|escape:'html'}" >&nbsp;

                <input type="button" name="quick_find_btn" style="position: relative; top:1px;" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="addressLookup();return false;"   value="Click to find Address" >

                &nbsp;&nbsp;<span id="fetch" style="display: none" class="blueText" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" ></span>

                
                <span style="float:right;margin-right:10px;" >
                
                    <input type="checkbox" name="PostalCodeChk1" id="PostalCode_chk1" value="1" {if $datarow.PostalCodeChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="PostalCodeChk2" id="PostalCode_chk2" value="1" {if $datarow.PostalCodeChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="PostalCodeChk3" id="PostalCode_chk3" value="1" {if $datarow.PostalCodeChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                </span>
                
            </p>


            <p id="selectOutput" ></p>


            <p  id="jbCompanyNameElement" >
                <label class="fieldLabel" for="jbCompanyName">{$page['Labels']['business_name']|escape:'html'}:<sup id="CompanyNameSup"  >*</sup></label>
                 &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="CompanyName" value="{$datarow.CompanyName|escape:'html'}"  id="jbCompanyName" >
                 
                <span style="float:right;margin-right:10px;" >
                    
                <input type="checkbox" name="CompanyNameChk1" id="CompanyName_chk1" value="1" {if $datarow.CompanyNameChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="CompanyNameChk2" id="CompanyName_chk2" value="1" {if $datarow.CompanyNameChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="CompanyNameChk3" id="CompanyName_chk3" value="1" {if $datarow.CompanyNameChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                
                </span>
            </p> 


            <p id="jb_p_house_name"  >
                <label class="fieldLabel" for="jbBuildingName" >{$page['Labels']['building_name']|escape:'html'}:<sup id="BuildingNameNumberSup"  >*</sup></label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber|escape:'html'}" id="jbBuildingName" >          
             
                <span style="float:right;margin-right:10px;" >
                    
                <input type="checkbox" name="BuildingNameNumberChk1" id="BuildingNameNumber_chk1" value="1" {if $datarow.BuildingNameNumberChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="BuildingNameNumberChk2" id="BuildingNameNumber_chk2" value="1" {if $datarow.BuildingNameNumberChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="BuildingNameNumberChk3" id="BuildingNameNumber_chk3" value="1" {if $datarow.BuildingNameNumberChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
               
                </span>
                
            </p>

            <p id="jb_p_street"  >
                <label class="fieldLabel" for="jbStreet" >{$page['Labels']['street']|escape:'html'}:<sup id="StreetSup" >*</sup></label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="Street" value="{$datarow.Street|escape:'html'}" id="jbStreet" >          
            
                <span style="float:right;margin-right:10px;" >
                    
                <input type="checkbox" name="StreetChk1" id="Street_chk1" value="1" {if $datarow.StreetChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="StreetChk2" id="Street_chk2" value="1" {if $datarow.StreetChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="StreetChk3" id="Street_chk3" value="1" {if $datarow.StreetChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
               
                </span>
                
            </p>

            <p id="jb_p_area"  >
                <label class="fieldLabel" for="jbArea" >{$page['Labels']['area']|escape:'html'}:<sup id="LocalAreaSup" >*</sup></label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="LocalArea" value="{$datarow.LocalArea|escape:'html'}" id="jbArea" >          
            
                <span style="float:right;margin-right:10px;" >
                <input type="checkbox" name="LocalAreaChk1" id="LocalArea_chk1" value="1" {if $datarow.LocalAreaChk1|escape:'html'} checked="checked" {/if}  class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="LocalAreaChk2" id="LocalArea_chk2" value="1" {if $datarow.LocalAreaChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="LocalAreaChk3" id="LocalArea_chk3" value="1" {if $datarow.LocalAreaChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                </span>
                
            </p>


            <p id="jb_p_town"  >
                <label class="fieldLabel" for="jbCity" >{$page['Labels']['city']|escape:'html'}:<sup id="CitySup" >*</sup></label>
                &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="TownCity" value="{$datarow.TownCity|escape:'html'}" id="jbCity" >          
                
                <span style="float:right;margin-right:10px;" >
                <input type="checkbox" name="TownCityChk1" id="TownCity_chk1" value="1" {if $datarow.TownCityChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="TownCityChk2" id="TownCity_chk2" value="1" {if $datarow.TownCityChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="TownCityChk3" id="TownCity_chk3" value="1" {if $datarow.TownCityChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                </span>
                
            
            </p>

            <p id="jb_p_county"   >
                <label class="fieldLabel" for="jbCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:<sup id="CountyIDSup" >*</sup></label>
                &nbsp;&nbsp;  


                <select name="CountyID" id="jbCounty" class="text" >
                    <option value="" id="country_0_county_0" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $countries as $country}


                        {foreach $country.Counties as $county}
                        <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                        {/foreach}


                    {/foreach}

                </select>
                    
                <span style="float:right;margin-right:10px;" >    
                
                <input type="checkbox" name="CountyIDChk1" id="CountyID_chk1" value="1" {if $datarow.CountyIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="CountyIDChk2" id="CountyID_chk2" value="1" {if $datarow.CountyIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="CountyIDChk3" id="CountyID_chk3" value="1" {if $datarow.CountyIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
               
                 </span>
                    

            </p>
                
            <p id="jb_p_country" >
                <label class="fieldLabel" for="jbCountry" >{$page['Labels']['country']|escape:'html'}: <sup id="CountryIDSup" >*</sup> </label>
                &nbsp;&nbsp; 
                    <select name="CountryID" id="jbCountry" >
                    <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $countries as $country}

                        <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                    {/foreach}

                </select>
                    
               <span style="float:right;margin-right:10px;" >
                <input type="checkbox" name="CountryIDChk1" id="CountryID_chk1" value="1" {if $datarow.CountryIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="CountryIDChk2" id="CountryID_chk2" value="1" {if $datarow.CountryIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="CountryIDChk3" id="CountryID_chk3" value="1" {if $datarow.CountryIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
               </span>
            </p>
            
            
            <p>
            
                <br>  <hr style="height:1px" >
                
            </p>
            
            
            
            
            
            <p id="jbProductLocationElement" >
            
                <label class="fieldLabel" for="jbProductLocation" >{$page['Labels']['product_location']|escape:'html'}:<sup id="ProductLocationSup" >*</sup></label> &nbsp;&nbsp; 

                <select name="ProductLocation" id="jbProductLocation" class="text"  >

                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                    {foreach $product_location_rows as $plocation}
                        {if $plocation.ProductLocation eq $datarow.ProductLocation}
                            <option value="{$plocation.ProductLocation}" selected="selected"  >{$plocation.Name|escape:'html'}</option>
                        {else}
                            <option value="{$plocation.ProductLocation}" >{$plocation.Name|escape:'html'}</option>
                        {/if}
                    {/foreach}

                </select>

                <span style="float:right;margin-right:10px;" >
                    <input type="checkbox" name="ProductLocationChk1" id="ProductLocation_chk1" value="1" {if $datarow.ProductLocationChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ProductLocationChk2" id="ProductLocation_chk2" value="1" {if $datarow.ProductLocationChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ProductLocationChk3" id="ProductLocation_chk3" value="1" {if $datarow.ProductLocationChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                </span>
            </p>


                <p id="jbColBranchAddressIDElement" title="{$page['Text']['collect_from_hint']|escape:'html'}" >
                    <label class="fieldLabel" for="jbColBranchAddressID" >{$page['Labels']['collect_from']|escape:'html'}:<sup id="ColBranchAddressIDSup" >*</sup></label>
                    &nbsp;&nbsp; <select name="ColBranchAddressID" id="jbColBranchAddressID" class="text"  >

                        <option value="" selected="selected"  >{$page['Text']['pseudo_branch']|escape:'html'}</option>
                        <option value="-1"  >{$page['Text']['pseudo_branch']|escape:'html'}</option>
                        
                            {if $showBranchDropDown}
                           
                                 {foreach $branches as $branch}
                                     <option value="{$branch.BranchID}"  {if $datarow.ColBranchAddressID eq $branch.BranchID} selected="selected" {/if} >{$branch.BranchName|escape:'html'}</option>
                                 {/foreach}
                                 
                            {else}
                                
                                 <option value="{$datarow.BranchID|escape:'html'}"  {if $datarow.ColBranchAddressID eq $datarow.BranchID} selected="selected" {/if} >{$BranchName|escape:'html'}</option>
                                 
                            {/if}

                     </select>
                        
                    <span style="float:right;margin-right:10px;" > 
                    <input type="checkbox" name="ColBranchAddressIDChk1" id="ColBranchAddressID_chk1" value="1" {if $datarow.ColBranchAddressIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColBranchAddressIDChk2" id="ColBranchAddressID_chk2" value="1" {if $datarow.ColBranchAddressIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColBranchAddressIDChk3" id="ColBranchAddressID_chk3" value="1" {if $datarow.ColBranchAddressIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
    
                 </p>

                

                <p id="isProductAtBranchP" style="margin:5px 0px 10px 0px;">
                    <label class="fieldLabel" >{$page['Labels']['is_product_at_branch']|escape:'html'}<sup id="isProductAtBranchSup" >*</sup></label>
                    &nbsp;&nbsp; 
                    <input type="radio" name="isProductAtBranch" id="isProductAtBranchYes" value="Yes" /> {$page['Labels']['yes_text']|escape:'html'}
                    <input type="radio" name="isProductAtBranch" id="isProductAtBranchNo" value="No" /> {$page['Labels']['not_at_address']|escape:'html'}
                    
                    
                    <span style="float:right;margin-right:10px;" > 
                    <input type="checkbox" name="isProductAtBranchChk1" id="isProductAtBranch_chk1" value="1" {if $datarow.isProductAtBranchChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="isProductAtBranchChk2" id="isProductAtBranch_chk2" value="1" {if $datarow.isProductAtBranchChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="isProductAtBranchChk3" id="isProductAtBranch_chk3" value="1" {if $datarow.isProductAtBranchChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                </p>


                <p id="jbColAddPostalCodeElement" >
                    <label class="fieldLabel" for="jbColAddPostalCode" id="jbColAddPostalCodeLabel" >
                        {$page['Labels']['collection_postcode']|escape:'html'}:<sup id="ColAddPostcodeSup" >*</sup>
                    </label>
                    &nbsp;&nbsp; 
                    <input class="text uppercaseText" style="width:230px;" type="text" name="ColAddPostcode" id="jbColAddPostalCode" value="{$datarow.ColAddPostcode|escape:'html'}" />&nbsp;
                    <input type="button" name="quick_find_btn2" id="quick_find_btn2" style="position: relative; top:1px;" class="textSubmitButton postCodeLookUpBtn"  onclick="addressLookup2();return false;"   value="Click to find Address" >
                    &nbsp;&nbsp;
                    <span id="fetch" style="display: none" class="blueText" >
                        <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >
                    </span>
                    
                    <span style="float:right;margin-right:10px;" > 
                        
                    <input type="checkbox" name="ColAddPostcodeChk1" id="ColAddPostcode_chk1" value="1" {if $datarow.ColAddPostcodeChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddPostcodeChk2" id="ColAddPostcode_chk2" value="1" {if $datarow.ColAddPostcodeChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddPostcodeChk3" id="ColAddPostcode_chk3" value="1" {if $datarow.ColAddPostcodeChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    
                    </span>
               </p>


               <p id="selectColAddOutput" ></p>


               <p  id="jbColAddCompanyNameElement"  >
                   <label class="fieldLabel" for="jbColAddCompanyName" id="jbColAddCompanyNameLabel">
                       {$page['Labels']['customer_name']|escape:'html'}:<sup id="ColAddCompanyNameSup" >*</sup>
                   </label>
                   
                    &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddCompanyName" value="{$datarow.ColAddCompanyName|escape:'html'}"  id="jbColAddCompanyName" >

                    <span style="float:right;margin-right:10px;" > 
                        
                    <input type="checkbox" name="ColAddCompanyNameChk1" id="ColAddCompanyName_chk1" value="1" {if $datarow.ColAddCompanyNameChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddCompanyNameChk2" id="ColAddCompanyName_chk2" value="1" {if $datarow.ColAddCompanyNameChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddCompanyNameChk3" id="ColAddCompanyName_chk3" value="1" {if $datarow.ColAddCompanyNameChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
             
                    </span>
               </p> 


               <p id="jb_p_c_a_house_name"   >
                   <label class="fieldLabel" for="jbColAddBuildingName" >{$page['Labels']['building_name']|escape:'html'}:<sup id="ColAddBuildingNameNumberSup" >*</sup></label>
                   &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="ColAddBuildingNameNumber" value="{$datarow.ColAddBuildingNameNumber|escape:'html'}" id="jbColAddBuildingName" >          
                   
                    <span style="float:right;margin-right:10px;" >    
                    
                    <input type="checkbox" name="ColAddBuildingNameNumberChk1" id="ColAddBuildingNameNumber_chk1" value="1" {if $datarow.ColAddBuildingNameNumberChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddBuildingNameNumberChk2" id="ColAddBuildingNameNumber_chk2" value="1" {if $datarow.ColAddBuildingNameNumberChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddBuildingNameNumberChk3" id="ColAddBuildingNameNumber_chk3" value="1" {if $datarow.ColAddBuildingNameNumberChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    
                    </span>
               
               </p>

               <p id="jb_p_c_a_street"  >
                   <label class="fieldLabel" for="jbColAddStreet" >{$page['Labels']['street']|escape:'html'}:<sup id="ColAddStreetSup" >*</sup></label>
                   &nbsp;&nbsp; <input type="text" class="text capitalizeText"  name="ColAddStreet" value="{$datarow.ColAddStreet|escape:'html'}" id="jbColAddStreet" >          
                   
                   
                    <span style="float:right;margin-right:10px;" > 
                    <input type="checkbox" name="ColAddStreetChk1" id="ColAddStreet_chk1" value="1" {if $datarow.ColAddStreetChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddStreetChk2" id="ColAddStreet_chk2" value="1" {if $datarow.ColAddStreetChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddStreetChk3" id="ColAddStreet_chk3" value="1" {if $datarow.ColAddStreetChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
               </p>

               <p id="jb_p_c_a_area" >
                   <label class="fieldLabel" for="jbColAddArea"  >{$page['Labels']['area']|escape:'html'}:<sup id="ColAddLocalAreaSup" >*</sup></label>
                   &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddLocalArea" value="{$datarow.ColAddLocalArea|escape:'html'}" id="jbColAddArea" >          
                   
                    <span style="float:right;margin-right:10px;" > 
                        
                    <input type="checkbox" name="ColAddLocalAreaChk1" id="ColAddLocalArea_chk1" value="1" {if $datarow.ColAddLocalAreaChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddLocalAreaChk2" id="ColAddLocalArea_chk2" value="1" {if $datarow.ColAddLocalAreaChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddLocalAreaChk3" id="ColAddLocalArea_chk3" value="1" {if $datarow.ColAddLocalAreaChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
             
                    </span>
               </p>


               <p id="jb_p_c_a_town"  >
                   <label class="fieldLabel" for="jbColAddTownCity" >{$page['Labels']['city']|escape:'html'}:<sup id="ColAddTownCitySup" >*</sup></label>
                   &nbsp;&nbsp; <input type="text" class="text capitalizeText" name="ColAddTownCity" value="{$datarow.ColAddTownCity|escape:'html'}" id="jbColAddTownCity" >          
                   
                    <span style="float:right;margin-right:10px;" > 
                        
                    <input type="checkbox" name="ColAddTownCityChk1" id="ColAddTownCity_chk1" value="1" {if $datarow.ColAddTownCityChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddTownCityChk2" id="ColAddTownCity_chk2" value="1" {if $datarow.ColAddTownCityChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddTownCityChk3" id="ColAddTownCity_chk3" value="1" {if $datarow.ColAddTownCityChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    
                    </span>
                   
               </p>

               <p id="jb_p_c_a_county" >
                   <label class="fieldLabel" for="jbColAddCounty" >{$page['Labels']['county_geographic_area']|escape:'html'}:<sup id="ColAddCountyIDSup" >*</sup></label>
                   &nbsp;&nbsp;  


                   <select name="ColAddCountyID" id="jbColAddCounty" class="text" >
                       <option value="" id="cacountry_0_county_0" {if $datarow.ColAddCountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                       {foreach $countries as $country}


                           {foreach $country.Counties as $county}
                           <option value="{$county.CountyID}"  id="cacountry_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.ColAddCountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                           {/foreach}


                       {/foreach}

                   </select>
                       
                    <span style="float:right;margin-right:10px;" >    
                        <input type="checkbox" name="ColAddCountyIDChk1" id="ColAddCountyID_chk1" value="1" {if $datarow.ColAddCountyIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                        <input type="checkbox" name="ColAddCountyIDChk2" id="ColAddCountyID_chk2" value="1" {if $datarow.ColAddCountyIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                        <input type="checkbox" name="ColAddCountyIDChk3" id="ColAddCountyID_chk3" value="1" {if $datarow.ColAddCountyIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>

               </p>

               <p id="jb_p_c_a_country" >
                   <label class="fieldLabel" for="jbColAddCountry" >{$page['Labels']['country']|escape:'html'}: <sup id="ColAddCountryIDSup" >*</sup></label>
                   &nbsp;&nbsp; 
                       <select name="ColAddCountryID" id="jbColAddCountry" >
                       <option value="" {if $datarow.ColAddCountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                       {foreach $countries as $country}

                           <option value="{$country.CountryID}" {if $datarow.ColAddCountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                       {/foreach}

                   </select>
                       
                    <span style="float:right;margin-right:10px;" >   
                        <input type="checkbox" name="ColAddCountryIDChk1" id="ColAddCountryID_chk1" value="1" {if $datarow.ColAddCountryIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                        <input type="checkbox" name="ColAddCountryIDChk2" id="ColAddCountryID_chk2" value="1" {if $datarow.ColAddCountryIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                        <input type="checkbox" name="ColAddCountryIDChk3" id="ColAddCountryID_chk3" value="1" {if $datarow.ColAddCountryIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
               </p> 


               <p id="jb_p_c_a_email"  >
                   <label class="fieldLabel" for="jbColAddEmail" >{$page['Labels']['col_email']|escape:'html'}: <sup id="ColAddEmailSup" >*</sup></label>
                   &nbsp;&nbsp; <input type="text" class="text" name="ColAddEmail" value="{$datarow.ColAddEmail|escape:'html'}" id="jbColAddEmail" >          
                    
                    <span style="float:right;margin-right:10px;" >   
                    <input type="checkbox" name="ColAddEmailChk1" id="ColAddEmail_chk1" value="1" {if $datarow.ColAddEmailChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddEmailChk2" id="ColAddEmail_chk2" value="1" {if $datarow.ColAddEmailChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ColAddEmailChk3" id="ColAddEmail_chk3" value="1" {if $datarow.ColAddEmailChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                     </span>
                   
               </p>



               <p id="jb_p_c_a_phone" >
                <label class="fieldLabel" for="jbColAddPhone" >{$page['Labels']['col_phone']|escape:'html'}:<sup id="ColAddPhoneSup" >*</sup></label>
                &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ColAddPhone" value="{$datarow.ColAddPhone|escape:'html'}" id="jbColAddPhone" >
                <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ColAddPhoneExt" value="{$datarow.ColAddPhoneExt|escape:'html'}" id="ColAddPhoneExt" >

                
               <span style="float:right;margin-right:10px;" >  
                <input type="checkbox" name="ColAddPhoneChk1" id="ColAddPhone_chk1" value="1" {if $datarow.ColAddPhoneChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                <input type="checkbox" name="ColAddPhoneChk2" id="ColAddPhone_chk2" value="1" {if $datarow.ColAddPhoneChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                <input type="checkbox" name="ColAddPhoneChk3" id="ColAddPhone_chk3" value="1" {if $datarow.ColAddPhoneChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
               </span>   
                
               </p>


               <p>
                    <hr style="height:1px" >
               </p>

               

                <p id="jbStockCodeElement" >
                    <label class="fieldLabel" for="jb_item_catalogue_no" >{$page['Labels']['stock_code']|escape:'html'}:<sup id="StockCodeSup" >*</sup></label>
                    &nbsp;&nbsp; 

                    
                        <select name="StockCode" id="jbStockCode" class="text" >

                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $stockcode_rows as $stc}

                                    {if $stc.ProductNo eq $datarow.StockCode}
                                    <option value="{$stc.ProductNo};;{$stc.ModelNumber};;{$stc.ModelDescription};;{$stc.UnitTypeID};;{$stc.ManufacturerID}" selected="selected">{$stc.ProductNo|escape:'html'}</option>
                                    {else}
                                     <option value="{$stc.ProductNo};;{$stc.ModelNumber};;{$stc.ModelDescription};;{$stc.UnitTypeID};;{$stc.ManufacturerID}" >{$stc.ProductNo|escape:'html'}</option>
                                    {/if}

                                    {/foreach}
                        </select>
                        
                       <span style="float:right;margin-right:10px;" > 
                        <a id="JobSettingsStockCodeHelp" class="helpTextIconQtip" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" >{$page['Text']['help']|escape:'html'}</a>
                        &nbsp;&nbsp;
                        <input type="checkbox" name="StockCodeChk1" id="StockCode_chk1" value="1" {if $datarow.StockCodeChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                        <input type="checkbox" name="StockCodeChk2" id="StockCode_chk2" value="1" {if $datarow.StockCodeChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                        <input type="checkbox" name="StockCodeChk3" id="StockCode_chk3" value="1" {if $datarow.StockCodeChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                       </span> 
                       
                       
                       

                </p>


               <p>

                    <label class="fieldLabel" >&nbsp;</label>
                   &nbsp;&nbsp; 

                   <input  type="radio" name="LocateBy"  value="1" {if $datarow.LocateBy eq '1'} checked="checked" {/if}  /> <span class="text auto-hint" >{$page['Labels']['locate_by_model']|escape:'html'}</span> 
                   <input  type="radio" name="LocateBy"  value="2" {if $datarow.LocateBy eq '2'} checked="checked" {/if}  /> <span class="text" >{$page['Text']['free_product_description']|escape:'html'}</span> 
                   
               </p>




                <p id="jbModelNameRow" >
                    
                    <label class="fieldLabel" for="jbModelName" >{$page['Labels']['model_no']|escape:'html'}:<sup id="ModelNameSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" name="ModelName" class="text auto-hint" id="jbModelName" value="{$datarow.ModelName|escape:'html'}" title="{$page['Text']['model_number_title']|escape:'html'}" >
                  
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="ModelNameChk1" id="ModelName_chk1" value="1" {if $datarow.ModelNameChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ModelNameChk2" id="ModelName_chk2" value="1" {if $datarow.ModelNameChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ModelNameChk3" id="ModelName_chk3" value="1" {if $datarow.ModelNameChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                    
                </p>

                <p id="jbProductDescriptionRow" >
                    <label class="fieldLabel" for="jbProductDescription" >{$page['Labels']['product_description']|escape:'html'}:<sup id="ProductDescriptionSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" name="ProductDescription" class="text auto-hint" id="jbProductDescription" value="{$datarow.ProductDescription|escape:'html'}" title="{$page['Text']['text_description_title']|escape:'html'}" >

                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="ProductDescriptionChk1" id="ProductDescription_chk1" value="1" {if $datarow.ProductDescriptionChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ProductDescriptionChk2" id="ProductDescription_chk2" value="1" {if $datarow.ProductDescriptionChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ProductDescriptionChk3" id="ProductDescription_chk3" value="1" {if $datarow.ProductDescriptionChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                    
                </p>


                <p id="jbManufacturerIDElement" >
                    
                    <label class="fieldLabel" for="jbManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup id="ManufacturerIDSup" >*</sup></label>
                    &nbsp;&nbsp;  <select name="ManufacturerID" id="jbManufacturerID" class="text" >

                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $mfg_rows as $mfg}

                                    {if $mfg.ManufacturerID eq $datarow.ManufacturerID}
                                    <option value="{$mfg.ManufacturerID}" selected="selected">{$mfg.ManufacturerName|escape:'html'}</option>
                                    {else}
                                     <option value="{$mfg.ManufacturerID}" >{$mfg.ManufacturerName|escape:'html'}</option>
                                     {/if}

                                    {/foreach}
                     </select>
                     
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="ManufacturerIDChk1" id="ManufacturerID_chk1" value="1" {if $datarow.ManufacturerIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ManufacturerIDChk2" id="ManufacturerID_chk2" value="1" {if $datarow.ManufacturerIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ManufacturerIDChk3" id="ManufacturerID_chk3" value="1" {if $datarow.ManufacturerIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                     
                </p>

                 <p id="jbUnitTypeIDElement" >
                    <label class="fieldLabel" for="jbUnitTypeID" >{$page['Labels']['product_type']|escape:'html'}:<sup id="UnitTypeIDSup" >*</sup></label>
                    &nbsp;&nbsp;           
                   
                    <select name="UnitTypeID" id="jbUnitTypeID" class="text" >

                                    <option value="" selected="selected" ></option>
                                    <option value="-1" >{$page['Text']['not_listed']|escape:'html'}</option>

                                    {foreach $product_type_rows as $ptype}

                                    {if $ptype.UnitTypeID eq $datarow.UnitTypeID}
                                    <option value="{$ptype.UnitTypeID}" selected="selected">{$ptype.UnitTypeName|escape:'html'}</option>
                                    {else}
                                     <option value="{$ptype.UnitTypeID}" >{$ptype.UnitTypeName|escape:'html'}</option>
                                     {/if}

                                    {/foreach}
                     </select>

         
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="UnitTypeIDChk1" id="UnitTypeID_chk1" value="1" {if $datarow.UnitTypeIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="UnitTypeIDChk2" id="UnitTypeID_chk2" value="1" {if $datarow.UnitTypeIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="UnitTypeIDChk3" id="UnitTypeID_chk3" value="1" {if $datarow.UnitTypeIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                     
                </p>

                
                
                <p id="ImeiNoP" >
                    <label class="fieldLabel" for="ImeiNo" >IMEI No:<sup id="ImeiNoSup" >*</sup></label>
                    &nbsp;&nbsp;           
                   
                    <input type="text" name="ImeiNo" class="text uppercaseText" id="ImeiNo" value="{$datarow.ImeiNo|escape:'html'}" />
         
                    <span style="float:right;margin-right:10px;" >  
                     
                     <a id="JobSettingsImeiNoHelp" class="helpTextIconQtip" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" >{$page['Text']['help']|escape:'html'}</a>
                        &nbsp;&nbsp;    
                        
                    <input type="checkbox" name="ImeiNoChk1" id="ImeiNo_chk1" value="1" {if $datarow.ImeiNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ImeiNoChk2" id="ImeiNo_chk2" value="1" {if $datarow.ImeiNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ImeiNoChk3" id="ImeiNo_chk3" value="1" {if $datarow.ImeiNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                     
                </p>
                
                
                

                <p id="jbSerialNoElement"  >
                    <label class="fieldLabel" for="jbSerialNo">{$page['Labels']['serial_no']|escape:'html'}:<sup id="SerialNoSup" >*</sup></label>
                    &nbsp;&nbsp;
                    <input type="text" name="SerialNo" class="text uppercaseText" id="jbSerialNo" value="{$datarow.SerialNo|escape:'html'}" />
                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="SerialNoChk1" id="SerialNo_chk1" value="1" {if $datarow.SerialNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="SerialNoChk2" id="SerialNo_chk2" value="1" {if $datarow.SerialNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="SerialNoChk3" id="SerialNo_chk3" value="1" {if $datarow.SerialNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                    
                </p>


                <p id="jbDateOfPurchaseElement"  >
                    <label class="fieldLabel" for="jb_purchase_date" >{$page['Labels']['purchase_date']|escape:'html'}:<sup id="DateOfPurchaseSup" >*</sup></label>
                    &nbsp;&nbsp;

                    {html_select_date class='selectDateClass'   prefix='DateOfPurchase' field_order='DMY' day_value_format="%02d"   time="{$datarow.DateOfPurchase|escape:'html'|default:"00-00-0000"}" all_id="DateOfPurchase" start_year='-5'   year_empty="{$page['Text']['year']|escape:'html'}" month_empty="{$page['Text']['month']|escape:'html'}" day_empty="{$page['Text']['day']|escape:'html'}"}  

                    <input type="text" class="text auto-hint" style="width:80px;" name="DateOfPurchase"  title="dd/mm/yyyy" value="{$datarow.DateOfPurchase|escape:'html'|date_format:'%d/%m/%Y'}" id="DateOfPurchase"  >
                
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="DateOfPurchaseChk1" id="DateOfPurchase_chk1" value="1" {if $datarow.DateOfPurchaseChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="DateOfPurchaseChk2" id="DateOfPurchase_chk2" value="1" {if $datarow.DateOfPurchaseChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="DateOfPurchaseChk3" id="DateOfPurchase_chk3" value="1" {if $datarow.DateOfPurchaseChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                
                </p>


                <p id="jbServiceTypeIDElement" >
                    <label class="fieldLabel" for="jbServiceTypeID" >{$page['Labels']['service_type']|escape:'html'}:<sup id="ServiceTypeIDSup" >*</sup></label>
                   &nbsp;&nbsp; <select name="ServiceTypeID" id="jbServiceTypeID" class="text" >


                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $service_type_rows as $stype}

                                    {if $stype.ServiceTypeID eq $datarow.ServiceTypeID}
                                    <option value="{$stype.ServiceTypeID}" selected="selected">{$stype.Name|escape:'html'}</option>
                                    {else}
                                     <option value="{$stype.ServiceTypeID}" >{$stype.Name|escape:'html'}</option>
                                     {/if}

                                    {/foreach}


                        </select>
                                    
                    <span style="float:right;margin-right:10px;" >
                        
                     <a id="JobSettingsServiceTypeIDHelp" class="helpTextIconQtip" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" >{$page['Text']['help']|escape:'html'}</a>
                        &nbsp;&nbsp;   
                        
                    <input type="checkbox" name="ServiceTypeIDChk1" id="ServiceTypeID_chk1" value="1" {if $datarow.ServiceTypeIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ServiceTypeIDChk2" id="ServiceTypeID_chk2" value="1" {if $datarow.ServiceTypeIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ServiceTypeIDChk3" id="ServiceTypeID_chk3" value="1" {if $datarow.ServiceTypeIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>               
                                    
                </p>

                <p>
                    <label class="fieldLabel" for="jbReportedFault" id="jbReportedFaultLabel" >
                        {$page['Labels']['reported_fault']|escape:'html'}:<sup id="ReportedFaultSup" >*</sup>
                    </label>
                    &nbsp;&nbsp; 
                    <textarea cols="20" rows="3" maxlength="2000" class="text" name="ReportedFault" id="jbReportedFault">{$datarow.ReportedFault|escape:'html'}</textarea>
                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="ReportedFaultChk1" id="ReportedFault_chk1" value="1" {if $datarow.ReportedFaultChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReportedFaultChk2" id="ReportedFault_chk2" value="1" {if $datarow.ReportedFaultChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReportedFaultChk3" id="ReportedFault_chk3" value="1" {if $datarow.ReportedFaultChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                    
                </p>






                <p id="jbAccessoriesElement"  style="margin-bottom:1px;" >
                    <label class="fieldLabel" for="jbAccessories" >{$page['Labels']['accessories']|escape:'html'}: <sup id="AccessoriesSup" >*</sup></label>
                     &nbsp;&nbsp; 


                     {$page['Text']['none_text']|escape:'html'}

                     <input  type="radio" name="AccessoriesRadio" id="jbAccessoriesRadio" value="-1" >
                     <span id="AccessoriesSpan1" ><input  type="text" maxlength="200" style="width:335px;" class="text" name="Accessories" id="jbAccessories" value="{$datarow.Accessories|escape:'html'}" ></span> 

                     <input type="hidden" id="HiddenAccessoriesFlag" name="HiddenAccessoriesFlag" value="" >
                     
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="AccessoriesChk1" id="Accessories_chk1" value="1" {if $datarow.AccessoriesChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="AccessoriesChk2" id="Accessories_chk2" value="1" {if $datarow.AccessoriesChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="AccessoriesChk3" id="Accessories_chk3" value="1" {if $datarow.AccessoriesChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                     
                </p>


                <p id="jbUnitConditionElement"  >
                    <label class="fieldLabel" for="jbUnitCondition" >
                           {$page['Labels']['unit_condition']|escape:'html'}: <sup id="UnitConditionSup" >*</sup>
                    </label>
                     &nbsp;&nbsp; 
                      <textarea cols="20" rows="2" maxlength="150" class="text" name="UnitCondition" id="jbUnitCondition" >{$datarow.UnitCondition|escape:'html'}</textarea>

                      
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="UnitConditionChk1" id="UnitCondition_chk1" value="1" {if $datarow.UnitConditionChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="UnitConditionChk2" id="UnitCondition_chk2" value="1" {if $datarow.UnitConditionChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="UnitConditionChk3" id="UnitCondition_chk3" value="1" {if $datarow.UnitConditionChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span> 
                      
                </p>




                <p >
                      <hr style="height:1px"  >
                </p>


                <p id="jbMobilePhoneNetworkIDElement"  >
                    <label class="fieldLabel" for="jbMobilePhoneNetworkID" >{$page['Labels']['mobile_phone_network']|escape:'html'}: <sup id="MobilePhoneNetworkIDSup" >*</sup></label>
                   &nbsp;&nbsp; <select name="MobilePhoneNetworkID" id="jbMobilePhoneNetworkID" class="text" >


                                    <option value="" selected="selected">{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $mobile_phone_networks as $mpn}

                                    {if $mpn.MobilePhoneNetworkID eq $datarow.MobilePhoneNetworkID}
                                    <option value="{$mpn.MobilePhoneNetworkID}" selected="selected">{$mpn.MobilePhoneNetworkName|escape:'html'}</option>
                                    {else}
                                     <option value="{$mpn.MobilePhoneNetworkID}" >{$mpn.MobilePhoneNetworkName|escape:'html'}</option>
                                     {/if}

                                    {/foreach}


                        </select>
                                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="MobilePhoneNetworkIDChk1" id="MobilePhoneNetworkID_chk1" value="1" {if $datarow.MobilePhoneNetworkIDChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="MobilePhoneNetworkIDChk2" id="MobilePhoneNetworkID_chk2" value="1" {if $datarow.MobilePhoneNetworkIDChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="MobilePhoneNetworkIDChk3" id="MobilePhoneNetworkID_chk3" value="1" {if $datarow.MobilePhoneNetworkIDChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>                 
                </p> 





                {if $datarow.OriginalRetailerFormElement neq 'Dropdown'}

                 <p id="jbOriginalRetailerElementTop"  >
                    <label class="fieldLabel" >&nbsp;</label>
                    &nbsp;&nbsp; 
                    <label class="oneThirdTextLabel" >Retailer Name</label>
                    <label class="oneThirdTextLabel" >Branch  Name</label>
                    <label class="oneThirdTextLabel" >Town</label>



                 </p>

                {/if}

                <p id="jbOriginalRetailerElement"  >


                    {if $datarow.OriginalRetailerFormElement eq 'Dropdown'}
                       <label class="fieldLabel" for="OriginalRetailerFullPart" >{$page['Labels']['original_retailer']|escape:'html'}:<sup id="OriginalRetailerSup" >*</sup></label>
                       &nbsp;&nbsp; 
                       <input  type="text" class="text capitalizeText"  title=""  name="OriginalRetailerFullPart" id="OriginalRetailerFullPart" value="{$datarow.OriginalRetailerPart1|escape:'html'}" maxlength="100" >
                       <a href="#" id="deleteOriginalRetailer"><img src="{$_subdomain}/images/cross-icon.png" /></a>

                    {else}
                        <label class="fieldLabel" for="OriginalRetailerPart1" >{$page['Labels']['original_retailer']|escape:'html'}:<sup id="OriginalRetailerSup" >*</sup></label>
                        &nbsp;&nbsp; 
                        <input  type="text" class="text capitalizeText" style="width:116px" title=""  name="OriginalRetailerPart1" id="OriginalRetailerPart1" value="{$datarow.OriginalRetailerPart1|escape:'html'}" maxlength="25" >
                        &nbsp;<input  type="text" class="text capitalizeText" style="width:116px" title=""  name="OriginalRetailerPart2" id="OriginalRetailerPart2" value="{$datarow.OriginalRetailerPart2|escape:'html'}" maxlength="25" >
                        &nbsp;<input  type="text" class="text capitalizeText" style="width:118px" title=""  name="OriginalRetailerPart3" id="OriginalRetailerPart3" value="{$datarow.OriginalRetailerPart3|escape:'html'}" maxlength="25" >
                        {* <a href="#" id="clearORFields"><img src="{$_subdomain}/images/cross-icon.png" /></a> *}

                    {/if}
                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="OriginalRetailerChk1" id="OriginalRetailer_chk1" value="1" {if $datarow.OriginalRetailerChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="OriginalRetailerChk2" id="OriginalRetailer_chk2" value="1" {if $datarow.OriginalRetailerChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="OriginalRetailerChk3" id="OriginalRetailer_chk3" value="1" {if $datarow.OriginalRetailerChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                </p>



                <p  >
                    <label class="fieldLabel" for="jbReceiptNo" id="jbReceiptNoLabel" >{$page['Labels']['sales_receipt_no']|escape:'html'}:<sup id="ReceiptNoSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="ReceiptNo" id="jbReceiptNo" value="{$datarow.ReceiptNo|escape:'html'}" >
                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="ReceiptNoChk1" id="ReceiptNo_chk1" value="1" {if $datarow.ReceiptNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReceiptNoChk2" id="ReceiptNo_chk2" value="1" {if $datarow.ReceiptNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReceiptNoChk3" id="ReceiptNo_chk3" value="1" {if $datarow.ReceiptNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                </p>


                <p id="jbInsurerElement"  >
                    <label class="fieldLabel" for="jbInsurer" >{$page['Labels']['insurer']|escape:'html'}:<sup id="InsurerSup" >*</sup></label>
                     &nbsp;&nbsp; <input  type="text" class="text" name="Insurer" id="jbInsurer" value="{$datarow.Insurer|escape:'html'}" >
                     
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="InsurerChk1" id="Insurer_chk1" value="1" {if $datarow.InsurerChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="InsurerChk2" id="Insurer_chk2" value="1" {if $datarow.InsurerChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="InsurerChk3" id="Insurer_chk3" value="1" {if $datarow.InsurerChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>

                </p>


                 <p id="jbPolicyNoElement"  >
                   <label class="fieldLabel" for="jbPolicyNo" >{$page['Labels']['policy_no']|escape:'html'}:<sup id="PolicyNoSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="PolicyNo" id="jbPolicyNo" value="{$datarow.PolicyNo|escape:'html'}" >
                    
                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="PolicyNoChk1" id="PolicyNo_chk1" value="1" {if $datarow.PolicyNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="PolicyNoChk2" id="PolicyNo_chk2" value="1" {if $datarow.PolicyNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="PolicyNoChk3" id="PolicyNo_chk3" value="1" {if $datarow.PolicyNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>
                    
                 </p>


                 <p id="jbAuthorisationNoElement"  >
                    <label class="fieldLabel" for="jbAuthorisationNo" >{$page['Labels']['authorisation_no']|escape:'html'}:<sup id="AuthorisationNoSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="AuthorisationNo"  value="{$datarow.AuthorisationNo|escape:'html'}" id="jbAuthorisationNo" >
                    
                    <span style="float:right;margin-right:10px;" >  
                        
                    <a id="JobSettingsAuthorisationNoHelp" class="helpTextIconQtip"  title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" >{$page['Text']['help']|escape:'html'}</a>
                        &nbsp;&nbsp;
                        
                    <input type="checkbox" name="AuthorisationNoChk1" id="AuthorisationNo_chk1" value="1" {if $datarow.AuthorisationNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="AuthorisationNoChk2" id="AuthorisationNo_chk2" value="1" {if $datarow.AuthorisationNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="AuthorisationNoChk3" id="AuthorisationNo_chk3" value="1" {if $datarow.AuthorisationNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>

                </p>

                <p id="jbNotesElement"  >
                    <label class="fieldLabel" for="jbNotes" >{$page['Labels']['additional_notes']|escape:'html'}:<sup id="NotesSup" >*</sup></label>
                    &nbsp;&nbsp; <textarea  cols="20" rows="3" class="text" name="Notes" id="jbNotes" >{$datarow.Notes|escape:'html'}</textarea>&nbsp;&nbsp;

                    <span style="float:right;margin-right:10px;" >  
                    <input type="checkbox" name="NotesChk1" id="Notes_chk1" value="1" {if $datarow.NotesChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="NotesChk2" id="Notes_chk2" value="1" {if $datarow.NotesChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="NotesChk3" id="Notes_chk3" value="1" {if $datarow.NotesChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>


                </p>
                <p  id="jbReferralNoElement"  >
                    <label class="fieldLabel" for="jbReferralNo" >{$page['Labels']['referral_no']|escape:'html'}: <sup id="ReferralNoSup" >*</sup></label>
                    &nbsp;&nbsp; <input  type="text" class="text uppercaseText" name="ReferralNo" id="jbReferralNo" value="{$datarow.ReferralNo|escape:'html'}" >
                    
                    <span style="float:right;margin-right:10px;" >  
                        
                    <a id="JobSettingsReferralNoHelp" class="helpTextIconQtip" title="{$page['Text']['help_info']|escape:'html'}" alt="{$page['Text']['help_info']|escape:'html'}" >{$page['Text']['help']|escape:'html'}</a>
                        &nbsp;&nbsp;       
                        
                    <input type="checkbox" name="ReferralNoChk1" id="ReferralNo_chk1" value="1" {if $datarow.ReferralNoChk1|escape:'html'} checked="checked" {/if} class="rightCheckboxC1" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReferralNoChk2" id="ReferralNo_chk2" value="1" {if $datarow.ReferralNoChk2|escape:'html'} checked="checked" {/if} class="rightCheckboxC2" >&nbsp;&nbsp;
                    <input type="checkbox" name="ReferralNoChk3" id="ReferralNo_chk3" value="1" {if $datarow.ReferralNoChk3|escape:'html'} checked="checked" {/if} class="rightCheckboxC3" >&nbsp;&nbsp;
                    </span>

                </p>
                
                
                {if $datarow.JobSettingsID neq ''}
                    
                    <p>
                        <label class="fieldLabel" for="StatusActive" >{$page['Labels']['status']|escape:'html'}: <sup>*</sup></label>
                        &nbsp;&nbsp; <input  type="radio"  name="Status" value="Active" {if $datarow.Status eq 'Active'} checked="checked" {/if} > {$page['Text']['active']|escape:'html'}
                        <input  type="radio"  name="Status" value="In-Active" {if $datarow.Status eq 'In-Active'} checked="checked" {/if} > {$page['Text']['inactive']|escape:'html'}
                    </p>
                    
                {else}
                   <input  type="hidden"  name="Status" value="Active"  >
                {/if}    
                
                
            
                <p style="padding-top:6px;" >
                 
                 <label>&nbsp;</label>&nbsp;&nbsp;&nbsp;     
                 
                 
                 <input type="hidden" name="JobSettingsID"  id="JobSettingsID"  value="{$datarow.JobSettingsID|escape:'html'}" >
                 
                 <input type="submit" name="saveRecord"  id="saveRecord" class="btnStandard"  value="{$page['Buttons']['save']|escape:'html'}" >
                 
                  &nbsp;&nbsp;
                  <input type="submit" name="cancelChanges" id="cancelChanges"   class="btnCancel"   value="{$page['Buttons']['cancel_changes']|escape:'html'}" > 

                  <span id="processDisplayText" style="color:red;display:none;" ><img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >&nbsp;&nbsp;{$page['Text']['process_record']|escape:'html'}</span>    
                           
            
                 </p>
                 
            </fieldset> 

        
        </form>
    </div>   
        
                      
    
                                    
</div>
{/block}
