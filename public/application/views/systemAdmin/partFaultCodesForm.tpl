 <script type="text/javascript">   
     
 $(document).ready(function() {
 $(function() {
    $( "#tabs" ).tabs({
    show: function(event, ui) {
        $.colorbox.resize();
    }
});
  });
    if($('#RestrictLength').attr('checked')=="checked"){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide() ;$.colorbox.resize();}
    if($('#CopyFromJobFaultCode').attr('checked')=="checked"){ $('#addFieldsDop2').show();$.colorbox.resize();}else{ $('#addFieldsDop2').hide();$.colorbox.resize(); }
    if($('#ReqOnlyForRepairType').attr('checked')=="checked"){ $('#addFieldsDop3').show();$.colorbox.resize();}else{ $('#addFieldsDop3').hide();$.colorbox.resize(); }
    
 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });  
    
    // validation
    
      $("#PartFaultCodeForm").validate = null;
	
	$("#PartFaultCodeForm").validate({
	    rules: {
                       
                        FieldName:{
                             required: function (element) {
                                    }    
                                  },
                        Manufacturer:{
                             required: function (element) {
                                   
                                     }
                                   },
                        FieldNumber:{
                             required: function (element) {
                                   
                                      }
                                   },
                           JobFaultCodeNo:
                                                    {
                                                        required: function (element) {
                                                            if($("#CopyFromJobFaultCode").is(':checked'))
                                                            {
                                                            return true;
                                                            }
                                                            else
                                                            {
                                                              return false;
                                                            }
                                                        },
                                                        digits: true
                                                    },
                           LengthFrom:
                                                    {
                                                        required: function (element) {
                                                            if($("#RestrictLength").is(':checked'))
                                                            {
                                                              return true;
                                                            }
                                                            else
                                                            {
                                                               return false;
                                                            }
                                                        },
                                                        digits: true
                                                    },
                           LengthTo:
                                                    {
                                                        required: function (element) {
                                                            if($("#RestrictLength").is(':checked'))
                                                            {
                                                            // $("#JobFaultCodeNo").fadeOut();
                                                                return true;
                                                            }
                                                            else
                                                            {
                                                            // $("#JobFaultCodeNo").fadeIn();  
                                                              return false;
                                                            }
                                                        },
                                                        digits: true
                                                    }
                           
                        

                    },
	   
 
	    errorPlacement: function(error, element) {
		error.appendTo(element.parent("p"));
                
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "label"
	   
	});
    
    
   }); 
    
  </script>  
    
    <div id="PartFaultCodeFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartFaultCodeForm" name="PartFaultCodeForm" method="post"  action="{$_subdomain}/LookupTables/savePartFaultCode" class="inline" >
                    <input type="hidden" name="PartFaultCodeID" value="{$datarow.PartFaultCodeID|default:""}">
                <fieldset>
                    <legend title="" >Part Fault Code</legend>
                        
                 
                            
                        
                        
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Default Settings</a></li>
    <li><a href="#tabs-2">Format Settings</a></li>
   {* <li><a href="#tabs-3">Action Settings</a></li>
    <li><a href="#tabs-4">Compulsory Settings</a></li> *}
    <li><a {if $datarow.PartFaultCodeID|default:''!=''}onclick="loadLinkedItem()" {/if} href="#tabs-3">Lookup Settings</a></li>
   
    
  </ul>
          <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
          <div id="tabs-1" class="SystemAdminFormPanel inline">
              
          <p>
                                <label class="cardLabel" for="FieldName" >Field Name:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="FieldName" value="{$datarow.FieldName|default:''}" id="FieldName" >
          </p>
          
          <p>
                            <label class="cardLabel" for="Manufacturer" >Manufacturer:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                                <select name="ManufacturerID" id="Manufacturer" class="text" >
                                <option value="" {if $datarow.ManufacturerID|default:"" eq ''}selected="selected"{/if}>Select from drop down</option>

                                
                                {foreach $manufacturerList|default:"" as $s}

                                    <option value="{$s.ManufacturerID|default:""}" {if $datarow.Manufacturer|default:"" eq $s.ManufacturerID|default:""} selected="selected"{/if}>{$s.ManufacturerName|default:""}</option>

                                {/foreach}
                                
                            </select>
         </p>
          
           <p>
                                <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
                                    
				<input  type="checkbox" name="Status"  value="In-active" {if $datarow.Status|default:'' eq 'In-active'}checked="checked"{/if}  /> Inactive &nbsp;
				 <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_Status_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >	
                                </span>

                                    
          </p>
          <p>
                                <label class="cardLabel" for="FieldNo" >Field Number:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input style="width:142px"  type="text" class="text"  name="FieldNumber" value="{$datarow.FieldNumber|default:''}" id="FieldNo" >
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FieldNo_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
          </p>
          
          <p>
                                <label class="cardLabel" for="NonFaultCode" >Non Fault Code:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="NonFaultCode"  value="Yes" {if $datarow.NonFaultCode|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_nofaultcode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p>
          
           <p>
                                <label class="cardLabel" for="MainOutFault" >Main Out Fault:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="MainOutFault"  value="Yes" {if $datarow.MainOutFault|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_mainoutfault_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p>
          
          <p>
                                <label class="cardLabel" for="KeyRepair" >Key Repair:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="KeyRepair"  value="Yes" {if $datarow.KeyRepair|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_keyrepair_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>                                    
          </p>
          
          <p>
                                <label class="cardLabel" for="CompulsoryAtCompletion" >Compulsory At Completion:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="CompulsoryAtCompletion"  value="Yes" {if $datarow.CompulsoryAtCompletion|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_compulsoryatcompletion_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>                                    
          </p>
          
          <p>
                                <label class="cardLabel" for="CompulsoryForAdjustment" >Compulsory For Adjustment:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="CompulsoryForAdjustment"  value="Yes" {if $datarow.CompulsoryForAdjustment|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_compulsoryforadjustment_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>                                    
          </p>
          
          
          
          <p>
                            <label class="cardLabel" for="FieldType" >Field Type:</label>
                            &nbsp; <input {if $datarow.FieldType|default:"Generic String"=="Generic String"}checked=checked{/if}  type="radio"  name="FieldType" value="Generic String" id="FieldType" >    Generic String 
                            &nbsp; <input {if $datarow.FieldType|default:"Generic String"=="Date"}checked=checked{/if}  type="radio"  name="FieldType" value="Date" id="FieldType" > Date         
                            &nbsp; <input {if $datarow.FieldType|default:"Generic String"=="Number Only"}checked=checked{/if}  type="radio"  name="FieldType" value="Number Only" id="FieldType" > Number Only         
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FieldType_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
          </p>
      {*    <p>
                                <label class="cardLabel" for="Lookup" >Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="Lookup"  value="Yes" {if $datarow.Lookup|default:'Yes' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_Lookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
          </p>
          <p>
                                <label class="cardLabel" for="MainInFault" >Main In Fault:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="MainInFault"  value="Yes" {if $datarow.MainInFault|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_maininfault_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
          </p>


          <p>
                                <label class="cardLabel" for="UseAsGenericDescription" >Use As Generic Description:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="UseAsGenericDescription"  value="Yes" {if $datarow.UseAsGenericDescription|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_useasgenericdescription_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
          </p>  *}
          </div>
          
    <div id="tabs-2" class="SystemAdminFormPanel inline">
        
        <p>
                                <label class="cardLabel" for="Format" >Format:</label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="Format" value="{$datarow.Format|default:''}" id="Format" >
        </p>
        
        <p>
                                <label class="cardLabel" for="ForceFormat" >Force Format:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input type="checkbox" name="ForceFormat" id="ForceFormat" value="Yes" {if $datarow.ForceFormat|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ForceFormat_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
        </p>
       {*   <div id="addFieldsDop2" style="display:none">
        <p>
                                <label class="cardLabel" for="Format" >Required Format:</label>
                                &nbsp;&nbsp;
                                <input  style="width:142px"  type="text" class="text"  name="Format" value="{$datarow.Format|default:''}" id="Format" >
                                
        </p>
        </div> *}
                                
                                
        <p>
                                <label class="cardLabel" for="CopyFromJobFaultCode" >Copy From Job Fault Code:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input onclick="if(this.checked==true){ $('#addFieldsDop2').show();$.colorbox.resize();}else{ $('#addFieldsDop2').hide();$.colorbox.resize(); }"  type="checkbox" name="CopyFromJobFaultCode" id="CopyFromJobFaultCode" value="Yes" {if $datarow.CopyFromJobFaultCode|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_CopyFromJobFaultCode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
        </p>
          <div id="addFieldsDop2" style="display:none">
        <p>
                                <label class="cardLabel" for="JobFaultCodeNo" >Job Fault Code No:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                {* <input  style="width:142px"  type="numeric" class="text"  name="JobFaultCodeNo" value="{$datarow.JobFaultCodeNo|default:''}" id="JobFaultCodeNo" > *}
                                
                                <select  name="JobFaultCodeNo" id="JobFaultCodeNo" > 
                                    
                                {for $i=1 to 20}                                    
                                    <option value="{$i}" {if isset($datarow.JobFaultCodeNo) && $datarow.JobFaultCodeNo==$i} selected {/if} >{$i}</option>                               
                                 {/for}
                             </select> 
                                
        </p>
        </div>
                                
                                

        <p>
                                <label class="cardLabel" for="RestrictLength" >Restrict Lenght:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input onclick="if(this.checked==true){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide();$.colorbox.resize(); }"  type="checkbox" name="RestrictLength" id="RestrictLength"  value="Yes" {if $datarow.RestrictLength|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_RestrictLength_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
        </p>
        <div id="addFieldsDop" style="display:none">
        <p>
                                <label class="cardLabel" for="LengthFrom" >Length - From:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input style="width:142px"  type="text" class="text"  name="LengthFrom" value="{$datarow.LengthFrom|default:''}" id="LengthFrom" >
                                
        </p>
        <p>
                                <label class="cardLabel" for="LengthTo" >Length - To:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input style="width:142px"  type="text" class="text"  name="LengthTo" value="{$datarow.LengthTo|default:''}" id="LengthTo" >
                                
        </p>
        </div>
                                
         <p>
                                <label class="cardLabel" for="FillwithNAForAccessory" >Fill with "N/A" For Accessory:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="FillwithNAForAccessory"  value="Yes" {if $datarow.FillwithNAForAccessory|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FillwithNAForAccessory_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p> 
          
         <p>
                                <label class="cardLabel" for="FillwithNAForSoftwareUpgrade" >Fill with "N/A" For Software Upgrade:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="FillwithNAForSoftwareUpgrade"  value="Yes" {if $datarow.FillwithNAForSoftwareUpgrade|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FillwithNAForSoftwareUpgrade_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p>
          
        {* <p>
                                <label class="cardLabel" for="UseLookup" >Use Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="UseLookup"  value="Yes" {if $datarow.UseLookup|default:'' eq 'Yes'}checked="checked"{/if} checked="checked" />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_UseLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p>
          
         <p>
                                <label class="cardLabel" for="ForceLookup" >Force Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="ForceLookup"  value="Yes" {if $datarow.ForceLookup|default:'' eq 'Yes'}checked="checked"{/if} />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ForceLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>
                                    
          </p> *}
                                

    </div>
  {* <div id="tabs-3" class="SystemAdminFormPanel inline">
      <p>
                                <label class="cardLabel" for="ReplicateToFaultDescription" >Replicate To Fault Description:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input  type="checkbox" name="ReplicateToFaultDescription" id="ReplicateToFaultDescription"  value="Yes" {if $datarow.ReplicateToFaultDescription|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReplicateToFaultDescription_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
      </p> 
      <p>
                                <label class="cardLabel" for="FillFromDOP" >Fill From DOP:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReplicateToFaultDescription" id="FillFromDOP"  value="Yes" {if $datarow.FillFromDOP|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FillFromDOP_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
      </p> 
      <p>
                                <label class="cardLabel" for="HideWhenRelatedFaultCodeBlank" >Hide When Related Fault Code Blank:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="HideWhenRelatedFaultCodeBlank" id="HideWhenRelatedFaultCodeBlank"  value="Yes" {if $datarow.HideWhenRelatedFaultCodeBlank|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_HideWhenRelatedFaultCodeBlank_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="FaultCodeNo" >Fault Code No:</label>
                                &nbsp;&nbsp;
                                <input  style="width:142px"  type="text" class="text"  name="FaultCodeNo" value="{$datarow.FaultCodeNo|default:''}" id="FaultCodeNo" >
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_FaultCodeNo_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
        </p>
   </div>
    <div id="tabs-4" class="SystemAdminFormPanel inline">
       <p>
                                <label class="cardLabel" for="HideForThirdPartyParts" >Hide For Third Party Parts:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="HideForThirdPartyParts" id="HideForThirdPartyParts"  value="Yes" {if $datarow.HideForThirdPartyParts|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_HideForThirdPartyParts_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
       <p>
                                <label class="cardLabel" for="NotReqForThirdPartyParts" >Not Required For Third Party Parts:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="NotReqForThirdPartyParts" id="NotReqForThirdPartyParts"  value="Yes" {if $datarow.NotReqForThirdPartyParts|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_NotReqForThirdPartyParts_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
       <p>
                                <label class="cardLabel" for="ReqAtBookingForChargeable" >Required At Booking For Chargeable:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReqAtBookingForChargeable" id="ReqAtBookingForChargeable"  value="Yes" {if $datarow.ReqAtBookingForChargeable|default:'' eq 'Yes'}checked="checked"{/if}  />
                                 <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqAtBookingForChargeable_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="ReqAtCompletionForChargeable" >Required At Completion For Chargeable:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReqAtCompletionForChargeable" id="ReqAtCompletionForChargeable"  value="Yes" {if $datarow.ReqAtCompletionForChargeable|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqAtCompletionForChargeable_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="ReqAtBookingForWarranty" >Required At Booking For Warranty:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReqAtBookingForWarranty" id="ReqAtBookingForWarranty"  value="Yes" {if $datarow.ReqAtBookingForWarranty|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqAtBookingForWarranty_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="ReqAtCompletionForWarranty" >Required At Completion For Warranty:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReqAtCompletionForWarranty" id="ReqAtCompletionForWarranty"  value="Yes" {if $datarow.ReqAtCompletionForWarranty|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqAtCompletionForWarranty_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="ReqIfExchangeIssued" >Required If Exchange Issued:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ReqIfExchangeIssued" id="ReqIfExchangeIssued"  value="Yes" {if $datarow.ReqIfExchangeIssued|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqIfExchangeIssued_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <p>
                                <label class="cardLabel" for="ReqOnlyForRepairType" >Required Only For Repair Type:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input onclick="if(this.checked==true){ $('#addFieldsDop3').show();$.colorbox.resize();}else{ $('#addFieldsDop3').hide();$.colorbox.resize(); }"   type="checkbox" name="ReqOnlyForRepairType" id="ReqOnlyForRepairType"  value="Yes" {if $datarow.ReqOnlyForRepairType|default:'' eq 'Yes'}checked="checked"{/if}  />
                                 <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqOnlyForRepairType_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
				</span>

                                    
      </p>
      <div id="addFieldsDop3" style="display:none">
       <p>
                            <label class="cardLabel" for="RepairTypeID" >Required Repair Type:</label>
                            &nbsp;&nbsp; 
                                <select name="RepairTypeID" id="RepairTypeID" class="text" >
                                <option value="" {if $datarow.RepairTypeID|default:"" eq ''}selected="selected"{/if}>Select from drop down</option>

                                
                                {foreach $RepairTypes|default:"" as $s}

                                    <option value="{$s.RepairTypeID|default:""}" {if $datarow.RepairTypeID|default:"" eq $s.RepairTypeID|default:""}selected="selected"{/if}>{$s.RepairTypeID|default:""}</option>

                                {/foreach}
                                
                            </select>
      </p>
      </div>
    </div>   *}                        
      <div id="tabs-3" class="SystemAdminFormPanel inline">
        <p>
                                <label class="cardLabel" for="UseLookup" >Use Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="UseLookup" id="UseLookup"  value="Yes" {if $datarow.UseLookup|default:'' eq 'Yes'}checked="checked"{/if}  />
                                 <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ReqOnlyForRepairType_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
      </p>  
        <p>
                                <label class="cardLabel" for="ForceLookup" >Force Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input   type="checkbox" name="ForceLookup" id="UseLookup"  value="Yes" {if $datarow.ForceLookup|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ForceLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
      </p>
      
       {if $datarow.PartFaultCodesLookupID|default:''!=''}
                     <p>
                            <label class="cardLabel" for="LinkedItem"  >Part Fault Code Lookups:</label>
                            &nbsp;&nbsp; 
                                <select name="LinkedItem" id="LinkedItem" class="text" style="margin-bottom:25px" >
                                <option value="" >{$page['Text']['select_default_option']|escape:'html'}</option>

                                
                                {foreach $LookupList|default:'' as $s}

                                    <option value="{$s.PartFaultCodesLookupLookupID|default:''}" >{$s.LookupName|default:''} </option>

                                {/foreach}
                                
                            </select>
                                <img style="cursor:pointer" src="{$_subdomain}/images/add_icon.png" onclick="addLinkedItem()">
                                
                        </p>
                        <p id="addLinkedItemP">
                        <div style="text-align:center" id="ajaxLoader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                        </p>
                        {else}
                            <p>
                            {* Restricted Models tab will become active only after saving changes *}
                            Part Fault Code Lookups tab will become active only after saving changes.
                            </p> 
                            
                         {/if}
      {if $datarow.PartFaultCodesLookupID|default:''!=''}
                        <script>
var ItemGroup="partpartscoudelookups";
                        function addLinkedItem(){
                       $('#addLinkedItemP').html('');
                       $('#ajaxLoader').show();
                         $.post("{$_subdomain}/LookupTables/addLinkedItem/",{ PID:{$datarow.PartFaultCodesLookupID|default:''} ,CID:$('#LinkedItem').val(),ItemGroup:ItemGroup},
function(data) {

removeLinkedItemOption($('#LinkedItem').val());
loadLinkedItem();
});
                        }
function removeLinkedItemOption(val){
$("#LinkedItem option[value="+val+"]").remove();
}

function loadLinkedItem(){
$('#ajaxLoader').show();
$('#addLinkedItemP').html('');
 $.post("{$_subdomain}/LookupTables/loadLinkedItem/",{ PID:{$datarow.PartFaultCodesLookupID|default:''} ,CID:$('#LinkedItem').val(),ItemGroup:ItemGroup},
function(data) {

en=jQuery.parseJSON(data);
$.each(en, function(key, value) { 
removeLinkedItemOption(en[key]['ChildItemID'])
$('#addLinkedItemP').append("<label class='cardLabel'></label><label title='Click to Remove' class='saFormSpan' style='cursor:pointer' onclick='removeAssoc("+en[key]['PartFaultCodesLookupLookupID']+")'>"+en[key]['ItemName']+"</label><br>");
});
$('#ajaxLoader').hide();
});
}
function removeAssoc(id){

if (confirm('Are you sure you want to remove this item?')) {
    $('#ajaxLoader').show();
   $.post("{$_subdomain}/LookupTables/delLinkedItem/",{ PID:{$datarow.PartFaultCodesLookupID|default:''} ,CID:id,ItemGroup:ItemGroup},
function() {
loadLinkedItem();
});
} else {
    // Do nothing!
}
}
                    
                        </script>
                        {/if}
      
          </div>
  <hr>
                               
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>

                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
