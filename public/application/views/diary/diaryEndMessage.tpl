<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
 <head>


{block name=config}
{$Title = ""}
{$PageId = 88}



{/block}

{block name=scripts}

        <link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" /> 
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
        
       <script type="text/javascript" src="{$_subdomain}/js/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script> 
        <script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
       
      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
    
    $(document).ready(function() {
    var modedd={if isset($mode)}{$mode}{else}0{/if};
    $.colorbox({ html:$('#h12s').html(), title:"Confirmation",escKey: false,width:'510px',height:'210px',
    overlayClose: false,
                       {if $type==1}
                       onLoad:function(){
                            $('#cboxClose').remove();
                        },
                       onComplete:function(){
                            $('#cboxClose').remove();
                        },
                       {/if}
                        onClosed:function(){
                       if(modedd==1){
                        window.location="{$_subdomain}/Diary/default/table=1";
                        }
                       }
});
//$('#cboxClose').remove();
$('#cboxLoadedContent').css('overflow','hidden');
//$('#cboxLoadedContent').css('height','500px');
});
function emailRoute(){

 $.post("{$_subdomain}/Diary/emailViamnteMap",{ a:1 },
function(data) {
$('.endMsgP1').html("<span style='color:green'>Emails sent, you can close this window.</span>");

});
}
function emailApptReminder()
{
    $.post("{$_subdomain}/Diary/sendAppointmentReminder",{ a:1,apptId:$("#apptId").val() },
    function(data) {
        $('.endMsgP2').html("<span style='color:green'>Emails sent, you can close this window.</span>");
    });
}
</script>
</head>
        <body>
{/block}
{if $type==1}
    <div style="display:none;" id="h12s" >
        <div style="text-align:center; color:#6b6b6b; overflow:hidden; height: 80px;">
            <h1 style="color:#8FD380">Appointment created!</h1>
            <div style="color:#6b6b6b; padding-top: 10px;">Its safe now to close Diary window.</div>
        </div>
    </div>
{/if}
{if $type==2}
    <div style="text-align:center;display:none;color:#6b6b6b;height:200px" id="h12s" >
        <div>
    <h1 style="color:#FF9900">Service Base Return Error: {$response.ResponseCode}!</h1>
    <div style="color:#6b6b6b">{if isset($response.ResponseDescription)}{$response.ResponseDescription}{/if}{if isset($response.Message)}{$response.Message}{/if}</div>
    </div>
    </div>
{/if}
{if $type==3}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#8FD380">Appointment deleted!</h1>
    <div style="color:#6b6b6b">Its safe now to close Diary window.</div>
    </div>
    </div>
{/if}
{if $type==4}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div >
    <h1 style="color:#8FD380">Appointment updated</h1>
    <div style="color:#6b6b6b">Its safe now to close Diary window.</div>
    </div>
    </div>
{/if}
{if $type==5}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Integrity Error!  Skyline records do not match ServiceBase records.</h1>
    <div style="color:#6b6b6b">Please contact PCCS support to resolve this issue.</div>
    </div>
    </div>
{/if}
{if $type==6}
    {*<div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#8FD380">Routine Optimisation Successful!</h1>
    {if $ret==2}<div style="color:#6b6b6b" ><input onclick="emailRoute();$('.endMsgP1').html('<span style=color:red>Please Wait! Sending emails, do not close this window</span>')" type="checkbox"> Email Route to Engineers<br><br>
                    <p class="endMsgP1" ></p>
               
                </div>{/if}
    </div>
    </div>*}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#8FD380">This Day has been Successfully Finalised</h1>
    {if $ret==2}
        <div style="color:#6b6b6b" >
            <input onclick="emailRoute();$('.endMsgP1').html('<span style=color:red>Please Wait! Sending emails, do not close this window</span>')" type="checkbox"> Email Route to Engineers<br><br>
            <input onclick="emailApptReminder();$('.endMsgP2').html('<span style=color:red>Please Wait! Sending emails, do not close this window</span>')" type="checkbox"> Email Customer Appointment Reminders<br><br>
            <p class="endMsgP1" ></p>            
            <p class="endMsgP2" ></p>
        </div>
    {/if}
    <input type="hidden" name="apptId" id="apptId" value="{if isset($apptId) && !empty($apptId)}{$apptId}{/if}" />
    </div>
    </div>
{/if}
{if $type==7}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Error</h1>
    <div style="color:#6b6b6b">
       There has been an incorrect response from the Remote Comms Application, please check that the correct version of Remote Comms is running.
        {foreach from=$response key=k item=i}
            {$k}={$i}<br>
            {/foreach}
    
    </div>
    </div>
    </div>
{/if}
{if $type==8}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#8FD380">Appointments updated!</h1>
    <div style="color:#6b6b6b">Its safe now to close Diary window.</div>
    </div>
    </div>
{/if}
{if $type==9}
    <div style="text-align:center;display:none;color:#6b6b6b;" id="h12s" >
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Appointment not deleted!</h1>
    <div style="color:#6b6b6b">Error while trying reach Service Base Remote Engineer!</div>
    </div>
    </div>
{/if}
{if $type==10}
    <div  id="h12s" style="text-align:center;display:none;color:#6b6b6b;">
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Appointments not updated!</h1>
    <div style="color:#6b6b6b">Unfortunately there are no engineers with the required skills available to fulfil this appointment. Please select another day for this appointment. </div>
    </div>
    </div>
{/if}
{if $type==11}
    <div  id="h12s" style="text-align:center;display:none;color:#6b6b6b;">
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Viamente key is wrong!</h1>
    <div style="color:#6b6b6b">The Viamente product key is incorrect or has expired</div>
    </div>
    </div>
{/if}
{if $type==12}
    <div  id="h12s" style="text-align:center;display:none;color:#6b6b6b;">
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Viamente return error!</h1>
    <div style="color:#6b6b6b">{$ret}</div>
    </div>
    </div>
{/if}
{if $type==13}
    <div  id="h12s" style="text-align:center;display:none;color:#6b6b6b;">
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Error day cannot be finalised, unreacehd appointments exist.</h1>
    <div style="color:#6b6b6b">Please make sure all appointments are reachable and try again.</div>
    </div>
    </div>
{/if}
{if $type==14}
    <div  id="h12s" style="text-align:center;display:none;color:#6b6b6b;">
        <div style="width:400px;text-align: center">
    <h1 style="color:#FF9900">Error day cannot be finalised, errors in appointments exist.</h1>
    <div style="color:#6b6b6b">Please make sure all errors are fixed and try again.</div>
    </div>
    </div>
{/if}
</body>