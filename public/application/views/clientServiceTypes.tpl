{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ClientServiceTypes}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
    </style>
{/block}
{block name=scripts}


{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}


<script type="text/javascript">
    
    var table;
    var checked = false;
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
        
        if (aData[3]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(2)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(2)', nRow).html( $statuses[0][0] );
        }
        
        
        //Getting assigned value for each Service Type for selected Client.
        
        if(aData[5])
        {
            
           $.post("{$_subdomain}/ProductSetup/clientServiceTypes/getAssigned/"+urlencode(aData[5])+"/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

            '',      
            function(data){
                    var p = eval("(" + data + ")");
                    
                    if(p['ServiceTypeName']!='undefined' && p['ServiceTypeName'] && p['ServiceTypeName']!='null')
                    {
                        $('td:eq(3)', nRow).html( p['ServiceTypeName'] );
                    }
                    else
                    {
                        $('td:eq(3)', nRow).html( '' );    
                    }
                    
                    if(p['Status']=='Active')
                    {   
                        $checked = ' checked="checked" ';
                    }
                    else
                    {
                        $checked = '  ';
                    }
                    
                    
                    $checkbox = '<input type="checkbox" class="checkBoxDataTable" name="assignedClientServiceTypes[]" '+$checked+' value="'+aData[4]+'" > ';
                    
                    $('td:eq(4)', nRow).html( $checkbox );

            });
        }
        
    }
    
    

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/clientServiceTypes/'+urlencode($("#nId").val()));
            }
        });
        $("#cId").combobox({
            change: function() {
                $showAssigned = '';
                if($("#showAssigned").prop("checked"))
                {
                    $showAssigned = 1;    
                }
                $(location).attr('href', '{$_subdomain}/ProductSetup/clientServiceTypes/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+$showAssigned);
            }
        });



                  //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });

                    //Click handler for save changes button.
                    $(document).on('click', '#save_changes_btn', 
                                function() {
                                    
                                    sltClientServiceTypes = '';
                                    
                                    $('.checkBoxDataTable').each(function() {
                                       sltClientServiceTypes += $(this).val()+',';
                                    });
                                       
                                    $("#sltClientServiceTypes").val(sltClientServiceTypes);  
                                  
                                  
                                    $.post("{$_subdomain}/ProductSetup/clientServiceTypes/saveChanges/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

                                        $("#ClientServiceTypesResultsForm").serialize(),      
                                        function(data){
                                        
                                               var p = eval("(" + data + ")");

                                               if(p=="OK")
                                               {
                                                   $("#centerInfoText").html("{$page['Text']['save_changes_done']|escape:'html'}").fadeIn('slow').delay("2000").fadeOut('slow');  
                                               }
                                               else 
                                               {
                                                   $("#centerInfoText").html("{$page['Text']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow').delay("2000").fadeOut('slow');  
                                               }
                                               
                                                     
                                            
                                        });



                                
                                    

                                });                    




                                


                      

                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                                           
                                $(location).attr('href', '{$_subdomain}/ProductSetup/clientServiceTypes/'+urlencode($("#nId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                      
                      
                      
                     /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {
                                
                                $showAssigned = '';
                                if($("#showAssigned").prop("checked"))
                                    {
                                        $showAssigned = 1;    
                                    }
                               
                            
                                $(location).attr('href', '{$_subdomain}/ProductSetup/clientServiceTypes/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+$showAssigned); 
                            }      
                        );*/
                      /* Add a change handler to the client dropdown - ends here*/ 
                      
                      
                      
                      
                      
                    /* Add a click handler to the show assigned only check box - strats here*/
                        $(document).on('click', '#showAssigned', 
                            function() {
                                
                                 $showAssigned = '';
                                if($("#showAssigned").prop("checked"))
                                    {
                                        $showAssigned = 1;    
                                    }
                            
                            
                                $(location).attr('href', '{$_subdomain}/ProductSetup/clientServiceTypes/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'+$showAssigned); 
                            }      
                        );
                      /* Add a click handler to the show assigned only check box - ends here*/ 
                     
              

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        


    
                         
                         
                        if($("#cId").val())
                        {
                            $("#showAssignedPanel").fadeIn();
                            $columnsList    = [ 
                                                        /* ServiceTypeID */  {  "bVisible":    false },    
                                                        /* JobTypeName */   null,
                                                        /* ServiceTypeName */   null,
                                                        /* Status */  null,
                                                        /* Mask */  null,
                                                        /* Assigned */   null
                                                ];
                            $('#save_changes_btn').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                         
                        }
                        else
                        {
                            $("#showAssignedPanel").fadeOut();
                            $columnsList    = [ 
                                                        /* ServiceTypeID */  {  "bVisible":    false },    
                                                        /* JobTypeName */   null,
                                                        /* ServiceTypeName */   null,
                                                        /* Status */  null
                                                ];
                             $('#save_changes_btn').attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');                    
                        }
     
                     
                    
                    
                        {if $nId && $cId}
                
                            $displayButtons = "U";
                            $dblclickCallbackMethod = "gotoEditPage";
                        
                        {else}
                            
                            $displayButtons = "";
                            $dblclickCallbackMethod = "";
                            
                        {/if}
                            
                        $tooltipTitle = "";
                        
                   
         
                    
			table = $('#ClientServiceTypesResults').PCCSDataTable({
                            aoColumns:		$columnsList,
                            displayButtons:	$displayButtons,
                            htmlTablePageId:	'ClientServiceTypesResultsPanel',
                            htmlTableId:	'ClientServiceTypesResults',
                            fetchDataUrl:	'{$_subdomain}/ProductSetup/ProcessData/ClientServiceTypes/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}")+"/"+urlencode("{$showAssigned}"),
                            fnRowCallback:	'inactiveRow',
                            searchCloseImage:	'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            dblclickCallbackMethod: $dblclickCallbackMethod,
                            tooltipTitle:	$tooltipTitle,
                            iDisplayLength:	100,
                            sDom:		'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:	'dataTables_command',
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/ProductSetup/clientServiceTypes/update/'+urlencode("{$nId}")+"/"+urlencode("{$cId}")+'/',
                            updateDataUrl:   '{$_subdomain}/ProductSetup/ProcessData/ClientServiceTypes/',
                            formUpdateButton:'update_save_btn',
                            formCancelButton:'cancel_btn',
                            popUpFormWidth:  715,
                            popUpFormHeight: 430,
                            colorboxFormId:  "STForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label"
                            
                            
                        });
                      
		      
			$(document).on("click", "#checkAll", function() {

			    if(!checked) {
				$(".checkBoxDataTable").each(function() {
				    $(this).attr("checked","checked");
				});
				checked = true;
				$("#checkAll label").text("Uncheck All");
			    } else {
				$(".checkBoxDataTable").each(function() {
				    $(this).removeAttr("checked");
				});
				checked = false;
				$("#checkAll label").text("Check All");
			    }
			    return false;

			});
		    

    });

</script>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ClientServiceTypesTopForm" name="ClientServiceTypesTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="ClientServiceTypesResultsPanel" >
                    
                    {if $SupderAdmin eq true} 
                        <form name="listsForm" id="clientServiceListsForm">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        &nbsp;&nbsp;{$page['Labels']['client_label']|escape:'html'}
                        <select name="cId" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        <span id="showAssignedPanel" style="display:none;" >
                        <input class="assignedTextBox" id="showAssigned"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        
                        </form>
                    {else if $NetworkUser eq true}
                        <form name="listsForm" >
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >   
                        
                        <select name="cId" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        <span id="showAssignedPanel" style="display:none;" >
                        <input class="assignedTextBox" id="showAssigned"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        </form>
                        
                    {else if $ClientUser eq true}
                        <form name="listsForm" >
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" > 
                        <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >  
                        
                        <span id="showAssignedPanel" style="display:none;" >
                            <input class="assignedTextBox" id="showAssigned"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        </form>
                        
                    {/if} 
                    <br><br>
                    <form id="ClientServiceTypesResultsForm" class="dataTableCorrections">
                        <table id="ClientServiceTypesResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['job_type']|escape:'html'}" >{$page['Text']['job_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_type']|escape:'html'}" >{$page['Text']['service_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                            {if $cId neq ''}
                                                <th title="{$page['Text']['service_type_mask']|escape:'html'}" >{$page['Text']['service_type_mask']|escape:'html'}</th>
                                                <th title="{$page['Text']['assigned']|escape:'html'}" >{$page['Text']['assigned']|escape:'html'}</th>
                                            {/if}
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        <input type="hidden" name="sltClientServiceTypes" id="sltClientServiceTypes" >
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    
		    <hr>
                    
		    <button id="save_changes_btn" type="button" class="gplus-blue-disabled leftBtn" ><span class="label">{$page['Buttons']['save_changes']|escape:'html'}</span></button>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
		    <button id="checkAll" type="button" class="gplus-blue rightBtn"><span class="label">Check All</span></button>

                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               
    </div>
                        

{/block}



