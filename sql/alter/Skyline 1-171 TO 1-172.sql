# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.171');

# ---------------------------------------------------------------------- #
# Modify table "part"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `part` ADD COLUMN `ModifiedUserID` INT(11) NULL DEFAULT NULL AFTER `SupplierOrderNo`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.172');
