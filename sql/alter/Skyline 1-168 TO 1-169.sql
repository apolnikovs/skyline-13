# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.168');

# ---------------------------------------------------------------------- #
# Modify table "service_type_alias"                                      #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_type_alias` CHANGE `ServiceTypeMask` `ServiceTypeMask` INT( 11 ) NULL DEFAULT NULL;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.169');
