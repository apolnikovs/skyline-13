CREATE TABLE IF NOT EXISTS import_noise_postcodes (
  Network VARCHAR(40),
  RepairSkill VARCHAR(20),
  MfgName VARCHAR(40),
  JobType VARCHAR(40),
  ServiceType VARCHAR(40),
  OutOfArea VARCHAR(3),
  Postcode VARCHAR(10),
  CompanyName VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/NoisePostcodes.csv' 
INTO TABLE import_noise_postcodes
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_noise_postcodes2 (
  ID INTEGER NOT NULL AUTO_INCREMENT,
  Network VARCHAR(40),
  RepairSkill VARCHAR(20),
  MfgName VARCHAR(40),
  JobType VARCHAR(40),
  ServiceType VARCHAR(40),
  CompanyName VARCHAR(40),
  PRIMARY KEY (ID)
);

INSERT INTO import_noise_postcodes2 (Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName)
SELECT DISTINCT Network,RepairSkill, MfgName, JobType, ServiceType, CompanyName FROM import_noise_postcodes;

CREATE TABLE IF NOT EXISTS import_sony_postcodes (
  Network VARCHAR(40),
  RepairSkill VARCHAR(20),
  MfgName VARCHAR(40),
  JobType VARCHAR(40),
  ServiceType VARCHAR(40),
  OutOfArea VARCHAR(3),
  Postcode VARCHAR(10),
  CompanyName VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/SonyPostcodes.csv' 
INTO TABLE import_sony_postcodes
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_sony_postcodes2 (
  ID INTEGER NOT NULL AUTO_INCREMENT,
  Network VARCHAR(40),
  RepairSkill VARCHAR(20),
  MfgName VARCHAR(40),
  JobType VARCHAR(40),
  ServiceType VARCHAR(40),
  CompanyName VARCHAR(40),
  PRIMARY KEY (ID)
);

INSERT INTO import_sony_postcodes2 (Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName)
SELECT DISTINCT Network, RepairSkill, MfgName, JobType, ServiceType, CompanyName FROM import_sony_postcodes;

CREATE INDEX idx_noise_postcodes2_1 ON import_noise_postcodes2 (Network,RepairSkill,MfgName,JobType,ServiceType,CompanyName);
CREATE INDEX idx_sony_postcodes2_1 ON import_sony_postcodes2 (Network,RepairSkill,MfgName,JobType,ServiceType,CompanyName);

CREATE TABLE IF NOT EXISTS import_branch_users (
Username VARCHAR(20),
PASSWORD VARCHAR(32),
FullName VARCHAR(40),
Role VARCHAR(40),
Branch VARCHAR(40),
Email VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/BranchUsers.csv' 
INTO TABLE import_branch_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_client_brand_branch (
network VARCHAR(50),
CLIENT VARCHAR(50),
AssignedASC VARCHAR(40),
BrandID INT,
Brand VARCHAR(40),
RetailerName VARCHAR(40),
Street VARCHAR(40),
AREA VARCHAR(40),
Town VARCHAR(60),
County VARCHAR(40),
Postcode VARCHAR(10),
PhoneNo VARCHAR(40),
FaxNo VARCHAR(40),
Email VARCHAR(40),
Contact VARCHAR(40),
AccountNo VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ClientBrandBranch.csv' 
INTO TABLE import_client_brand_branch
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_client_users (
Username VARCHAR(20),
PASSWORD VARCHAR(32),
FullName VARCHAR(40),
Role VARCHAR(40),
Retailer VARCHAR(40),
Email VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ClientUsers.csv' 
INTO TABLE import_client_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_manufacturers (
  MfgName VARCHAR(30),
  CostCodeAccountNo VARCHAR(40),
  AccountNumber VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/Manufacturers.csv' 
INTO TABLE import_manufacturers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_providers (
  companyname VARCHAR(50),
  street VARCHAR (40),
  localarea VARCHAR (40),
  towncity VARCHAR (40),
  county VARCHAR (255),
  postalcode VARCHAR (10),
  contactphone VARCHAR (40),
  contactfax VARCHAR (40),
  contactemail VARCHAR (40),
  contactname VARCHAR (50),
  vatnumber VARCHAR (40),
  sageaccountnumber VARCHAR (40),
  sagepiaccount VARCHAR (40),
  accountemailaddress VARCHAR (40),
  invoicetocompanyname VARCHAR (40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ServiceProviders.csv' 
INTO TABLE import_service_providers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_types (
  servicetypename VARCHAR(40),
  CODE VARCHAR(64),
  jobtype VARCHAR(30)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ServiceTypes.csv' 
INTO TABLE import_service_types
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS import_network_service_providers
(
	wid VARCHAR (50),
	companyname VARCHAR(50),
	accountnumber VARCHAR (40),
	companycode VARCHAR (255),
	trackingusername VARCHAR (40),
	trackingpassword VARCHAR (40),
	region VARCHAR (2),
	no_link INT,
	warrantyusername VARCHAR (40),
	warrantypassword VARCHAR (40), 
	warrantyaccountnumber VARCHAR (40),
	ascarea VARCHAR (20),
	walkinjobsupload TINYINT (1)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/NetworkServiceProviders.csv' 
INTO TABLE import_network_service_providers
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_unit_types (
  unittype VARCHAR(30),
  jobtype VARCHAR(30),
  repairskill VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/UnitTypes.csv' 
INTO TABLE import_unit_types
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_completion_statuses (
  statusname VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/CompletionStatus.csv' 
INTO TABLE import_completion_statuses
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_contact_actions (
	actioncode INTEGER,
	actionname VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ContactActions.csv' 
INTO TABLE import_contact_actions
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_irish_counties (
  County VARCHAR(40),
  InternationalCode VARCHAR(3),
  CountryName VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/IrishCounties.csv' 
INTO TABLE import_irish_counties
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_network_users (
Username VARCHAR(20),
PASSWORD VARCHAR(32),
FullName VARCHAR(40),
Role VARCHAR(40),
Network VARCHAR(40),
Email VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/NetworkUsers.csv' 
INTO TABLE import_network_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_service_centre_users (
Username VARCHAR(20),
PASSWORD VARCHAR(32),
FullName VARCHAR(40),
Role VARCHAR(40),
ServiceCentre VARCHAR(40),
Email VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ServiceCentreUsers.csv' 
INTO TABLE import_service_centre_users
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_status (
  statusname VARCHAR(40)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/Status.csv' 
INTO TABLE import_status
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_uk_postcode_05 (
  postcode VARCHAR(5) NOT NULL,
  eastings INT(11) NOT NULL DEFAULT '0',
  northings INT(11) NOT NULL DEFAULT '0',
  latitude DECIMAL(12,5) NOT NULL DEFAULT '0.00000',
  longitude DECIMAL(12,5) NOT NULL DEFAULT '0.00000',
  town VARCHAR(255) DEFAULT NULL,
  region VARCHAR(255) DEFAULT NULL,
  country VARCHAR(10) NOT NULL,
  country_string VARCHAR(20) NOT NULL
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/uk_postcode_05.csv' 
INTO TABLE import_uk_postcode_05
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_bog (
  Network VARCHAR(50),
  CLIENT VARCHAR(50),
  Manufacturer VARCHAR(30),
  RepairSkill VARCHAR(40),
  UnitType VARCHAR(30),
  Model VARCHAR(30)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/BoughtOutGuarantees.csv' 
INTO TABLE import_bog
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_central_service (
  Network VARCHAR(50),
  Country VARCHAR(10),
  RepairSkill VARCHAR(40),
  CLIENT VARCHAR(50),
  MfgName VARCHAR(40),
  JobType VARCHAR(30),
  ServiceType VARCHAR(30),
  UnitType VARCHAR(30),
  UKCountyID VARCHAR(30),
  CompanyName VARCHAR(50)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/CentralService.csv' 
INTO TABLE import_central_service
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_ireland_towns (
  Network VARCHAR(50),
  TownName VARCHAR(40),
  CountyName VARCHAR(40),
  ServiceCentre VARCHAR(50) 
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/IrelandTownAllocations.csv' 
INTO TABLE import_ireland_towns
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n';

CREATE TABLE IF NOT EXISTS import_products (
	CATNO VARCHAR(10),
	ProductType VARCHAR(30),
	Manufacturer VARCHAR(30),
	ASP DECIMAL(10,2),
	Model VARCHAR(30),
	AuthorityLimit DECIMAL(10,2),
	ClaimbackInvoiceNoise CHAR(2)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/ArgosCatalogue.csv' 
INTO TABLE import_products
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS import_unit_pricing_repair (
	Network VARCHAR(40),
	CLIENT VARCHAR(40),
	ClientAccount VARCHAR(10),
	UnitType VARCHAR(40),
	JobSite VARCHAR(40),
	CompletionStatus VARCHAR(20),
	ServiceRate DECIMAL(10,2),
	ReferralFee DECIMAL(10,2)
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/UnitPricingRepair.csv' 
INTO TABLE import_unit_pricing_repair
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES;

CREATE TABLE IF NOT EXISTS import_full_postcodes (
  `PostCode` varchar(10) DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Easting` int(11) DEFAULT NULL,
  `Northing` int(11) DEFAULT NULL,
  `GridRef` varchar(9) DEFAULT NULL,
  `County` varchar(50) DEFAULT NULL,
  `District` varchar(50) DEFAULT NULL,
  `Ward` varchar(100) DEFAULT NULL,
  `DistrictCode` varchar(50) DEFAULT NULL,
  `WardCode` varchar(50) DEFAULT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `CountyCode` varchar(20) DEFAULT NULL
);

LOAD DATA INFILE 'C:/Users/employee1/Documents/NetBeansProjects/skyline/sql/Initialise Database/RMA Import Data/FullPostcodes.csv/FullPostcodes.csv' 
INTO TABLE import_full_postcodes
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' ESCAPED BY '\\'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
