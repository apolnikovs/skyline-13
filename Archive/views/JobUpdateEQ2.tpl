    <div id="EQ2_old" class="span-24 hide">
        
        <div class="span-12">
            
            <fieldset style="height: 100%">
                <legend>Key Dates</legend>
                <p>
                    <span class="fieldLabel">Job Booked:</span>
                    <span class="fieldValue">12/01/2012</span>
                </p>
                <p>
                    <span class="fieldLabel">Appointment:</span>
                    <span class="fieldValue">13/01/2012</span>
                </p>
                <p>
                    <span class="fieldLabel">Completed:</span>
                    <span class="fieldValue">13/01/2012</span>
                </p>
                <p>
                    <span class="fieldLabel">Despatched:</span>
                    <span class="fieldValue">13/01/2012</span>
                </p>
                <p>
                    <span class="fieldLabel">Courier::</span>
                    <span class="fieldValue">City Link</span>
                </p>
                <p>
                    <span class="fieldLabel">Tracking No:</span>
                    <span class="fieldValue">RT7593011239</span>
                </p>
                <p>
                    <span class="fieldLabel">Received in Store:</span>
                    <span class="fieldValue"><a href="#">Click here</a> to receive back</span>
                </p>
                <p>
                    <span class="fieldLabel">Returned to Customer:</span>
                    <span class="fieldValue"><a href="#">Click here</a> to despatch out</span>
                </p>
            </fieldset>
            
        </div>
    
        <div class="span-12 last">
            
            <fieldset >
                <legend>Reported Issues</legend>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Morbi posuere suscipit mauris ut sollicitudin. 
                    Mauris pretium euismod risus, quis interdum eros convallis non. 
                    Mauris ac tortor est, sit amet rhoncus velit. 
                    Mauris nibh nulla, iaculis sed tempus non, lacinia a dolor. 
                    In nec sem velit. Vivamus a fermentum leo. 
                    Curabitur blandit commodo tellus vel malesuada. 
                    Sed sed consectetur nisi. Maecenas mi ante, interdum in iaculis 
                    quis, feugiat vitae nisl. Sed faucibus rutrum nisl, sed aliquet dui 
                    venenatis eu. Quisque vitae arcu augue. Fusce quis varius lacus. 
                </p>
            </fieldset>
            
            <fieldset >
                <legend>Service Report</legend>
                <p>
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
                    posuere cubilia Curae; Sed bibendum auctor vulputate. Vivamus et 
                    faucibus metus. Nullam vel odio nec sem volutpat eleifend vitae 
                    quis nulla
                </p>
            </fieldset>
            
            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1">Contact History</a></li>
                    <li><a href="#tabs-2">Appointment History</a></li>
                    <li><a href="#tabs-3">Status</a></li>
                    <li><a href="#tabs-4">Parts Used</a></li>
                </ul>
                <div id="tabs-1" style="padding: 5px;">
                    <h4 style="margin-bottom: 5px;">Contact History</h4>
                    <table id="contact_history_table" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>01/01/2012</td>
                                <td>10:00</td>
                                <td>Vestibulum ante ipsum</td>
                            </tr>  
                            <tr>
                                <td>14/01/2012</td>
                                <td>14:30</td>
                                <td>Nullam vel odio nec sem volutpat</td>
                            </tr> 
                            <tr>
                                <td>17/01/2012</td>
                                <td>09:15</td>
                                <td>Sed faucibus rutrum nisl</td>
                            </tr>                
                        </tbody>
                    </table>
                </div>
                <div id="tabs-2" style="padding: 5px;">
                    <h4 style="margin-bottom: 5px;">Appointment History</h4>
                    <table id="appointment_history_table" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Appointment Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>07/01/2012</td>
                                <td>11:15</td>
                                <td></td>
                            </tr>  
                            <tr>
                                <td>12/01/2012</td>
                                <td>14:30</td>
                                <td></td>
                            </tr>               
                        </tbody>
                    </table>
                    </div>
                <div id="tabs-3" style="padding: 5px;">
                    <h4 style="margin-bottom: 5px;">Status</h4>
                    <table id="status_table" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Status</th>
                                <th>User</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr>  
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr> 
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr> 
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr>
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr>  
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr> 
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr> 
                            <tr>
                                <td>10/01/2012</td>
                                <td>11:15</td>
                                <td>02 ALLOCATED TO AGENT</td>
                                <td>AC001</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="tabs-4" style="padding: 5px;">
                    <h4 style="margin-bottom: 5px;">Parts Used</h4>
                    <table id="parts_used_table" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                            <tr>
                                <th>Part No</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Ordered</th>
                                <th>Order No</th>
                                <th>Received</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>123456</td>
                                <td>this is a part</td>
                                <td>1</td>
                                <td>20/01/2012</td>
                                <td>1234</td>
                                <td>26/01/2012</td>                                
                            </tr>  
                            <tr>
                                <td>123456</td>
                                <td>this is a part</td>
                                <td>1</td>
                                <td>20/01/2012</td>
                                <td>1234</td>
                                <td>26/01/2012</td> 
                            </tr>               
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        
    </div>